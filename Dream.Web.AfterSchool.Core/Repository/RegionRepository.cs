using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// RegionRepository
    /// </summary>
    public class RegionRepository : BaseRepository
    {
        /// <summary>
        /// Region 목록 조회
        /// </summary>
        /// <param name="entity">Region</param>
        /// <returns></returns>
        public List<Region> RegionSelect(Region entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                

                var ret = dbConnection.Query<Region>("RegionSelect", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// Region 페이징 목록 조회
        /// </summary>
        /// <param name="entity">Region</param>
        /// <returns></returns>
        public void RegionSelectPaging(ViewModel<Region> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@SortField", entity.SortField);
                parameters.Add("@SortOrder", entity.SortOrder);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.Query<Region>("RegionSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);
                entity.List = ret.ToList();
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// Region 항목 조회
        /// </summary>
        /// <param name="entity">Region</param>
        /// <returns></returns>
        public Region RegionSelectItem(Region entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                

                var ret = dbConnection.Query<Region>("RegionSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// Region 등록
        /// </summary>
        /// <param name="entity">Region</param>
        /// <returns></returns>
        public int RegionInsert(Region entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RegionCode", entity.RegionCode);
				parameters.Add("@RegionName", entity.RegionName);
				parameters.Add("@UpperCode", entity.UpperCode);
				parameters.Add("@UpperName", entity.UpperName);
				parameters.Add("@OfficeofEducationName", entity.OfficeofEducationName);

                return dbConnection.Execute("RegionInsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Region 수정
        /// </summary>
        /// <param name="entity">Region</param>
        /// <returns></returns>
        public int RegionUpdate(Region entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RegionCode", entity.RegionCode);
				parameters.Add("@RegionName", entity.RegionName);
				parameters.Add("@UpperCode", entity.UpperCode);
				parameters.Add("@UpperName", entity.UpperName);
				parameters.Add("@OfficeofEducationName", entity.OfficeofEducationName);

                return dbConnection.Execute("RegionUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Region 등록 및 수정
        /// </summary>
        /// <returns></returns>
        public int RegionUpsert(Region entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RegionCode", entity.RegionCode);
				parameters.Add("@RegionName", entity.RegionName);
				parameters.Add("@UpperCode", entity.UpperCode);
				parameters.Add("@UpperName", entity.UpperName);
				parameters.Add("@OfficeofEducationName", entity.OfficeofEducationName);

                return dbConnection.Execute("RegionUpsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Region 삭제
        /// </summary>
        /// <param name="entity">Region</param>
        /// <returns></returns>
        public int RegionDelete(Region entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                

                return dbConnection.Execute("RegionDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("Course")]
    public partial class Course : BaseEntity
    {
        /// <summary>
		/// 교과코드
		/// </summary>
		[Required]
		[StringLength(20)]
		public string CssCode { get; set; }
		

		/// <summary>
		/// 교과명
		/// </summary>
		[Required]
		[StringLength(20)]
		public string RegionName { get; set; }
		

		/// <summary>
		/// 교과타입(0:내신, 1:정시)
		/// </summary>
		[Required]
		public int CssType { get; set; }
		

		/// <summary>
		/// 방과후 개설 교과명
		/// </summary>
		[StringLength(100)]
		public string CourseName { get; set; }
		

    }
}

﻿using Dream.Web.AfterSchool.Core.Repository;
using Dream.Web.AfterSchool.MobileWeb.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.MobileWeb.Controllers
{
    public class APIController : Controller
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult PopUp()
        {
            var popup = PortalPopupRepository.Instance.PortalPopupSelect();

            try
            {
                if (popup.IsUse)
                {
                    return Json(new
                    {
                        popup.PopupType,
                        popup.Width,
                        popup.Height
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return HttpNotFound();
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult PopupDetail()
        {
            ViewBag.LocalHost = string.Format("{0}://{1}", Request.Url.Scheme, Request.Url.Authority);

            return View();
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult PopupDetail2()
        {
            ViewBag.LocalHost = string.Format("{0}://{1}", Request.Url.Scheme, Request.Url.Authority);

            return View();
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult PopupDetail3()
        {
            ViewBag.LocalHost = string.Format("{0}://{1}", Request.Url.Scheme, Request.Url.Authority);

            return View();
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult ASRedirectionUrl()
        {
            try
            {
                var asurl = PortalPopupRepository.Instance.AfterSchoolURL();
                return Json(new
                {
                    IsUse = asurl.IsUse ? 1 : 0,
                    asurl.URL
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    IsUse = 0,
                    URL = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult ASRedirectionUrlView()
        {
            try
            {
                var asurl = PortalPopupRepository.Instance.AfterSchoolURL();

                if (asurl == null)
                {
                    asurl.IsUse = false;
                    asurl.URL = "";
                }

                ViewBag.IsUse = asurl.IsUse ? 1 : 0;
                ViewBag.URL = asurl.URL;

                return View();
            }
            catch (Exception ex)
            {
                ViewBag.IsUse = 0;
                ViewBag.URL = "";

                return View();
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult ClientIP()
        {
            string ipAddress = Request.UserHostAddress;
            ViewBag.ip1 = Request.UserHostAddress;

            StringBuilder serverval = new StringBuilder();


            foreach (string val in Request.ServerVariables)
            {
                serverval.Append(string.Format("{0}:{1}", val, Request.ServerVariables[val].ToString()));
                serverval.Append(";");
            }

            ViewBag.ip2 = serverval.ToString();

            StringBuilder serverval2 = new StringBuilder();

            foreach (string val in Request.Headers.AllKeys)
            {
                serverval2.Append(string.Format("{0}:{1}", val, Request.Headers[val].ToString()));
                serverval2.Append(";");
            }

            ViewBag.ip3 = serverval2.ToString();


            return View();
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult SendMail(string data)
        {
            try
            {
                Dictionary<string, string> jsonObject = (new System.Web.Script.Serialization.JavaScriptSerializer()).Deserialize<Dictionary<string, string>>(data);
                StringBuilder body = new StringBuilder("<html>");
                body.Append("<head>");
                body.Append("<title>패스워드 발급</title>");
                body.Append("</head>");
                body.Append("<body>");
                body.Append("<div style='width:500px; height:200px; border:2px solid blue; padding:10px; margin:10px; background:#eee;'>");
                body.Append("<h1 style='text-align:center; color:#fff; background:#555; padding:10px 0 10px 0 ; margin:0;'>패스워드  발급 안내</h1>");
                body.Append("<p style='padding: 10px 0 0 20px; font-family:dotum; font-size:14px;'> 안녕하세요. 진로진학 시스템입니다.</p>");
                body.Append("<p style='padding: 10px 0 0 20px; font-family:dotum; font-size:14px;'>" + jsonObject["uname"] + " 님의 패스워드를 변경하려면 아래 링크를 클릭해 주세요. </p>");
                body.Append("<a href='" + "https://jdream.jne.go.kr/UAccount/PasswordLink?token=" + jsonObject["token"] + "' style='padding: 10px 0 0 20px; font-family:dotum; font-size: 16px;' >패스워드 변경</a>");
                body.Append("</body>");
                body.Append("</html>");
                
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.UseDefaultCredentials = true; // 시스템에 설정된 인증 정보를 사용하지 않는다.
                client.EnableSsl = true;  // SSL을 사용한다.
                client.DeliveryMethod = SmtpDeliveryMethod.Network; // 이걸 하지 않으면 Gmail에 인증을 받지 못한다.
                client.Credentials = new System.Net.NetworkCredential("futuredev1004@gmail.com", "future1004**");

                MailAddress from = new MailAddress("futureplandev@gmail.com", "대학연계관리자", System.Text.Encoding.UTF8);
                MailAddress to = new MailAddress(jsonObject["to"]);

                MailMessage message = new MailMessage(from, to);

                message.Body = body.ToString();
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Subject = "패스워드 발급";
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                
                // 동기로 메일을 보낸다.
                client.Send(message);

                // Clean up.
                message.Dispose();
                
                return Json(new
                {
                    success = true,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult RequestSMS(string data)
        {
            try
            {
                Dictionary<string, string> jsonObject = (new System.Web.Script.Serialization.JavaScriptSerializer()).Deserialize<Dictionary<string, string>>(data);
                var identity = SSOAuth.Instance.Unprotect(jsonObject["token"]);
                if (identity != null && identity.FindFirst(ClaimTypes.Role).Value == "REQUESTSMS")
                {
                    string email = identity.FindFirst(ClaimTypes.Email).Value;
                    string phone = identity.FindFirst("phone").Value;
                    string deviceid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    string smscode = SMSManage.Instance.RndNumber();

                    if (string.IsNullOrEmpty(SMSManage.Instance.SendSMSCode(smscode, phone)))
                    {
                        if (SMSRepository.Instance.SMSRequest(email, smscode, deviceid, phone))
                        {
                            return Json(new
                            {
                                success = true,
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult ValidateSMS(string data)
        {
            try
            {
                Dictionary<string, string> jsonObject = (new System.Web.Script.Serialization.JavaScriptSerializer()).Deserialize<Dictionary<string, string>>(data);
                var identity = SSOAuth.Instance.Unprotect(jsonObject["token"]);
                if (identity != null && identity.FindFirst(ClaimTypes.Role).Value == "VALIDATESMS")
                {
                    string email = identity.FindFirst(ClaimTypes.Email).Value;
                    string phone = identity.FindFirst("phone").Value;
                    string deviceid = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                    string smscode = identity.FindFirst("smscode").Value;

                    int ret = SMSRepository.Instance.SMSValidate(email, smscode, deviceid, phone);

                    if (ret == 99)
                    {
                        return Json(new
                        {
                            success = true,
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            success = false,
                            retry = ret
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public ActionResult SelectSchool(string type)
        {
            try
            {
                var ret = SchoolInfosRepository.Instance.SelectSchool(type);

                return Json(new
                {
                    success = true,
                    schools = ret
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }
        }
   }
}
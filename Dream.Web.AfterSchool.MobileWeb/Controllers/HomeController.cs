﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.MobileWeb.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("LectureList", "LectureRequest", new { lecturetype = 0 });
        }
    }
}
using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// SchoolInfosRepository
    /// </summary>
    public class SchoolInfosRepository : BaseRepository
    {
        #region Singleton Instance

        private static volatile SchoolInfosRepository _instance;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static SchoolInfosRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(SchoolInfosRepository))
                    {
                        if (_instance == null)
                        {
                            _instance = new SchoolInfosRepository();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion
        /// <summary>
        /// SchoolInfos 목록 조회
        /// </summary>
        /// <param name="entity">SchoolInfos</param>
        /// <returns></returns>
        public List<SchoolInfos> SchoolInfosSelect(SchoolInfos entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                var ret = dbConnection.Query<SchoolInfos>("SchoolInfosSelect", commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// SchoolInfos ?�이�?목록 조회
        /// </summary>
        /// <param name="entity">SchoolInfos</param>
        /// <returns></returns>
        public void SchoolInfosSelectPaging(ViewModel<SchoolInfos> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@SortField", entity.SortField);
                parameters.Add("@SortOrder", entity.SortOrder);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.Query<SchoolInfos>("SchoolInfosSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);
                entity.List = ret.ToList();
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// SchoolInfos ??�� 조회
        /// </summary>
        /// <param name="entity">SchoolInfos</param>
        /// <returns></returns>
        public SchoolInfos SchoolInfosSelectItem(SchoolInfos entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@sch_code", entity.sch_code);

                var ret = dbConnection.Query<SchoolInfos>("SchoolInfosSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// SchoolInfos ?�록
        /// </summary>
        /// <param name="entity">SchoolInfos</param>
        /// <returns></returns>
        public int SchoolInfosInsert(SchoolInfos entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@o_sch_code", entity.o_sch_code);
				parameters.Add("@sch_code", entity.sch_code);
				parameters.Add("@sch_type", entity.sch_type);
				parameters.Add("@sch_name", entity.sch_name);
				parameters.Add("@sch_region1", entity.sch_region1);
				parameters.Add("@sch_region2", entity.sch_region2);
				parameters.Add("@sch_addr", entity.sch_addr);
				parameters.Add("@sch_url", entity.sch_url);
				parameters.Add("@sch_tel", entity.sch_tel);
				parameters.Add("@sch_fax", entity.sch_fax);
				parameters.Add("@sch_est_ghn", entity.sch_est_ghn);

                return dbConnection.Execute("SchoolInfosInsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// SchoolInfos ?�정
        /// </summary>
        /// <param name="entity">SchoolInfos</param>
        /// <returns></returns>
        public int SchoolInfosUpdate(SchoolInfos entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@o_sch_code", entity.o_sch_code);
				parameters.Add("@sch_code", entity.sch_code);
				parameters.Add("@sch_type", entity.sch_type);
				parameters.Add("@sch_name", entity.sch_name);
				parameters.Add("@sch_region1", entity.sch_region1);
				parameters.Add("@sch_region2", entity.sch_region2);
				parameters.Add("@sch_addr", entity.sch_addr);
				parameters.Add("@sch_url", entity.sch_url);
				parameters.Add("@sch_tel", entity.sch_tel);
				parameters.Add("@sch_fax", entity.sch_fax);
				parameters.Add("@sch_est_ghn", entity.sch_est_ghn);

                return dbConnection.Execute("SchoolInfosUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// SchoolInfos ?�록 �??�정
        /// </summary>
        /// <returns></returns>
        public int SchoolInfosUpsert(SchoolInfos entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@o_sch_code", entity.o_sch_code);
				parameters.Add("@sch_code", entity.sch_code);
				parameters.Add("@sch_type", entity.sch_type);
				parameters.Add("@sch_name", entity.sch_name);
				parameters.Add("@sch_region1", entity.sch_region1);
				parameters.Add("@sch_region2", entity.sch_region2);
				parameters.Add("@sch_addr", entity.sch_addr);
				parameters.Add("@sch_url", entity.sch_url);
				parameters.Add("@sch_tel", entity.sch_tel);
				parameters.Add("@sch_fax", entity.sch_fax);
				parameters.Add("@sch_est_ghn", entity.sch_est_ghn);

                return dbConnection.Execute("SchoolInfosUpsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// SchoolInfos ??��
        /// </summary>
        /// <param name="entity">SchoolInfos</param>
        /// <returns></returns>
        public int SchoolInfosDelete(SchoolInfos entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@sch_code", entity.sch_code);

                return dbConnection.Execute("SchoolInfosDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public List<School> SelectSchool(string type = "H")
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@sch_type", type);

                return dbConnection.Query<School>("sp_Select_School", param: parameters, commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
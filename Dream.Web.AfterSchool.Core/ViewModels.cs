using System.Collections.Generic;

namespace Dream.Web.AfterSchool.Core.Entities
{
    public class ViewModel<ItemT, ListT, SearchT>
    {
        public ItemT Item { get; set; }

        public List<ListT> List { get; set; }
        public SearchT Search { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }

    public class ViewModel<ListT, SearchT>
    {
        public List<ListT> List { get; set; }
        public SearchT Search { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }

    public class ViewModel<T>
    {
        public T Item { get; set; }
        public List<T> List { get; set; }
        public T Search { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public string SearchField { get; set; }
        public string SearchText { get; set; }

        public ViewModel()
        {
            List = new List<T>();
            Search = default(T);
        }
    }
}
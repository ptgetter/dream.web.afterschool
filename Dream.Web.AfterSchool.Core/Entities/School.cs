using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("School")]
    public partial class School : BaseEntity
    {
        public int sidx { get; set; }

        public string sch_type { get; set; }
        public string sch_code { get; set; }
        public string sch_name { get; set; }
        public string sch_area { get; set; }
        public string sch_area2 { get; set; }
    }
}

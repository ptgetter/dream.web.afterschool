using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// AfterSchool_Lecture_FilesRepository
    /// </summary>
    public class AfterSchool_Lecture_FilesRepository : BaseRepository
    {
        #region Singleton Instance

        private static volatile AfterSchool_Lecture_FilesRepository _instance;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static AfterSchool_Lecture_FilesRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(AfterSchool_Lecture_FilesRepository))
                    {
                        if (_instance == null)
                        {
                            _instance = new AfterSchool_Lecture_FilesRepository();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion

        /// <summary>
        /// AfterSchool_Lecture_Files 목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Files</param>
        /// <returns></returns>
        public List<AfterSchool_Lecture_Files> AfterSchool_Lecture_FilesSelect(AfterSchool_Lecture_Files entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UploaderID", entity.UploaderID);

                var ret = dbConnection.Query<AfterSchool_Lecture_Files>("AfterSchool_Lecture_FilesSelect", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Files ?�이�?목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Files</param>
        /// <returns></returns>
        public void AfterSchool_Lecture_FilesSelectPaging(ViewModel<AfterSchool_Lecture_Files> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@SortField", entity.SortField);
                parameters.Add("@SortOrder", entity.SortOrder);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.Query<AfterSchool_Lecture_Files>("AfterSchool_Lecture_FilesSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);
                entity.List = ret.ToList();
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Files ??�� 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Files</param>
        /// <returns></returns>
        public AfterSchool_Lecture_Files AfterSchool_Lecture_FilesSelectItem(AfterSchool_Lecture_Files entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UploaderID", entity.UploaderID);

                var ret = dbConnection.Query<AfterSchool_Lecture_Files>("AfterSchool_Lecture_FilesSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Files ?�록
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Files</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_FilesInsert(AfterSchool_Lecture_Files entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UploaderID", entity.UploaderID);
				parameters.Add("@UploaderType", entity.UploaderType);
				parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@FileID", entity.FileID);
				parameters.Add("@FileName", entity.FileName);
				parameters.Add("@FileSize", entity.FileSize);
				parameters.Add("@FilePath", entity.FilePath);
				parameters.Add("@SchCode", entity.SchCode);
				parameters.Add("@IsDelete", entity.IsDelete);
				parameters.Add("@CreateDate", entity.CreateDate);
				parameters.Add("@DeleteDate", entity.DeleteDate);

                return dbConnection.Execute("AfterSchool_Lecture_FilesInsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Files ?�정
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Files</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_FilesUpdate(AfterSchool_Lecture_Files entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UploaderID", entity.UploaderID);
				parameters.Add("@UploaderType", entity.UploaderType);
				parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@FileID", entity.FileID);
				parameters.Add("@FileName", entity.FileName);
				parameters.Add("@FileSize", entity.FileSize);
				parameters.Add("@FilePath", entity.FilePath);
				parameters.Add("@SchCode", entity.SchCode);
				parameters.Add("@IsDelete", entity.IsDelete);
				parameters.Add("@CreateDate", entity.CreateDate);
				parameters.Add("@DeleteDate", entity.DeleteDate);

                return dbConnection.Execute("AfterSchool_Lecture_FilesUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Files ?�록 �??�정
        /// </summary>
        /// <returns></returns>
        public int AfterSchool_Lecture_FilesUpsert(List<AfterSchool_Lecture_Files> entities)
        {
            int rVal = 0;            
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                 foreach (AfterSchool_Lecture_Files entity in entities)
                 {
                    try
                    {
                        DynamicParameters parameters = new DynamicParameters();
                        parameters.Add("@UploaderID", entity.UploaderID);
                        parameters.Add("@LectureSeq", entity.LectureSeq);
                        parameters.Add("@FileType", entity.FileType);
                        parameters.Add("@FileID", entity.FileID);
                        parameters.Add("@FileName", entity.FileName);
                        parameters.Add("@FileSize", entity.FileSize);
                        parameters.Add("@Result", rVal, DbType.Int32, ParameterDirection.Output);


                        dbConnection.Execute("sp_AfterSchool_Lecture_File_Insert", param: parameters, commandType: CommandType.StoredProcedure);
                        rVal = parameters.Get<int>("@Result");
                    }
                    catch (Exception ex)
                    {

                    }
                }
                
                return rVal;
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Files ??��
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Files</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_FilesDelete(AfterSchool_Lecture_Files entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UploaderID", entity.UploaderID);

                return dbConnection.Execute("AfterSchool_Lecture_FilesDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
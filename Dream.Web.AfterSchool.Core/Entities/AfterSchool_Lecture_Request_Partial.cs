using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AfterSchool_Lecture_Request : BaseEntity
    {
        /// <summary>
		/// 개설년도
		/// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 학기
        /// </summary>
        public int Semester { get; set; }

        /// <summary>
		/// 강의명
		/// </summary>
        public string LectureName { get; set; }

        /// <summary>
		/// 수업시작일
		/// </summary>
		public DateTime? OperationStartDate { get; set; }


        /// <summary>
        /// 수업종료일
        /// </summary>
        public DateTime? OperationEndDate { get; set; }

        /// <summary>
        /// 학교명
        /// </summary>
        public string sch_name { get; set; }

        /// <summary>
		/// 대상학년
		/// </summary>
		public string TargetGrade { get; set; }

        /// <summary>
        /// 성별
        /// </summary>
        public int? TargetGender { get; set; }

        /// <summary>
        /// 수업기간
        /// </summary>
        public string LectureTime { get; set; }

        /// <summary>
        /// 시수
        /// </summary>
        public int? MaxTime { get; set; }

        /// <summary>
        /// 현재 신청 인원
        /// </summary>
        public int CurrentSignUp { get; set; }

        /// <summary>
		/// 신청정원
		/// </summary>
        public int MaxSignUp { get; set; }

        /// <summary>
        /// 교과 명
        /// </summary>
        public string CssName { get; set; }

        /// <summary>
        /// 선생님 이름
        /// </summary>
        public string TeacherName { get; set; }

        /// <summary>
        /// 검색용 년, 학기
        /// </summary>
        public string YearSemester { get; set; }

        /// <summary>
        /// 강의 타입
        /// </summary>
        public string LectureType { get; set; }

        /// <summary>
        /// 강의실
        /// </summary>
        public string Place { get; set; }

        /// <summary>
        /// 교과명
        /// </summary>
        public string NssName { get; set; }

        /// <summary>
        /// 알림란
        /// </summary>
        public string InfoMessage { get; set; }
    }
}

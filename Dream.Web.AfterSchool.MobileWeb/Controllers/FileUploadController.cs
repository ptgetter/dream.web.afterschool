﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.MobileWeb.Controllers
{
    public class FileUploadController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        public JsonResult UploadPost()
        {
            return null;
            try
            {
                string filePath = @"D:\IIS\TheSchool.Afterschool.MobileWeb\UploadFiles";
                HttpFileCollectionBase files = Request.Files;

                if (files.Count == 0)
                {
                    try
                    {
                        Stream stream = Request.InputStream;
                        string fileName = HttpUtility.UrlDecode(Request.Headers.Get("Content-Disposition"), System.Text.Encoding.UTF8);
                        if (fileName.IndexOf("filename") > 0)
                        {
                            fileName = fileName.Substring(fileName.IndexOf("filename=\"") + 10, fileName.Length - (fileName.IndexOf("filename=\"") + 11));
                        }
                        else
                        {
                            fileName = "untitle.txt";
                        }

                        
                        

                        string change_Name = Path.Combine(filePath, fileName);

                        if (System.IO.File.Exists(change_Name))
                        {
                            int renameSuffix = 1;
                            string name = fileName.Substring(0, fileName.LastIndexOf("."));
                            string ext = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf("."));

                            while (System.IO.File.Exists(Path.Combine(filePath, string.Format("{0} ({1}){2}", name, renameSuffix.ToString(), ext))))
                            {
                                renameSuffix++;
                            }

                            change_Name = Path.Combine(filePath, string.Format("{0} ({1}){2}", name, renameSuffix.ToString(), ext));
                            //documentLog.FileFullPath = Path.Combine(filePath, fileName);
                            //documentLog.FileName = fileName;
                            //documentLog.FileExtension = ext;
                        }


                        using (FileStream fs = new FileStream(change_Name, FileMode.Create))
                        {
                            stream.Position = 0;
                            stream.CopyTo(fs);
                        }

                        fileName = (new System.IO.FileInfo(fileName)).Name;

                        return Json(new JsonResultValue
                        {
                            result = string.Format("{0} upload complete.", fileName)
                        });
                    }
                    catch (Exception ex)
                    {
                        return Json(new JsonResultValue
                        {
                            result = HttpUtility.UrlDecode(Request.Headers.Get("Content-Disposition"), System.Text.Encoding.UTF8) + "\r\n" + ex.ToString()
                        });
                    }
                }
                else
                {
                    for (int i = 0; i < files.Count; i++)
                    {
                        Stream stream = files[i].InputStream;
                        string fileName = files[i].FileName;
                        string change_Name = Path.Combine(filePath, fileName);
                        int fileId = 0;
                        int.TryParse(files.AllKeys[i], out fileId);

                        if (System.IO.File.Exists(change_Name))
                        {
                            int renameSuffix = 1;
                            string name = fileName.Substring(0, fileName.LastIndexOf("."));
                            string ext = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf("."));

                            while (System.IO.File.Exists(Path.Combine(filePath, string.Format("{0} ({1}){2}", name, renameSuffix.ToString(), ext))))
                            {
                                renameSuffix++;
                            }

                            change_Name = Path.Combine(filePath, string.Format("{0} ({1}){2}", name, renameSuffix.ToString(), ext));
                            //documentLog.FileFullPath = Path.Combine(filePath, fileName);
                            //documentLog.FileName = fileName;
                            //documentLog.FileExtension = ext;
                        }


                        using (FileStream fs = new FileStream(change_Name, FileMode.Create))
                        {
                            stream.Position = 0;
                            stream.CopyTo(fs);
                        }
                    }

                    return Json(new JsonResultValue
                    {
                        result = "ok"
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new JsonResultValue
                {
                    result = ex.ToString()
                });
            }
        }
    }

    public class JsonResultValue
    {
        public string result { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dream.Web.AfterSchool.Core.Util
{
    public class ExcelResult<T> : ActionResult
    {
        public string FileName { get; set; }
        public string CharSet { get; set; }
        public List<T> DataList { get; set; }
        public string[] ColumnTitles { get; set; }
        public DataTable dt { get; set; }
        public bool IsUseDataTable = false;
        public ExcelResult() { }
        public ExcelResult(string fileName, List<T> dataList, string[] columnTitles = null, string charSet = "UTF-8")
        {
            this.FileName = fileName;
            this.DataList = dataList;
            this.CharSet = charSet;
            this.ColumnTitles = columnTitles;
        }

        public ExcelResult(string fileName, DataTable dt = null, string[] columnTitles = null, string charSet = "UTF-8")
        {
            this.FileName = fileName;
            this.dt = dt;
            this.CharSet = charSet;
            this.ColumnTitles = columnTitles;
            this.IsUseDataTable = true;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (dt.Rows?.Count > 0 || DataList?.Count > 0)
            {
                var gv = new GridView();
                if (IsUseDataTable)
                {
                    gv.DataSource = dt;
                }
                else
                {
                    gv.DataSource = DataList;
                }
                gv.DataBind();
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);


                gv.RenderControl(objHtmlTextWriter);

                string htmlData = objStringWriter.ToString();
                if (ColumnTitles != null && ColumnTitles.Length > 0)
                {
                    List<string> fieldList = FpReflection.GetFieldNameList<T>();
                    for (int i = 0; i < ColumnTitles.Length; i++)
                    {
                        htmlData = htmlData.Replace(fieldList[i], ColumnTitles[i]);
                    }
                }

                HttpResponseBase Response = context.HttpContext.Response;
                Response.ClearContent();
                Response.Buffer = true;
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = CharSet;
                Response.ContentEncoding = System.Text.Encoding.GetEncoding(CharSet);
                Response.Output.Write(htmlData);
                Response.Flush();
                Response.End();
            }

            else
            {
                HttpResponseBase Response = context.HttpContext.Response;
                Response.ClearContent();
                Response.Buffer = true;
                Response.Clear();
                Response.Charset = CharSet;
                Response.ContentEncoding = System.Text.Encoding.GetEncoding(CharSet);
                Response.Output.Write("<script>alert('데이터가 없습니다.');history.back();</script>");
                Response.Flush();
                Response.End();
            }
        }
    }

    public class FpReflection
    {
        public static List<string> GetFieldNameList<T>()
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));

            List<string> list = new List<string>(props.Count);

            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                list.Add(prop.Name);
            }

            return list;
        }
    }
}

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// BaseRepository
    /// </summary>
    public class BaseRepository : IDisposable
    {
        #region OpenConnection

        /// <summary>
        /// Open DB Connection
        /// </summary>
        /// <param name="connectionStringName">Connection String Name</param>
        /// <returns>IDbConnection</returns>
        protected IDbConnection OpenConnection(string connectionStringName)
        {
            IDbConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString);
            dbConnection.Open();

            return dbConnection;
        }

        #endregion

        #region OpenConnection

        /// <summary>
        /// Open DB Connection(Default)
        /// </summary>
        /// <returns>IDbConnection</returns>
        protected IDbConnection OpenConnection()
        {
            IDbConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LectureManage"].ConnectionString);
            dbConnection.Open();

            return dbConnection;
        }

        #endregion


        /// <summary>
        /// Dispose
        /// </summary>
        void IDisposable.Dispose()
        {
            
        }
    }
}
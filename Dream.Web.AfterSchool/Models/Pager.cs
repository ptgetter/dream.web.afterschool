﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dream.Web.AfterSchool.Models
{
    public class Pager
    {
        /// <summary>
        /// 현재페이지
        /// </summary>
        public int NowPage { get; set; }
        /// <summary>
        /// 첫 페이지 1
        /// </summary>
        public int FirstPage { get; set; }
        /// <summary>
        /// 마지막 페이지
        /// </summary>
        public int LastPage { get; set; }
        /// <summary>
        /// 페이저에 그려질 시작 페이지
        /// </summary>
        public int StartPage { get; set; }
        /// <summary>
        /// 페이저에 그려질 마지막 페이지
        /// </summary>
        public int EndPage { get; set; }
        /// <summary>
        /// 이전 페이지 블록
        /// </summary>
        public int Prev { get; set; }
        /// <summary>
        /// 다음 페이지 블록
        /// </summary>
        public int Next { get; set; }

        public Pager(PageParam pageParam, long TotalCount)
        {
            NowPage = pageParam.PG;
            int pageBlock = (int)Math.Ceiling((double)NowPage / pageParam.RC);

            FirstPage = 1;
            //LastPage = (int)((TotalCount - 1) / pageParam.RC) + 1;
            LastPage = (int)Math.Ceiling((double)TotalCount / pageParam.RC);

            StartPage = (pageBlock - 1) * pageParam.RC + 1;
            if (StartPage <= 0) StartPage = 1;
            EndPage = StartPage + pageParam.RC - 1;
            if (EndPage > LastPage) EndPage = LastPage;

            Prev = StartPage - pageParam.RC;
            Next = EndPage + 1;
        }
    }
}
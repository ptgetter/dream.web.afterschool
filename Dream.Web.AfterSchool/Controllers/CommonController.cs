﻿using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Repository;

namespace Dream.Web.AfterSchool.Controllers
{
    public class CommonController : BaseController
    {
        public ActionResult UserInformation()
        {
            ViewBag.SubTitle = string.Format("정보수정");
            int midx = 0;
            int.TryParse(AppIdentity.Midx, out midx);

            memberRepository mRepository = new memberRepository();
            SchoolInfosRepository sRepository = new SchoolInfosRepository();

            var user = mRepository.memberSelectItem(new member() { midx = midx });
            var schools = sRepository.SchoolInfosSelect(new SchoolInfos());

            return View(new Tuple<List<SchoolInfos>, member>(schools, user));
        }

        [HttpPost]
        public JsonResult SetUserInformation(member item2)
        {
            try
            {
                memberRepository mRepository = new memberRepository();
                mRepository.memberUpdate(item2);

                var updatemember = mRepository.memberSelectItem(item2);

                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                var emailClaim = AppIdentity.Email;
                var SchoolNameClaim = claims.Where(c => c.Type == "School").SingleOrDefault();
                var userinfoClaim = claims.Where(c => c.Type == "setuserinfo").SingleOrDefault();

                foreach (string claimName in new string[] { "Email", "School", "setuserinfo" })
                {
                    var tClaim = claims.Where(c => c.Type == claimName).SingleOrDefault();
                    if (tClaim != null)
                    {
                        identity.TryRemoveClaim(tClaim);
                    }
                }

                identity.AddClaim(new Claim("Email", updatemember.email));
                identity.AddClaim(new Claim("School", string.IsNullOrEmpty(updatemember.school) ? "드림고등학교" : updatemember.school));

                if (string.IsNullOrEmpty(updatemember.sch_code) || string.IsNullOrEmpty(updatemember.grade) || updatemember.mclass == null || string.IsNullOrEmpty(updatemember.no) || string.IsNullOrEmpty(updatemember.uname) || string.IsNullOrEmpty(updatemember.sch_code))
                {
                    identity.AddClaim(new Claim("setuserinfo", "Y"));
                }

                HttpContext.GetOwinContext().Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties { IsPersistent = false });

                return Json(new
                {
                    ok = true,
                    message = "수정하였습니다."
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = true,
                    message = "오류가 발생하였습니다."
                });
            }
        }
    }
}
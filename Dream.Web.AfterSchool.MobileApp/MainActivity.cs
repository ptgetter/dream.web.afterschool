﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content.PM;
using Android.Webkit;

using Android.Views;
using Android;
using Android.Content;
using Android.Net;
using System.Net;
using Android.Support.V4.Content;
using Android.Media;
using System;
using System.Threading;
using System.Collections.Generic;

namespace Dream.Web.AfterSchool.MobileApp
{
    [Activity(Label = "AfterSchool", Icon = "@drawable/ic_launcher", Theme = "@style/splashscreen", MainLauncher = true, LaunchMode = Android.Content.PM.LaunchMode.SingleTop, ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.KeyboardHidden, WindowSoftInputMode = SoftInput.AdjustResize,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        bool initPageLoad = false;
        const string siteURL = "http://after.futureplan.co.kr/MobileWeb";
        WebView webView;
        Context MainContext = null;

        int requestActivity_Camera = 1000;
        int requestActivity_Gallery = 1001;
        int requestPermission_Camera = 1002;
        int requestPermission_Storage = 1003;
        int requestPermission_Camera_Use = 1004;
        int requestPermission_Storage_Use = 1005;
        int requestPermission_NetworkState = 1006;

        string tempFileName = string.Empty;
        string tempFileUrl = string.Empty;

        public ProgressDialog webViewDialog;

        Dictionary<Guid, WebClient> webClients = new Dictionary<Guid,WebClient>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            this.Window.AddFlags(WindowManagerFlags.ForceNotFullscreen);
            this.Window.ClearFlags(WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            MainContext = this;

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            //CheckPlayServices();

            //var sharedPreferences = Android.Preferences.PreferenceManager.GetDefaultSharedPreferences(this);
            //StoreApplication.deviceToken = sharedPreferences.GetString("PushToken", "");


            webViewDialog = new ProgressDialog(this);
            webViewDialog.Indeterminate = true;
            webViewDialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            webViewDialog.SetMessage("Please wait...");
            webViewDialog.SetCancelable(false);
            //webViewDialog.Show();

            // Get our button from the layout resource,
            // and attach an event to it
            webView = FindViewById<WebView>(Resource.Id.FP_webView);
            webView.SetWebViewClient(new FPWebViewClient(this)); // stops request going to Web Browser
            webView.Settings.UseWideViewPort = true;
            webView.Settings.JavaScriptEnabled = true;
            webView.Settings.AllowFileAccess = true;
            webView.Settings.AllowContentAccess = true;
            webView.Settings.SaveFormData = false;
            webView.Settings.DomStorageEnabled = true;
            webView.SetWebChromeClient(new FPWebChromeClient(this));
            webView.Settings.CacheMode = CacheModes.NoCache;
            webView.Settings.SetAppCacheEnabled(false);
            //webView.SetWebViewClient(new WebViewClientAuthentication());
            webView.AddJavascriptInterface(new JSBridge(this), "JSBridge");
            //webView.Settings.SetPluginState(WebSettings.PluginState.On);
#if DEBUG
            WebView.SetWebContentsDebuggingEnabled(true);
#else
            WebView.SetWebContentsDebuggingEnabled(false);
#endif

            //StoreApplication.deviceOS = "A";
            //if (Intent.GetStringExtra("NotificationData") != null)
            //{
            //    string notiData = Intent.GetStringExtra("NotificationData");
            //    Intent.RemoveExtra("NotificationData");
            //    Intent.PutExtra("waitNotification", notiData);
            //}


            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.M)
            {
                if (CheckSelfPermission(Manifest.Permission.AccessNetworkState) != (int)Permission.Granted)
                {
                    if (ShouldShowRequestPermissionRationale(Manifest.Permission.AccessNetworkState))
                    {
                        RequestPermissions(new string[] { Manifest.Permission.AccessNetworkState }, requestPermission_NetworkState);
                    }
                    else
                    {
                        RequestPermissions(new string[] { Manifest.Permission.AccessNetworkState }, requestPermission_NetworkState);
                    }
                }
                else
                {
                    LoginWebView();
                }
            }
            else
            {
                LoginWebView();
            }
        }

        public class FPWebViewClient : WebViewClient
        {
            MainActivity context;
            public FPWebViewClient(MainActivity pcontext)
            {
                this.context = pcontext;
            }

            public override void OnPageFinished(WebView view, string url)
            {
                base.OnPageFinished(view, url);
                if (!context.initPageLoad && url.IndexOf(siteURL) > -1)
                {


                    if (context.Intent.GetStringExtra("waitNotification") != null)
                    {
                        string notiData = context.Intent.GetStringExtra("waitNotification");
                        context.Intent.RemoveExtra("waitNotification");
                        view.LoadUrl("javascript:setTimeout(function () { onPushNotificationOpen(null, null, '" + notiData + "'); }, 1000)");
                    }
                }
                if (!context.initPageLoad)
                {
                    context.initPageLoad = true;
                    context.RunOnUiThread(() =>
                    {
                        context.webViewDialog.Dismiss();
                    });
                }
            }

            public override void OnReceivedError(WebView view, IWebResourceRequest request, WebResourceError error)
            {
                if (request.Url.ToString().ToLower() == siteURL.ToLower())
                {
                    view.StopLoading();
                    view.LoadUrl("file:///android_asset/notInternetAvailable.html");
                    Toast.MakeText(context, error.Description, ToastLength.Long);
                    return;
                }
                else
                {
                    base.OnReceivedError(view, request, error);
                }
            }

            /*public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
            {
                

                string fileext = string.Empty;

                int extpos = request.Url.ToString().LastIndexOf('.');
                int extlength = request.Url.ToString().Length - (extpos + 1);

                if (request.Url.Scheme.ToLower() == "tel" || (extpos > -1 && (extlength == 4 || extlength == 5)))
                {
                    Intent intent = new Intent(Intent.ActionView, request.Url);
                    context.StartActivity(intent);
                }

                System.Console.WriteLine(request.Url.Scheme);
                return base.ShouldOverrideUrlLoading(view, request);
            }*/

            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {
                int extpos = url.LastIndexOf('.');
                int extlength = url.Length - (extpos + 1);

                if (Android.Net.Uri.Parse(url).Scheme.ToLower() == "tel" || Android.Net.Uri.Parse(url).Scheme.ToLower() == "market")
                {
                    Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
                    context.StartActivity(intent);
                }
                else if (extpos > -1 && (extlength == 3 || extlength == 4))
                {
                    //context.downloadFileUrl = url;
                    //context.DownloadFile(url);
                }
                else if (url.IndexOf(siteURL) > -1)
                {
                    return base.ShouldOverrideUrlLoading(view, url);
                }

                return true;
            }
        }

        public class FPWebChromeClient : WebChromeClient
        {
            MainActivity context;
            public FPWebChromeClient(MainActivity pcontext)
            {
                this.context = pcontext;
            }

            public override bool OnJsAlert(WebView view, string url, string message, JsResult result)
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(context);

                alert.SetTitle("");
                alert.SetMessage(message);
                alert.SetPositiveButton(Android.Resource.String.Ok, (sender, args) =>
                {
                    result.Confirm();
                });
                alert.SetCancelable(false);
                alert.Create();
                alert.Show();
                
                return true;
            }

            public override bool OnJsConfirm(WebView view, string url, string message, JsResult result)
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(context);

                alert.SetTitle("");
                alert.SetMessage(message);
                alert.SetPositiveButton(Android.Resource.String.Ok, (sender, args) =>
                {
                    result.Confirm();
                });
                alert.SetNegativeButton(Android.Resource.String.Cancel, (sender, args) =>
                {
                    result.Cancel();
                });
                alert.SetCancelable(false);
                alert.Create();
                alert.Show();

                return true;
            }
        }

        public void LoginWebView()
        {
            using (ConnectivityManager cm = GetSystemService(Context.ConnectivityService).JavaCast<ConnectivityManager>())
            {
                bool isconnect = false;
                if (cm.ActiveNetworkInfo != null)
                {
                    if (cm.ActiveNetworkInfo.IsConnected)
                    {
                        isconnect = true;
                        webView.Post(new Java.Lang.Runnable(() =>
                        {
                            webView.ClearHistory();
                            webView.ClearCache(true);
                            initPageLoad = false;
                            webViewDialog.Show();
                            webView.LoadUrl(siteURL);
                        }));
                    }
                }
                if (!isconnect)
                {
                    webView.LoadUrl("file:///android_asset/notInternetAvailable.html");
                }
            }
        }

        #region JSBridge

        public void DownloadFromURL(string name, string url)
        {
            if (Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.M)
            {
                RunDownloadUrl(name, url);
            }
            else
            {
                if (CheckSelfPermission(Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
                {
                    tempFileName = name;
                    tempFileUrl = url;

                    if (ShouldShowRequestPermissionRationale(Manifest.Permission.WriteExternalStorage))
                    {
                        RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, requestPermission_Storage_Use);
                    }
                    else
                    {
                        RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, requestPermission_Storage_Use);
                    }
                }
                else
                {
                    RunDownloadUrl(name, url);
                }
            }
            
        }

        #endregion

        #region Public Method

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == requestPermission_Camera_Use)
            {
                if (grantResults.Length == 2 && grantResults[0] == Permission.Granted && grantResults[1] == Permission.Granted)
                {
                    //OpenCamera();
                }
                else
                {
                    //Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(MainContext, Resource.Style.AlertDialogStyle);
                    //alert.SetMessage("카메라 사용 못함!!");
                    //alert.Show();

                    Toast.MakeText(ApplicationContext, "Camera is not available.", ToastLength.Long).Show();
                }
            }
            else if (requestCode == requestPermission_Storage_Use)
            {
                if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                {
                    if (!string.IsNullOrEmpty(tempFileName) && !string.IsNullOrEmpty(tempFileUrl)) {
                        RunOnUiThread(() => { RunDownloadUrl(tempFileName, tempFileUrl); });
                    }
                }
                else if (grantResults.Length == 2 && grantResults[0] == Permission.Granted && grantResults[1] == Permission.Granted)
                {
                    //OpenGallery();
                }
                else
                {
                    //Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(MainContext, Resource.Style.AlertDialogStyle);
                    //alert.SetMessage("저장소 사용 못함!!");
                    //alert.Show();

                    Toast.MakeText(ApplicationContext, "You do not have permission to use the storage.", ToastLength.Long).Show();
                }
            }
            else if (requestCode == requestPermission_NetworkState)
            {
                if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                {
                    LoginWebView();
                }
                else
                {
                    //Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(MainContext, Resource.Style.AlertDialogStyle);
                    //alert.SetMessage("인터넷 연결 상태를 확인할 수 없습니다.");
                    //alert.Show();

                    Toast.MakeText(ApplicationContext, "Internet is not available.", ToastLength.Long).Show();
                }
            }
        }

        #endregion

        #region Private Method

        private void RunDownloadUrl(string name, string url)
        {
            string storagePath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).Path;
            string fileName = name.Substring(0, name.LastIndexOf("."));
            string fileExt = name.Substring(name.LastIndexOf(".") + 1, name.Length - name.LastIndexOf(".") - 1);
            string filePath = System.IO.Path.Combine(storagePath, name);

            for (int i = 1; ; i++)
            {
                if (System.IO.File.Exists(filePath))
                {
                    fileName = name.Substring(0, name.LastIndexOf(".")) + " (" + i.ToString() + ")";
                    filePath = System.IO.Path.Combine(storagePath, fileName + "." + fileExt);
                }
                else
                {
                    break;
                }
            }

            tempFileName = string.Empty;
            tempFileUrl = string.Empty;

            //AutoResetEvent are = new AutoResetEvent(false);

            try
            {
                using (WebClient webClient = new WebClient())
                {
                    //webClient.DownloadDataAsync(new System.Uri(url));
                    //webClient.DownloadDataCompleted += (sender, args) =>
                    //{
                    //    using (System.IO.Stream stream = System.IO.File.Create(filePath))
                    //    {
                    //        stream.Write(args.Result, 0, args.Result.Length);
                    //    }
                    //    are.Set();
                    //};

                    byte[] data = webClient.DownloadData(new System.Uri(url));
                    using (System.IO.Stream stream = System.IO.File.Create(filePath))
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    //are.Set();
                }


                //are.WaitOne();

                Toast.MakeText(ApplicationContext, fileName + "." + fileExt + "\r\ndownload complete.", ToastLength.Short).Show();


                if (System.IO.File.Exists(filePath))
                {
                    MimeTypeMap mtm = MimeTypeMap.Singleton;
                    string mimeType = mtm.GetMimeTypeFromExtension(fileExt);

                    Android.Net.Uri fileUri = FileProvider.GetUriForFile(this, PackageName + ".provider", new Java.IO.File(filePath));

                    MediaScannerConnection.ScanFile(this, new string[] { fileUri.Path }, new string[] { mimeType }, null);

                    try
                    {
                        Intent intent = new Intent();
                        intent.SetAction(Intent.ActionView);
                        intent.SetDataAndType(fileUri, mimeType);
                        intent.AddFlags(ActivityFlags.GrantReadUriPermission);
                        intent.AddFlags(ActivityFlags.NewTask);
                        StartActivity(intent);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(ApplicationContext, "파일 다운로드에 실패하였습니다.", ToastLength.Short).Show();
            }
        }

        #endregion
    }
}
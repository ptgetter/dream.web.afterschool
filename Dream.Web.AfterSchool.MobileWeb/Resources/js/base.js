/* **********************************************
 * ?�임?�페?�스 ?�성
*********************************************** */
; (function (window, $) {
    'use strict';

    var global = "$utils", nameSpace = "school.utils", nameSpaceRoot = null;

    function createNameSpace(identifier, module) {
        var win = window, name = identifier.split('.'), p, i = 0;

        if (!!identifier) {
            for (i = 0; i < name.length; i += 1) {
                if (!win[name[i]]) {
                    if (i === 0) {
                        win[name[i]] = {};
                        nameSpaceRoot = win[name[i]];
                    } else {
                        win[name[i]] = {};
                    }
                }
                win = win[name[i]];
            }
        }
        if (!!module) {
            for (p in module) {
                if (!win[p]) {
                    win[p] = module[p];
                } else {
                    throw new Error("module already exists! >> " + p);
                }
            }
        }
        return win;
    }

    if (!!window[global]) {
        throw new Error("already exists global!> " + global);
    }

    window[global] = createNameSpace(nameSpace, {
        namespace: function (identifier, module) {
            return createNameSpace(identifier, module);
        }
    });
})(window, jQuery);

/* **********************************************
 * PLUGIN [popmodal]
 * ?�이?�팝??
*********************************************** */
; (function (window, $) {
    'use strict';
    var short = '$plugin';

    window[short] = window['$utils'].namespace('school.plugins', {
        popmodal: function (element, options) {
            var version = "0.0.1",
                pluginName = "publish.popmodal",
                methods = {},
                el = element,
                el_idvalue = element.attr('id'),
                el_ev = $('[href="#' + el_idvalue + '"]'),
                length = el.size(),
                pops = [],
                popmodals,
                defaults = {
                    overlay: false,
                    overlay_fixed: false,
                    overlay_click: false,
                    class_overlay: "modalOverlay",
                    class_open: "open",
                    selector_close: '.popClose',
                    selector_return: false,
                    position_top: null,
                    position_target: false,
                    position_auto: true,
                    load_display: false,
                    load_img: false,
                    load_animation: false,
                    load_only: false,
                    load_only_expect: false,
                    scroll_doc: true,
                    scroll_doc_type: 'class',
                    scroll_doc_class: 'popopen',
                    auto_focus: false,
                    modal_type: "modal",
                    callback_before: null,
                    callback_load: null,
                    callback_after: null
                };

            if (length < 1) return;
            if (length > 1) {
                el.each(function (i, tar) {
                    pops.push(window[short].popmodal($(tar), options));
                });
                return pops;
            }
            if (el.data(pluginName)) return;

			/* ---------------------------------------------------------------------------
				popmodal.init : 초기??
			--------------------------------------------------------------------------- */
            methods.init = function () {
                methods.initVars();
                methods.initEvent();
                //methods.validation();
            };

			/* ---------------------------------------------------------------------------
				popmodal.initVars : 변?�선??
			--------------------------------------------------------------------------- */
            methods.initVars = function () {
                el.options = $.extend({}, defaults, options);
                el.vars = {
                    id: pluginName + "-" + new Date().getTime(),
                    pop: null,
                    pop_ev: null,
                    pop_close: null,
                    popWidth: null,
                    popHeight: null,
                    modal: null,
                    modalTop: null,
                    active: false,
                    overflow: null
                };
            };

			/* ---------------------------------------------------------------------------
				popmodal.initEvent : ?�벤?�선??
			--------------------------------------------------------------------------- */
            methods.initEvent = function () {
                el.vars.pop = $("#" + el_idvalue);

                $(document).on("click.popmodal", 'a[href="#' + el_idvalue + '"]', function (event) {
                    event.preventDefault();
                    var href = el_ev.filter("a").attr("href") || el_ev.find("a").attr("href");
                    el.vars.pop_ev = $(this);

                    if (el.options.modal_type == "toggle") {
                        if (!methods.display()) {
                            if (typeof el.options.callback_before === 'function') {
                                if (!el.options.callback_before.call(el.vars.pop_ev)) return;
                            };
                            methods.pop();
                        } else {
                            el.vars.pop_close.trigger('click');
                        }
                    } else {
                        if (!methods.display()) {
                            if (typeof el.options.callback_before === 'function') {
                                if (!el.options.callback_before.call(el.vars.pop_ev)) return;
                            };
                            methods.pop();
                        }
                    }
                });

                if (el.options.load_display) {
                    methods.popCall();
                }
            };

			/* ---------------------------------------------------------------------------
				popmodal.validation :
			--------------------------------------------------------------------------- */
            methods.validation = function () {
            };

			/* ---------------------------------------------------------------------------
				popmodal.pop : ?�업?�출
			--------------------------------------------------------------------------- */
            methods.pop = function () {
                //?�이�?
                el.vars.overflow = $("body").css("overflow");
                el.vars.popWidth = el.vars.pop.width();
                el.vars.popHeight = el.vars.pop.height();
                el.vars.active = true;

                if (el.options.load_only) {
                    popmodals.each(function (index) {
                        if (popmodals[index] != el && !popmodals[index].options.load_only_expect) {
                            popmodals[index].closeOutput();
                        }
                    });
                }

                //리사?�즈
                $(window).resize(function () {
                    methods.setResize();
                });

                //?�업?�픈
                methods.popShow();

                if (el.options.overlay_fixed) {
                    $(window).on("scroll", function () {
                        if (!!el.vars.modal) {
                            el.vars.modal.css({ marginTop: $(window).scrollTop() * (-1) });
                        }
                    });
                }

            };
			/* ---------------------------------------------------------------------------
				popmodal.pop : ?�업?�출 콜함??
			--------------------------------------------------------------------------- */
            methods.popCall = function () {
                el.vars.pop = el;
                if (el.options.selector_return) el.vars.pop_ev = $(el.options.selector_return);
                methods.pop();
            };
			/* ---------------------------------------------------------------------------
				popmodal.popShow : ?�업?�픈
			--------------------------------------------------------------------------- */
            methods.popShow = function () {
                if (el.options.load_only_expect) {
                    $('body').addClass('ui-banner-open');
                }

                if (el.options.load_animation) {
                    el.vars.pop.slideDown(function () {
                        $(this).addClass(el.options.class_open);
                    });
                } else {
                    el.vars.pop.addClass(el.options.class_open);
                }
                if (el.vars.pop_ev != null) el.vars.pop_ev.addClass(el.options.class_open);
                methods.setResize();

                //모달?�성
                methods.modalCreate();

                //?�기버튼 ?�벤???�정
                if (!!el.options.selector_close) {
                    el.vars.pop_close = el.find(el.options.selector_close);
                    if (el.vars.pop_close) {
                        if (el.options.auto_focus) el.vars.pop_close.eq(0).focus();
                        else $(el.vars.pop_ev).focus();
                    }
                    el.vars.pop_close.on("click.popmodal", function (event) {
                        event.preventDefault();
                        methods.close();
                    });
                }

                //				바닥?�이지 ?�크롤설??
                //				if(!el.options.scroll_doc){
                //					$('body').on('touchmove.Modal', function(e){e.preventDefault()});
                //				}else{
                //					$('body').addClass(el.options.scroll_doc_class);
                //					$('html,body').animate({
                //						scrollTop : 0
                //					},0.1);
                //				}

                //?��?지컨텐�?로드??리사?�즈 ?�정
                if (el.options.load_img) {
                    el.vars.pop.find("img").load(function () {
                        methods.setResize();
                    });
                }

                //콜백?�수
                if (typeof el.options.callback_load === 'function') {
                    el.options.callback_load.call(el.vars.pop);
                };

            };
			/* ---------------------------------------------------------------------------
				popmodal.setResize : ?�업?�이즈설??
			--------------------------------------------------------------------------- */
            methods.setResize = function () {
                if (el.options.position_auto) {
                    var browser_width = $(window).width();
                    var browser_height = $(window).height();
                    //var layer_width = el.vars.pop.outerWidth();
                    var layer_width = el.vars.pop.outerWidth();
                    var layer_height = el.vars.pop.outerHeight();
                    //var margin_left = Math.floor(layer_width / 2) * (-1) + 'px';
                    var margin_left =  '0px';
                    var position_top = $(window).scrollTop() + ((browser_height - layer_height) / 2);
                    //var position_top = $(window).scrollTop() + ((browser_height - layer_height) / 2); - 158;
                    //var position_top = ((($(window).height() - layer_height) / 2) + $(window).scrollTop()) - 300 + 'px';
                    
                    //$(window).scrollTop() + ((browser_height-layer_height)/2);
                    //var margin_left = (-1)*(layer_width/2);
                    //var position_left = Math.max(0, (($(window).width() - layer_width) / 2) + $(window).scrollLeft()) + 'px';                   
                    var position_left = '0px';

                    var pop_parent = ($(".contWrap.m").width() + 260) * 0.9;

                    if (browser_height <= layer_height) position_top = $(window).scrollTop();
                    //if (el.options.position_top) position_top = el.options.position_top + $(window).scrollTop();
                    //position_top  = el.options.position_top;

                    if (el.options.position_target) {
                        el.vars.pop.css({
                            "top": el.vars.pop_ev.offset()
                        });
                    } else {
                        el.vars.pop.css({
                            "top": position_top,
                            "left": position_left,
                            "max-width": pop_parent
                        });
                    }
                }
            };
			/* ---------------------------------------------------------------------------
				el.close : ?��??�출 : ?�기
			--------------------------------------------------------------------------- */
            el.closeOutput = function () {
                methods.popHide();
            };
			/* ---------------------------------------------------------------------------
				el.close : ?��??�출 : ?�기
			--------------------------------------------------------------------------- */
            el.openOutput = function (etarget) {
                if (etarget) {
                    if (etarget.tagName == "SPAN") etarget = $(etarget).parent();
                    el.vars.pop_ev = $(etarget);
                }
                methods.popCall();
            };
			/* ---------------------------------------------------------------------------
				el.openCheck : ?�업?�픈?�태
			--------------------------------------------------------------------------- */
            el.openCheck = function () {
                return methods.display();
            };
            methods.display = function () {
                return el.vars.active;
            };
			/* ---------------------------------------------------------------------------
				popmodal.close : ?�업?�기?�출
			--------------------------------------------------------------------------- */
            methods.close = function () {
                if (typeof el.options.callback_after === 'function') {
                    el.options.callback_after.call();
                };
                methods.popHide();
            };
			/* ---------------------------------------------------------------------------
				popmodal.popHide : ?�업?�기
			--------------------------------------------------------------------------- */
            methods.popHide = function () {
                $(window).off("resize.popmodal");

                if (el.options.load_only_expect) {
                    $('body').removeClass('ui-banner-open');
                }

                if (!!el.vars.pop) {
                    if (el.options.load_animation) {
                        el.vars.pop.slideUp(function () {
                            $(this).removeClass(el.options.class_open);
                        });
                    } else {
                        el.vars.pop.removeClass(el.options.class_open);
                    }
                    if (el.vars.pop_ev != null) el.vars.pop_ev.removeClass(el.options.class_open);
                }

                methods.modalRemove();

                if (!!el.vars.this_close) {
                    el.vars.this_close.off("click.popmodal");
                }
                el.vars.active = false;

                if (!!el.vars.pop_ev) el.vars.pop_ev.focus();

                if (!el.options.scroll_doc) $('body').unbind('touchmove.Modal');
                else {
                    $('body').removeClass(el.options.scroll_doc_class);
                }
            };
			/* ---------------------------------------------------------------------------
				popmodal.modalCreate : 모달?�성
			--------------------------------------------------------------------------- */
            methods.modalCreate = function (zindex) {
                if (!!el.options.overlay) {
                    var id = el_idvalue + "Overlay";
                    if (!el.vars.modal) {
                        var modal_el = $('<div id="' + id + '" class="' + el.options.class_overlay + '"></div>')
                        el.before(modal_el);
                        el.vars.modal = modal_el;

                        el.vars.modal = el.vars.modal.css({
                            "width": $(document).width(),
                            "height": $(document).height()
                        });

                        if (el.options.overlay_fixed) {
                            el.vars.modal.css({ marginTop: $(window).scrollTop() * (-1) });
                        }

                        if (el.options.overlay_click) {
                            el.vars.modal.on("click.popmodal", function (event) {
                                methods.close();
                            });
                        }
                    }
                }
            };
			/* ---------------------------------------------------------------------------
				popmodal.modalRemove : 모달??��
			--------------------------------------------------------------------------- */
            methods.modalRemove = function () {
                if (!!el.vars.modal) {
                    el.vars.modal.off("click.popmodal");
                    el.vars.modal.remove();
                    el.vars.modal = null;
                }
            };

            methods.init();

            popmodals = $(document).data(pluginName);
            if (!popmodals) {
                popmodals = $([]);
            }

            if ($.inArray(el, popmodals) === -1) {
                popmodals.push(el);
            }
            $(document).data(pluginName, popmodals);
            el.data(pluginName, el);
            return el;
        }
    });
})(window, jQuery);

$(document).ready(function () {
	/* lnb wrap */
    $(".lnbWrap > .lnbBtn").on("click",function(){
        var $this = $(this);
		$this.parent().toggleClass("open");
		$this.parents("#container").find(".contWrap").toggleClass("m");
		$("#footer").toggleClass("m");
	});

	/* division */
    $(".division .diviClose").on("click",function(){
        var $this = $(this);
		$this.next("div").slideToggle();
		$this.toggleClass("open");
	});

	/* lnb toggle ?�팅 */
//	var lnbL = $(".lnbWrap li").length;
//	for ( var i = 0; i < lnbL + 1; i = i++ ){
//		$(".lnbWrap li").addClass("no");
//		if($(".lnbWrap ul a").next()){
//			console.log($(".lnbWrap ul a").next());
//			var $this = $(".lnbWrap ul a").next();
//			$this.find(" > li").removeClass("no");
//		}
//		return false;
//	}

	/* lnb toggle */
	$(".lnb a").next("ul").css("display","none");
	$(".lnb li.on > a").next("ul").css("display","block");
    $(".lnb a").on("click",function(){
        var $this = $(this);
		$this.next("ul").slideToggle();
		$this.parent("li").toggleClass("on");
	});

//	/* gnb*/
//	$("#gnb .before").on("click",function(){
//		var $this = $(this);
//		$(".gnb").slideToggle();
//	});
//	if($( window ).width() > 1101){
//		$(".gnb").css("display","block");
//	}
//	$( window ).resize(function() {
//		if($( window ).width() > 1101){
//			$(".gnb").css("display","block");
//		}
//		if($( window ).width() < 1100){
//			$(".gnb").css("display","none");
//		}
//	});

	//?�일 ?�로??
	var fileTarget = $('.fileBox .uploadHidden');
	fileTarget.on('change', function(){
		if(window.FileReader){
			var filename = $(this)[0].files[0].name;
		} else {
			var filename = $(this).val().split('/').pop().split('\\').pop();
		}
		$(this).siblings('.upload').val(filename);
	});

	//?�세?�보�??�이�?
	$(".detailTable .btn.view").on("click",function(){
		var $dTable = $(this).parents().find(".detailTable");
		$dTable.find(".dtailT").toggleClass("on");
    });

    /* ?�상?�요?�간 */
    $(".popBox .box .title .btn").on("click", function () {
        var $this = $(this);
        $this.parent().next().slideToggle();
        $this.toggleClass("open");
    });

    /* 관?��??�추가 */
    $(".btn.Ty4.add").on("click", function () {
        var $this = $(this);
        $this.toggleClass("on");
    });
});
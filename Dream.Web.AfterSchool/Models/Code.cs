﻿using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.Models
{
    public class Code
    {
        public static SelectList Year(int year = 2019, bool isEmpty = true)
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            //if (isEmpty)
            //{
            //    dic.Add(0, text);
            //}

            int CurrentYear = DateTime.Now.Year;
            for (int k = year; k <= (CurrentYear+1); k++)
            {
                dic.Add(k, k + "년");
            }

            return new SelectList(dic, "key", "value");
        }

        public static SelectList YearCurr(bool isEmpty = true)
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            //if (isEmpty)
            //{
            //    dic.Add(0, text);
            //}

            int CurrentYear = DateTime.Now.Year;
            for (int k = CurrentYear; k <= (CurrentYear+1); k++)
            {
                dic.Add(k, k + "년");
            }

            return new SelectList(dic, "key", "value");
        }


        public static SelectList Semester(bool isEmpty = true, bool isAllSearch = false)
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            //if (isAllSearch)
            //{
            //    dic.Add(-1, "전체학기");
            //}

            //if (isEmpty)
            //{
            //    dic.Add(0, text);
            //}

            int CurrentYear = DateTime.Now.Year;
            dic.Add(0, "전학기");
            //dic.Add(0, "전체");
            dic.Add(1, 1 + "학기");
            dic.Add(2, 2 + "학기");

            return new SelectList(dic, "key", "value");
        }

        public static SelectList GetOfficeOfEducation()
        {
            AfterSchool_LectureRepository repository = new AfterSchool_LectureRepository();
            Dictionary<string, string> dic = new Dictionary<string, string>();

            dic.Add("", "전체");

            //if (isEmpty)
            //{
            //    dic.Add(0, text);
            //}
            List<Region> regions = repository.GetRegion(string.Empty);
            foreach (Region region in regions)
            {
                dic.Add(region.RegionCode, region.OfficeofEducationName);
            }
            return new SelectList(dic, "key", "value");

        }

        public static SelectList CssCode()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            dic.Add("", "교과선택");
            dic.Add("10", "국어교과");
            dic.Add("30", "수학교과");
            dic.Add("70", "영어교과");
            dic.Add("22", "사회교과");
            dic.Add("40", "과학교과");
            dic.Add("55", "음악교과");
            dic.Add("56", "미술교과");
            dic.Add("50", "체육교과");
            dic.Add("90", "기타");
            
            return new SelectList(dic, "key", "value");
        }
    }

}
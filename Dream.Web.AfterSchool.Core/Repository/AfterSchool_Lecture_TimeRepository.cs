using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// AfterSchool_Lecture_TimeRepository
    /// </summary>
    public class AfterSchool_Lecture_TimeRepository : BaseRepository
    {
        /// <summary>
        /// AfterSchool_Lecture_Time 목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Time</param>
        /// <returns></returns>
        public List<AfterSchool_Lecture_Time> AfterSchool_Lecture_TimeSelect(AfterSchool_Lecture_Time entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@LectureTimeSeq", entity.LectureTimeSeq);

                var ret = dbConnection.Query<AfterSchool_Lecture_Time>("AfterSchool_Lecture_TimeSelect", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Time 페이징 목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Time</param>
        /// <returns></returns>
        public void AfterSchool_Lecture_TimeSelectPaging(ViewModel<AfterSchool_Lecture_Time> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@SortField", entity.SortField);
                parameters.Add("@SortOrder", entity.SortOrder);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.Query<AfterSchool_Lecture_Time>("AfterSchool_Lecture_TimeSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);
                entity.List = ret.ToList();
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Time 항목 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Time</param>
        /// <returns></returns>
        public AfterSchool_Lecture_Time AfterSchool_Lecture_TimeSelectItem(AfterSchool_Lecture_Time entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@LectureTimeSeq", entity.LectureTimeSeq);

                var ret = dbConnection.Query<AfterSchool_Lecture_Time>("AfterSchool_Lecture_TimeSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Time 등록
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Time</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_TimeInsert(AfterSchool_Lecture_Time entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@LectureTimeSeq", entity.LectureTimeSeq);
				parameters.Add("@DayOfTheWeek", entity.DayOfTheWeek);
				parameters.Add("@StartTime", entity.StartTime);
				parameters.Add("@EndTime", entity.EndTime);
				parameters.Add("@IsDelete", entity.IsDelete);
				parameters.Add("@RegID", entity.RegID);
				parameters.Add("@RegDate", entity.RegDate);
				parameters.Add("@ModifyID", entity.ModifyID);
				parameters.Add("@ModifyDate", entity.ModifyDate);

                return dbConnection.Execute("AfterSchool_Lecture_TimeInsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Time 수정
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Time</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_TimeUpdate(AfterSchool_Lecture_Time entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@LectureTimeSeq", entity.LectureTimeSeq);
				parameters.Add("@DayOfTheWeek", entity.DayOfTheWeek);
				parameters.Add("@StartTime", entity.StartTime);
				parameters.Add("@EndTime", entity.EndTime);
				parameters.Add("@IsDelete", entity.IsDelete);
				parameters.Add("@RegID", entity.RegID);
				parameters.Add("@RegDate", entity.RegDate);
				parameters.Add("@ModifyID", entity.ModifyID);
				parameters.Add("@ModifyDate", entity.ModifyDate);

                return dbConnection.Execute("AfterSchool_Lecture_TimeUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Time 등록 및 수정
        /// </summary>
        /// <returns></returns>
        public int AfterSchool_Lecture_TimeUpsert(AfterSchool_Lecture_Time entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@LectureTimeSeq", entity.LectureTimeSeq);
				parameters.Add("@DayOfTheWeek", entity.DayOfTheWeek);
				parameters.Add("@StartTime", entity.StartTime);
				parameters.Add("@EndTime", entity.EndTime);
				parameters.Add("@IsDelete", entity.IsDelete);
				parameters.Add("@RegID", entity.RegID);
				parameters.Add("@RegDate", entity.RegDate);
				parameters.Add("@ModifyID", entity.ModifyID);
				parameters.Add("@ModifyDate", entity.ModifyDate);

                return dbConnection.Execute("AfterSchool_Lecture_TimeUpsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Time 삭제
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Time</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_TimeDelete(AfterSchool_Lecture_Time entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@LectureTimeSeq", entity.LectureTimeSeq);

                return dbConnection.Execute("AfterSchool_Lecture_TimeDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// SchoolInfosRepository
    /// </summary>
    public class SMSRepository : BaseRepository
    {
        #region Singleton Instance

        private static volatile SMSRepository _instance;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static SMSRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(SMSRepository))
                    {
                        if (_instance == null)
                        {
                            _instance = new SMSRepository();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion
        
        public int SMSValidate(string email, string code, string deviceid, string phone)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                try
                {
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DeviceId", deviceid);
                    parameters.Add("@Email", email);
                    parameters.Add("@SMSCode", code);
                    parameters.Add("@Phone", phone);
                    parameters.Add("@OUTPUT", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    var ret = dbConnection.ExecuteScalar("SMS_Validate", param: parameters, commandType: CommandType.StoredProcedure);

                    return parameters.Get<int>("@OUTPUT");
                }
                catch (Exception ex) {
                    return 0;
                }
            }
        }

        public bool SMSRequest(string email, string code, string deviceid, string phone)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                try
                {
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DeviceId", deviceid);
                    parameters.Add("@Email", email);
                    parameters.Add("@SMSCode", code);
                    parameters.Add("@Phone", phone);

                    var ret = dbConnection.ExecuteScalar("SMS_Request", param: parameters, commandType: CommandType.StoredProcedure);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
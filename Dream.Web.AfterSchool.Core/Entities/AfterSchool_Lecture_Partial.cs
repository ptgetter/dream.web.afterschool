using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    public partial class AfterSchool_Lecture : BaseEntity
    {
        /// <summary>
        /// 현재 수강 신청 인원
        /// </summary>
        public int CurrentSignUp { get; set; }

        /// <summary>
        /// 개설학교 명
        /// </summary>
        public string sch_name { get; set; }

        /// <summary>
        /// 신청자 RequestSeq
        /// </summary>
        public int RequestSeq { get; set; }

        /// <summary>
        /// 현재 사용자 idx
        /// </summary>
        public string midx { get; set; }

        /// <summary>
        /// 교과 명
        /// </summary>
        public string CssName { get; set; }

        /// <summary>
        /// 수강 신청 상태
        /// </summary>
        public int ReqState { get; set; }

        /// <summary>
        /// 수강 신청 성별
        /// </summary>
        public string TargetGradeStr { get; set; }

        /// <summary>
        /// 검색용 년, 학기
        /// </summary>
        public string YearSemester { get; set; }
    }
}

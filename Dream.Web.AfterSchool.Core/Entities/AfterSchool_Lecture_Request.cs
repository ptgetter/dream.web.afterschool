using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("AfterSchool_Lecture_Request")]
    public partial class AfterSchool_Lecture_Request : BaseEntity
    {
        /// <summary>
		/// 강의 신청 Seq
		/// </summary>
		[Key]
        [Required]
        public long RequestSeq { get; set; }


        /// <summary>
        /// Member 테이블 idx
        /// </summary>
        [Required]
        public int midx { get; set; }


        /// <summary>
        /// 학교코드
        /// </summary>
        [StringLength(20)]
        public string SchCode { get; set; }


        /// <summary>
        /// 강의 Seq
        /// </summary>
        [Required]
        public long LectureSeq { get; set; }


        /// <summary>
        /// 강의 지원(신청) 동기
        /// </summary>
        [Required]
        public string Motive { get; set; }


        /// <summary>
        /// 상태(0: 대기, 1: 수강)
        /// </summary>
        [Required]
        public int State { get; set; }

        public member Member { get; set; }



        #region |MLML|

        public string SchoolName { get; set; }
        public string Grade { get; set; }
        public string Class { get; set; }
        public string StudentId { get; set; }
        public string Uname { get; set; }
        public string Phone { get; set; }
        public List<AfterSchool_Lecture_Files> AfterSchoolLectureFiles { get; set; }

        #endregion
    }
}

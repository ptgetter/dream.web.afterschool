using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// SchoolInfosRepository
    /// </summary>
    public class PortalPopupRepository : BaseRepository
    {
        #region Singleton Instance

        private static volatile PortalPopupRepository _instance;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static PortalPopupRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(PortalPopupRepository))
                    {
                        if (_instance == null)
                        {
                            _instance = new PortalPopupRepository();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion
        /// <summary>
        /// SchoolInfos 목록 조회
        /// </summary>
        /// <param name="entity">SchoolInfos</param>
        /// <returns></returns>
        public PortalPopup PortalPopupSelect()
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                var ret = dbConnection.QueryFirst<PortalPopup>("sp_PortalPopup_Select", commandType: CommandType.StoredProcedure);
                return ret;
            }
        }

        public AfterSchool_URL AfterSchoolURL()
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                var ret = dbConnection.QueryFirst<AfterSchool_URL>("sp_AfterSchoolURL_Select", commandType: CommandType.StoredProcedure);
                return ret;
            }
        }
    }
}
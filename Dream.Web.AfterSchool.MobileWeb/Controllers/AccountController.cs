﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Dream.Web.AfterSchool.MobileWeb.Models;
using Dream.Web.AfterSchool.Core.Repository;
using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Util;

namespace Dream.Web.AfterSchool.MobileWeb.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel() { Title = "로그인" });
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            memberRepository repository = new memberRepository();

            member loginMember = repository.LoginMember(new member() { email = model.Email.Trim(), pwd = WEBEncryptHelper.Encrypt(model.Password) });

            if (loginMember == null)
            {
                ModelState.AddModelError("", "잘못된 로그인 시도입니다.");
                return View(model);
            }
            else
            {
                if (loginMember.mtype != "S")
                {
                    ModelState.AddModelError("", "학생만 로그인 가능합니다.");
                    return View(model);
                }

                var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Email, ClaimTypes.Role);
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginMember.midx.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Name, loginMember.uname));
                identity.AddClaim(new Claim("schoolName", string.IsNullOrEmpty(loginMember.school) ? "학교" : loginMember.school));
                identity.AddClaim(new Claim(ClaimTypes.Email, model.Email));
                identity.AddClaim(new Claim(ClaimTypes.Role, "AfterSchool"));

                if (string.IsNullOrEmpty(loginMember.sch_code) || string.IsNullOrEmpty(loginMember.grade) || loginMember.mclass == null || string.IsNullOrEmpty(loginMember.no) || string.IsNullOrEmpty(loginMember.uname) || string.IsNullOrEmpty(loginMember.sch_code))
                {
                    identity.AddClaim(new Claim("setuserinfo", "Y"));
                }
                
                //if (loginMember.)
                //identity.AddClaim(new Claim(""))

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);

                return RedirectToLocal(returnUrl);
            }


            // 계정이 잠기는 로그인 실패로 간주되지 않습니다.
            // 암호 오류 시 계정 잠금을 트리거하도록 설정하려면 shouldLockout: true로 변경하십시오.
            //var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            //switch (result)
            //{
            //    case SignInStatus.Success:
            //        return RedirectToLocal(returnUrl);
            //    case SignInStatus.LockedOut:
            //        return View("Lockout");
            //    case SignInStatus.RequiresVerification:
            //        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
            //    case SignInStatus.Failure:
            //    default:
            //        ModelState.AddModelError("", "잘못된 로그인 시도입니다.");
            //        return View(model);
            //}
        }

        //
        // POST: /Account/LogOff
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        public ActionResult Signin(string returnUrl)
        {
            return RedirectToAction("Login", new { returnUrl = returnUrl });
        }

        #region 도우미
        // 외부 로그인을 추가할 때 XSRF 보호에 사용
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
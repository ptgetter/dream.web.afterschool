﻿using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Dream.Web.AfterSchool.Core.Entities;

namespace Dream.Web.AfterSchool.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        IAuthenticationManager Authentication
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName.ToLower();
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();

            if (!(actionName == "logout" && controllerName == "account") && !((actionName == "userinformation" || actionName == "setuserinformation") && controllerName == "common"))
            {
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                if (claims.Where(c => c.Type == "setuserinfo").Select(c => c.Value).SingleOrDefault() == "Y")
                {
                    filterContext.Result = new RedirectResult("https://jdream.jne.co.kr/UAccount/AccountMngUser");
                    return;
                }
            }

            base.OnActionExecuting(filterContext);
        }


        //public async Task SetAdminAuthorization(member member = null)
        //{
        //    var identity = new ClaimsIdentity(
        //        DefaultAuthenticationTypes.ApplicationCookie,
        //        ClaimTypes.NameIdentifier,
        //        ClaimTypes.Role
        //        );
        //    if (member != null)
        //    {
        //        identity.TryRemoveClaim(identity.FindFirst("Name"));
        //        identity.TryRemoveClaim(identity.FindFirst("Grade"));
        //        identity.TryRemoveClaim(identity.FindFirst("UnitCode"));
        //        identity.TryRemoveClaim(identity.FindFirst("UserType"));
        //        identity.TryRemoveClaim(identity.FindFirst("School"));
        //        identity.TryRemoveClaim(identity.FindFirst("MIdx"));
        //        identity.TryRemoveClaim(identity.FindFirst("Email"));

        //        identity.AddClaim(new Claim("Name", "관리자"));
        //        identity.AddClaim(new Claim("Grade", "3"));
        //        identity.AddClaim(new Claim("UserType", "A"));
        //        identity.AddClaim(new Claim("School", "전남교육청"));
        //        identity.AddClaim(new Claim("MIdx", Common.CheckNullOrEmpty(member.midx.ToString())));
        //        identity.AddClaim(new Claim("Email", Common.CheckNullOrEmpty(member.email)));
                
        //        var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

        //        if (authenticationContext != null)
        //            Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
        //                identity, authenticationContext.Properties);

        //        Authentication.SignIn(new AuthenticationProperties
        //        {
        //            IsPersistent = true,
        //            ExpiresUtc = DateTime.UtcNow.AddHours(24)
        //        },
        //        identity);
        //    }
        //    else
        //    {
        //        ViewBag.Javascript = "<script language='javascript' type='text/javascript'>alert('회원정보설정에 실패하였습니다.\r\n고객지원팀에 문의하세요.');location.history(-1);</script>";
        //    }
        //}

        public async Task SetTeacherAuthorization(member member = null)
        {
            var identity = new ClaimsIdentity(
                DefaultAuthenticationTypes.ApplicationCookie,
                ClaimTypes.NameIdentifier,
                ClaimTypes.Role
                );

            if (member != null)
            {
                identity.TryRemoveClaim(identity.FindFirst("Name"));
                identity.TryRemoveClaim(identity.FindFirst("Grade"));
                identity.TryRemoveClaim(identity.FindFirst("UnitCode"));
                identity.TryRemoveClaim(identity.FindFirst("UnitCodeName"));
                identity.TryRemoveClaim(identity.FindFirst("UserType"));
                identity.TryRemoveClaim(identity.FindFirst("School"));
                identity.TryRemoveClaim(identity.FindFirst("SchoolType"));
                identity.TryRemoveClaim(identity.FindFirst("SchoolTypeName"));
                identity.TryRemoveClaim(identity.FindFirst("MIdx"));
                identity.TryRemoveClaim(identity.FindFirst("Email"));

                identity.AddClaim(new Claim("Name", Common.CheckNullOrEmpty(member.uname)));
                identity.AddClaim(new Claim("Grade", member.grade));
                //identity.AddClaim(new Claim("UnitCode", member.UnitCode));
                //identity.AddClaim(new Claim("UnitCodeName", member.UnitCodeName));
                identity.AddClaim(new Claim("UserType", "T"));
                identity.AddClaim(new Claim("School", Common.CheckNullOrEmpty(member.school)));
                //identity.AddClaim(new Claim("SchoolType", member.SchoolType));
                //identity.AddClaim(new Claim("SchoolTypeName", member.SchoolTypeName));
                identity.AddClaim(new Claim("MIdx", Common.CheckNullOrEmpty(member.midx.ToString())));
                identity.AddClaim(new Claim("Email", Common.CheckNullOrEmpty(member.email)));
                //identity.AddClaim(new Claim("ConsultPostion", Common.CheckNullOrEmpty(member.ConsultPosition, "---")));


                var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                if (authenticationContext != null)
                    Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                        identity, authenticationContext.Properties);

                Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(24)
                },
                identity);
            }
            else
            {
                ViewBag.Javascript = "<script language='javascript' type='text/javascript'>alert('회원정보설정에 실패하였습니다.\r\n고객지원팀에 문의하세요.');location.history(-1);</script>";
            }
        }

        public async Task SetStudentAuthorization(member member = null)
        {
            var identity = new ClaimsIdentity(
                DefaultAuthenticationTypes.ApplicationCookie,
                ClaimTypes.NameIdentifier,
                ClaimTypes.Role
                );
            if (member != null)
            {
                identity.TryRemoveClaim(identity.FindFirst("Name"));
                identity.TryRemoveClaim(identity.FindFirst("Grade"));
                identity.TryRemoveClaim(identity.FindFirst("UnitCode"));
                identity.TryRemoveClaim(identity.FindFirst("UnitCodeName"));
                identity.TryRemoveClaim(identity.FindFirst("UserType"));
                identity.TryRemoveClaim(identity.FindFirst("School"));
                identity.TryRemoveClaim(identity.FindFirst("SchoolType"));
                identity.TryRemoveClaim(identity.FindFirst("SchoolTypeName"));
                identity.TryRemoveClaim(identity.FindFirst("MIdx"));
                identity.TryRemoveClaim(identity.FindFirst("Email"));

                identity.AddClaim(new Claim("Name", Common.CheckNullOrEmpty(member.uname)));
                identity.AddClaim(new Claim("Grade", Common.CheckNullOrEmpty(member.grade)));
                identity.AddClaim(new Claim("UnitCode", Common.CheckNullOrEmpty(member.UnitCode)));
                //identity.AddClaim(new Claim("UnitCodeName", Common.CheckNullOrEmpty(member.UnitCodeName)));
                identity.AddClaim(new Claim("UserType", "S"));
                identity.AddClaim(new Claim("School", Common.CheckNullOrEmpty(member.school)));
                identity.AddClaim(new Claim("SchoolType", Common.CheckNullOrEmpty(member.SchoolType)));
                //identity.AddClaim(new Claim("SchoolTypeName", Common.CheckNullOrEmpty(member.SchoolTypeName)));
                identity.AddClaim(new Claim("MIdx", Common.CheckNullOrEmpty(member.midx.ToString())));
                identity.AddClaim(new Claim("Email", Common.CheckNullOrEmpty(member.email)));

                var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                if (authenticationContext != null)
                    Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                        identity, authenticationContext.Properties);

                Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(24)
                },
                identity);
            }
            else
            {
                ViewBag.Javascript = "<script language='javascript' type='text/javascript'>alert('회원정보설정에 실패하였습니다.\r\n고객지원팀에 문의하세요.');location.history(-1);</script>";
            }
        }

        public async Task SetAuthorization(member member)
        {
            var identity = new ClaimsIdentity(
                DefaultAuthenticationTypes.ApplicationCookie,
                ClaimTypes.NameIdentifier,
                ClaimTypes.Role
                );
            if (member != null)
            {
                identity.TryRemoveClaim(identity.FindFirst("Name"));
                identity.TryRemoveClaim(identity.FindFirst("Grade"));
                identity.TryRemoveClaim(identity.FindFirst("UnitCode"));
                identity.TryRemoveClaim(identity.FindFirst("UnitCodeName"));
                identity.TryRemoveClaim(identity.FindFirst("UserType"));
                identity.TryRemoveClaim(identity.FindFirst("School"));
                identity.TryRemoveClaim(identity.FindFirst("SchoolType"));
                identity.TryRemoveClaim(identity.FindFirst("SchoolTypeName"));
                identity.TryRemoveClaim(identity.FindFirst("MIdx"));
                identity.TryRemoveClaim(identity.FindFirst("Email"));
                identity.TryRemoveClaim(identity.FindFirst("ConsultPostion"));

                identity.AddClaim(new Claim("Name", Common.CheckNullOrEmpty(member.uname)));
                identity.AddClaim(new Claim("Grade", Common.CheckNullOrEmpty(member.grade)));
                identity.AddClaim(new Claim("UnitCode", Common.CheckNullOrEmpty(member.UnitCode)));
                identity.AddClaim(new Claim("UnitCodeName", Common.CheckNullOrEmpty(member.UnitCodeName)));
                identity.AddClaim(new Claim("UserType", Common.CheckNullOrEmpty(member.mtype)));
                identity.AddClaim(new Claim("School", Common.CheckNullOrEmpty(member.school)));
                identity.AddClaim(new Claim("SchoolType", Common.CheckNullOrEmpty(member.SchoolType)));
                identity.AddClaim(new Claim("SchoolTypeName", Common.CheckNullOrEmpty(member.SchoolTypeName)));
                identity.AddClaim(new Claim("MIdx", Common.CheckNullOrEmpty(member.midx.ToString())));
                identity.AddClaim(new Claim("Email", Common.CheckNullOrEmpty(member.email)));
                identity.AddClaim(new Claim("ConsultPostion", Common.CheckNullOrEmpty(member.ConsultPosition.HasValue ? member.ConsultPosition.Value.ToString() : "")));

                // 연합교육과정
                if (member.mtype == "A" || member.mtype == "T")
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, member.Role.ToString()));
                }
                else
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, "STUDENT"));
                }
                identity.AddClaim(new Claim("IsUsed", member.IsUsed.ToString()));

                var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                if (authenticationContext != null)
                    Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                        identity, authenticationContext.Properties);

                Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(24)
                },
                identity);
            }
            else
            {
                ViewBag.Javascript = "<script language='javascript' type='text/javascript'>alert('회원정보설정에 실패하였습니다.\r\n로그인 한 시간이 오래 경과되었다면 다시 로그인을 해주시기 바랍니다.');location.history(-1);</script>";
            }
        }
    }

    public class Common
    {
        public static string CheckNullOrEmpty(string obj)
        {
            return string.IsNullOrEmpty(obj) ? string.Empty : obj;
        }

        public static string CheckNullOrEmpty(string obj, string replace)
        {
            return string.IsNullOrEmpty(obj) ? replace : obj;
        }
    }

    public class AppIdentity
    {
        public static ClaimsIdentity ManualIdentityClaims;

        protected static ClaimsIdentity _identityClaims
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return ManualIdentityClaims;
                }
                return HttpContext.Current.User.Identity as ClaimsIdentity;
            }
        }

        public static string UserType
        {
            get
            {
                var role= _identityClaims.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault();
                return role;
            }
        }

        public static string Midx
        {
            get
            {
                var role = _identityClaims.Claims.Where(c => c.Type == "MIdx").Select(c => c.Value).SingleOrDefault();
                return role;
            }
        }

        public static string SchoolName
        {
            get
            {
                var role = _identityClaims.Claims.Where(c => c.Type == "School").Select(c => c.Value).SingleOrDefault();
                return role;
            }
        }

        public static string Email
        {
            get
            {
                var email = _identityClaims.Claims.Where(c => c.Type == "Email").Select(c => c.Value).SingleOrDefault();
                return email;
            }
        }

        // 기존 UserType (A, T1, S, 이런거)
        public static string MemberType
        {
            get
            {
                var mtype = _identityClaims.Claims.Where(c => c.Type == "mtype").Select(c => c.Value).SingleOrDefault();
                return mtype;
            }
        }
    }    
}
﻿using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.Controllers
{
    public class LectureRequestController : BaseController
    {
        /// <summary>
        /// 강의 리스트
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult LectureList(ViewModel<AfterSchool_Lecture> model, string selectYear = null, int lectureType = 0, int page = 1)
        {
            if (model == null)
            {
                model = new ViewModel<AfterSchool_Lecture>();
            }
            if (model.Search == null)
            {
                model.Search = new AfterSchool_Lecture();
            }
            if (!string.IsNullOrEmpty(selectYear))
            {
                model.Search.YearSemester = selectYear;
            }
            model.Search.LectureType = lectureType;


            ViewBag.LectureType = model.Search.LectureType;
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            ViewBag.SubTitle = string.Format("{0} 강의 강의신청", SetLectureType(model.Search.LectureType));

            AfterSchool_LectureRepository repository = new AfterSchool_LectureRepository();

            model.PageIndex = page;
            model.PageSize = 15;
            //model.PageIndex = search.PageIndex;
            //model.SearchField = search.SearchField == null ? "" : search.SearchField;
            //model.SearchText = search.SearchText;
            //model.Search = new AfterSchool_Lecture();
            //model.Search.LectureType = search.Search.LectureType;
            model.Search.midx = AppIdentity.Midx;
            model.Search.Year = string.IsNullOrEmpty(model.Search.YearSemester) ? DateTime.Now.Year : int.Parse(model.Search.YearSemester.Split('|')[0]);
            model.Search.Semester = string.IsNullOrEmpty(model.Search.YearSemester) ? (DateTime.Now.Month < 7 ? 1 : 2) : int.Parse(model.Search.YearSemester.Split('|')[1]);

            repository.AfterSchool_LectureSelectPaging(model);

            return View(model);
        }

        /// <summary>
        /// 강의 신청 페이지
        /// </summary>
        /// <param name="lectureSeq"></param>
        /// <returns></returns>
        public ActionResult LectureWrite(int lectureSeq = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            int midx = 0;
            int.TryParse(AppIdentity.Midx, out midx);
            ViewBag.SubTitle = "강의신청";

            AfterSchool_LectureRepository lRepository = new AfterSchool_LectureRepository();
            memberRepository mRepository = new memberRepository();

            var user = mRepository.memberSelectItem(new member() { midx = midx });
            var lecture = lRepository.AfterSchool_LectureSelectItem(new AfterSchool_Lecture() { LectureSeq = lectureSeq });

            try
            {
                int grade = 0;

                // 신청 가능한지 검사
                if (lecture.MaxSignUp <= lecture.CurrentSignUp)
                {
                    ViewBag.ErrorMsg = "신청 인원이 초과되었습니다.";
                    return View();
                }
                else if (int.TryParse(user.grade, out grade) && lecture.TargetGrade.Length <= grade && lecture.TargetGrade[grade] == '1')
                {
                    ViewBag.ErrorMsg = "신청 대상 학년이 아닙니다.";
                    return View();
                }
                else if (lecture.TargetSchCodes.IndexOf(user.sch_code) < 0)
                {
                    ViewBag.ErrorMsg = "신청 대상 학교가 아닙니다.";
                    return View();
                }
            }
            catch { }

            Tuple<AfterSchool_Lecture, member> model = new Tuple<AfterSchool_Lecture, member>(lecture, user);

            return View(model);
        }

        /// <summary>
        /// 나의 수강신청 리스트
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public ActionResult LectureMyList(ViewModel<AfterSchool_Lecture_Request> model, string selectYear = null, int page = 1)
        {
            if (model == null)
            {
                model = new ViewModel<AfterSchool_Lecture_Request>();
            }
            if (model.Search == null)
            {
                model.Search = new AfterSchool_Lecture_Request();
            }
            if (!string.IsNullOrEmpty(selectYear))
            {
                model.Search.YearSemester = selectYear;
            }

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            ViewBag.SubTitle = string.Format("나의 수강신청");
            int midx = 0;
            int.TryParse(AppIdentity.Midx, out midx);

            AfterSchool_LectureRepository repository = new AfterSchool_LectureRepository();
            model.PageIndex = page;

            AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();

            model.PageSize = 15;
            //model.PageIndex = search.PageIndex;
            //model.SearchField = search.SearchField == null ? "" : search.SearchField;
            //model.SearchText = search.SearchText;
            model.Search = new AfterSchool_Lecture_Request();
            model.Search.midx = midx;
            model.Search.Year = string.IsNullOrEmpty(model.Search.YearSemester) ? DateTime.Now.Year : int.Parse(model.Search.YearSemester.Split('|')[0]);
            model.Search.Semester = string.IsNullOrEmpty(model.Search.YearSemester) ? (DateTime.Now.Month < 7 ? 1 : 2) : int.Parse(model.Search.YearSemester.Split('|')[1]);

            rRepository.AfterSchool_Lecture_RequestSelectPaging(model);

            return View(model);
        }
       
        /// <summary>
        /// 강의 신청
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LectureRequestInsert(AfterSchool_Lecture_Request request, member user)
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                int midx = 0;
                int.TryParse(AppIdentity.Midx, out midx);

                AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();
                memberRepository mRepository = new memberRepository();

                request.midx = midx;
                request.Motive = request.Motive ?? "";
                user.midx = midx;
                mRepository.memberUpdate(user);
                user = mRepository.memberSelectItem(user);



                int rtn = rRepository.AfterSchool_Lecture_RequestInsert(request);


                if (rtn == 0)
                {
                    return Json(new
                    {
                        ok = true,
                        message = "신청하였습니다."
                    });

                }
                else if (rtn == 1)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "신청 인원을 초과 하였습니다."
                    });
                }
                else if (rtn == 2)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "이미 신청한 강의입니다."
                    });
                }
                else if (rtn == 3)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "신청 조건이 맞지 않습니다."
                    });
                }
                else
                {
                    return Json(new
                    {
                        ok = false,
                        message = "오류가 발생하였습니다."
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = string.Format("오류가 발생 했습니다.\r\n{0}", ex.ToString())
                });
            }
        }

        /// <summary>
        /// 강의 신청 취소
        /// </summary>
        /// <param name="requestSeq"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LectureRequestCancel(long requestSeq)
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                int midx = 0;
                int.TryParse(AppIdentity.Midx, out midx);

                AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();

                int rtn = rRepository.AfterSchool_Lecture_RequestDelete(new AfterSchool_Lecture_Request { midx = midx, RequestSeq = requestSeq });


                if (rtn == -1)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "오류가 발생 했습니다"
                    });
                }
                else
                {
                    return Json(new
                    {
                        ok = true,
                        message = "취소 했습니다."
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = string.Format("오류가 발생 했습니다.\r\n{0}", ex.ToString())
                });
            }
        }

        string SetLectureType(int lectureType)
        {
            if (lectureType == 0)
            {
                return "개방형 ";
            }
            else
            {
                return "학교연합형 ";
            }
        }
    }
}
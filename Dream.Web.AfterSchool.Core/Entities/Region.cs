using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("Region")]
    public partial class Region : BaseEntity
    {
        /// <summary>
		/// 지역코드
		/// </summary>
		[Required]
        [StringLength(50)]
        public string RegionCode { get; set; }


        /// <summary>
        /// 지역이름
        /// </summary>
        [Required]
        public string RegionName { get; set; }


        /// <summary>
        /// 상위지역코드
        /// </summary>
        [Required]
        [StringLength(50)]
        public string UpperCode { get; set; }


        /// <summary>
        /// 상위지역이름
        /// </summary>
        public string UpperName { get; set; }


        /// <summary>
        /// 교육청이름
        /// </summary>
        public string OfficeofEducationName { get; set; }


    }
}

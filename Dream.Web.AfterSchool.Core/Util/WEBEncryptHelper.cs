﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEBEncrypt;

namespace Dream.Web.AfterSchool.Core.Util
{
    public static class WEBEncryptHelper
    {
        const string seedKey = "SNU20131021SUBSTN";

        /// <summary>
        /// 지정된 키로 문자열 암호화
        /// </summary>
        /// <param name="pStr"></param>
        /// <returns></returns>
        public static string Encrypt(string pStr)
        {
            return WEncrypt.Enc(pStr, seedKey);
        }

        /// <summary>
        /// 지정된 키로 문자열 복호화
        /// </summary>
        /// <param name="pStr"></param>
        /// <returns></returns>
        public static string Decrypt(string pStr)
        {
            return WEncrypt.Dec(pStr, seedKey);
        }
    }
}

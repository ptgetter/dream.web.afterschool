var topmidx = "";
var topName = "";
function InitGNB(v1, v2) {
    topmidx = "hfdkshjfkdsljfa" + v1 + "fjdkjfksdf";
    topName = v2;

    if (getVersion_IE() > 0) {
        $("#emptyIE").height(167);
    }

    $("div.menu > a").on("click", function (e) {
        e.preventDefault();
        GoURLWithIdx($(this)[0].href, $(this)[0].getAttribute("newTab"));
    });

    $("ul.submenu > li > a").on("click", function (e) {
        e.preventDefault();
        GoURLWithIdx($(this)[0].href, $(this)[0].getAttribute("newTab"));
    });

    $("a[name=golinksso]").on("click", function (e) {
        e.preventDefault();
        GoURLWithIdx($(this)[0].href, $(this)[0].getAttribute("newTab"));
    });


    $("div.menu > a").on("mouseover", function () {
        $(".topmenu").each(function () {
            $(this).removeClass("on");
        });
        $(this).parent().parent().addClass("on");
    });

    $("ul.submenu > li > a").on("mouseover", function () {
        $(this).parent().addClass("on");
    }).on("mouseout", function () {
        $(this).parent().removeClass("on");
    });

    $("ul.submenu > li").each(function () {
        if ($(this).attr("actionName") == v2) {
            $(this).addClass("select");
            $(this).parent().parent().addClass("on");
            $curMenu = $(this);
        }
    });



    if ($curMenu != null) {
        $("div[name=navarea]").on("mouseleave", function () {
            $(".topmenu").each(function () {
                $(this).removeClass("on");
            });
            $curMenu.addClass("select");
            $curMenu.parent().parent().addClass("on");
        });
    }
}

function GoURLWithIdx(url, newTab) {
    let locationURL = url;
    if (url.indexOf(window.location.origin) != 0) {
        if (/^(http|https):\/\/consult.futureplan.co.kr/gi.test(url)) {
            locationURL = "http://consult.futureplan.co.kr/Account/Login?m=" + topmidx + "&url=" + url;
        }
        else if (/^(http|https):\/\/after.futureplan.co.kr/gi.test(url)) {
            locationURL = "http://after.futureplan.co.kr/Account/sso?m=" + topmidx + "&url=" + url;
        }
    }

    if (newTab) {
        window.open(url, "_blank");
    }
    else {
        window.location = locationURL;
    }
}

function GoURL(url, newTab) {
    if (newTab) {
        window.open(url, "_blank");
    }
    else {
        window.location = url;
    }
}

function getVersion_IE() {
    const agent = navigator.userAgent.toLowerCase();

    if (navigator.appName == "Microsoft Internet Explorer") {
        return 10;
    }
    else if (agent.search("trident") > -1) {
        return 11;
    }
    else {
        return -1;  // No IE
    }
}
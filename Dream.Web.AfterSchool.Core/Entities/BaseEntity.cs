using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
    public partial class BaseEntity
    {
		/// <summary>
        /// 번호
        /// </summary>
        public long RowNum { get; set; }
    }
}

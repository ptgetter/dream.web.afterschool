﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dream.Web.AfterSchool.Core.Entities
{
    [Table("AfterSchool_Lecture_School")]
    public partial class AfterSchool_Lecture_School : BaseEntity
    {
        public string SchoolCode { get; set; }
        public string SchoolName { get; set; }
    }
}

﻿using PagedList;
using System.Collections.Generic;
using LectureManage.Entities;

namespace LectureManage.Models
{
    public class LectureManageModel
    {
    }

    #region LectureList

    public class SearchLectureManageModel : BaseModel
    {
        public SearchCondition search { get; set; }
        public ViewModel<AfterSchool_Lecture> entity { get; set; }
        public StaticPagedList<AfterSchool_Lecture> pageList { get; set; }
    }

    public class SearchCondition
    {
        public int page { get; set; }
        public int lectureType { get; set; }
        public string selectYear { get; set; }
        public string selectField { get; set; }
        public string searchText { get; set; }
        public List<YearField> period { get; set; }
        public List<SearchField> searchField { get; set; }
        public List<StateField> stateField { get; set; }
    }

    public class YearField
    {
        public string year_semester { get; set; }
        public string yearString
        {
            get
            {
                return string.Format("{0}년 {1}학기", year_semester.Split('|')[0], year_semester.Split('|')[1]);
            }
        }
    }

    public class SearchField
    {
        public string fieldValue { get; set; }
        public string fieldName { get; set; }
    }

    public class StateField
    {
        public int stateCode { get; set; }
        public string stateName { get; set; }
    }

    #endregion

    #region LectureRequest

    public class LectureRequestModel : BaseModel
    {
        public AfterSchool_Lecture lecture { get; set; }
        public member user { get; set; }
        public List<string> telcom { get; set; }
        public List<SchoolInfo> school { get; set; }
    }

    public class SchoolInfo
    {
        public string schoolCode { get; set; }
        public string schoolName { get; set; }
    }

    #endregion

    #region LectureRequestList

    public class SearchLectureRequstModel : BaseModel
    {
        public SearchCondition search { get; set; }
        public ViewModel<AfterSchool_Lecture_Request> entity { get; set; }
        public StaticPagedList<AfterSchool_Lecture_Request> pageList { get; set; }
    }

    #endregion
}
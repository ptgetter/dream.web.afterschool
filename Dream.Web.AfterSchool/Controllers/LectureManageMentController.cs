﻿using Dream.Web.AfterSchool.Models;
using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Util;
using Dream.Web.AfterSchool.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.Controllers
{
    [Authorize]
    public class LectureManageMentController : BaseController
    {
        // GET: LectureManageMent
        public ActionResult Index()
        {
            if (AppIdentity.UserType == "STUDENT")
            {
                return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
            }

            return View();
        }

        public ActionResult Detail(int type, int seq)
        {
            //if (AppIdentity.UserType == "STUDENT")
            //{
            //    return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
            //}
            AfterSchool_Lecture model = new AfterSchool_Lecture();
            model = AfterSchool_LectureRepository.Instance.GetAfterSchoolLecture(type,seq)[0];
            ViewBag.SubTitle = "강의정보";
            return View(model);
        }

        public ActionResult DetailLecture()
        {
            if (AppIdentity.UserType == "STUDENT")
            {
                return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
            }
            ViewBag.SubTitle = "강의정보";
            return View();
        }

        public ActionResult List(int type, AfterShoolLectureSearch search=null, int page = 1)
        {
            if (AppIdentity.UserType == "STUDENT")
            {
                return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
            }
            List<AfterSchool_Lecture> model = new List<AfterSchool_Lecture>();

            if (search == null)
            {
                //해당년도로 기본바인딩 되게 수정필요
                search = new AfterShoolLectureSearch { Year = DateTime.Now.Year.ToString() };
            }
            else if (search.Year == null)
            {
                search.Year = DateTime.Now.Year.ToString();
            }

            search.LectureType = type.ToString();
            ViewModel<AfterSchool_Lecture, AfterShoolLectureSearch> v_model = new ViewModel<AfterSchool_Lecture, AfterShoolLectureSearch>()
            {
                List = model
               ,Search = search
               ,PageSize = 10
               ,PageIndex = page
            };
            AfterSchool_LectureRepository.Instance.GetAfterSchoolLecture(v_model);
            ViewBag.SubTitle = SetLectureType(type) +"강의정보";
            ViewBag.LectureType = type;

            ViewBag.Year = Code.Year();
            ViewBag.Semester = Code.Semester();
            ViewBag.CssCode = Code.CssCode();
            return View(v_model);
        }

        public ActionResult Modify(int type, int seq=0)
        {
            if (AppIdentity.UserType == "STUDENT")
            {
                return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
            }
            ViewBag.SubTitle = SetLectureType(type) + "강의정보";

            List<AfterSchool_Lecture> models = new List<AfterSchool_Lecture>();
            AfterSchool_Lecture model = new AfterSchool_Lecture();

            if (seq > 0)
            {
                models = AfterSchool_LectureRepository.Instance.GetAfterSchoolLecture(type, seq);
                if (models.Count() > 0)
                {
                    model = models[0];

                }
            }

            ViewBag.LectureType = type;
            ViewBag.LectureSeq = seq;
            ViewBag.Year =  Code.Year();
            ViewBag.Semester = Code.Semester();
            ViewBag.CssCode = Code.CssCode();            

            return View(model);
        }

        public  ActionResult StudentList(int seq = 0)
        {
            if (AppIdentity.UserType == "STUDENT")
            {
                return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
            }
            ViewBag.SubTitle = "강의 신청현황";
            AfterSchool_Lecture entity = new AfterSchool_Lecture();
            entity.LectureSeq = seq;
            List<AfterSchool_Lecture_Request> list = new List<AfterSchool_Lecture_Request>();
            ViewModel<AfterSchool_Lecture_Request, AfterSchool_Lecture> model = new ViewModel<AfterSchool_Lecture_Request, AfterSchool_Lecture>()
            {
                List = list
               ,Search = entity
            };

            AfterSchool_LectureRepository.Instance.GetAfterShcoolLectureRequest(model);

            return View(model);
        }

        public ActionResult SendFile (int type, AfterShoolLectureSearch search = null)
        {
            if (AppIdentity.UserType == "STUDENT")
            {
                return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
            }
            if (search == null)
            {
                search = new AfterShoolLectureSearch();
            }
            search.LectureType = type.ToString();
            List<SchoolInfos> schoolItems = AfterSchool_LectureRepository.Instance.GetAfterSchoolItem(search);
            ViewBag.CssCode = Code.CssCode();
            ViewBag.LectureItem = AfterSchool_LectureRepository.Instance.GetAfterSchoolLectureItem(search);
            ViewBag.CreateSchoolItem = schoolItems.Where(x => x.SchType == "C");
            ViewBag.SchoolItem = schoolItems.Where(x => x.SchType == "R");
            return View();
        }

        [HttpPost]
        public JsonResult InsertAftterSchoolLecture(AfterSchool_Lecture model)
        {
            if (AppIdentity.UserType == "STUDENT")
            {
                return Json(new { ok = false, styleMessage = ApplicationMessages.Success("권한이 없습니다."), seq = "0" });
            }
            try
            {
                int count = 0;
                model.RegID = AppIdentity.Email;
                model.LectureTime = model.LectureTime.Substring(0, model.LectureTime.Length - 1).ToString();
                //model.LectureTime.Replace("월", "1/")
                //    .Replace("화", "2/")
                //    .Replace("수", "3/")
                //    .Replace("목", "4/")
                //    .Replace("금", "5/")
                //    .Replace("토", "6/")
                //    .Replace("일", "7/").Replace(":", "/").Replace(":", "/");
                if (model.LectureType == 1)
                {
                    model.TargetSchCodes = model.TargetSchCodes.Substring(0, model.TargetSchCodes.Length - 1).ToString();
                }
                count = AfterSchool_LectureRepository.Instance.SetAfterSchoolLecture(model);
                //임시                
                //return Redirect("Detail?seq=" + count.ToString() + "&type=" + model.LectureType.ToString());
                //return Json(new { ok = false, styleMessage = ApplicationMessages.Success("저장에 실패했습니다.") });
                //return View();
                return Json(new { ok = true, styleMessage = ApplicationMessages.Success("저장 했습니다."), seq = count.ToString() });
            }
            catch (Exception e)
            {
                return Json(new { ok = false, styleMessage = ApplicationMessages.Success("저장에 실패했습니다."), seq="0" });
            }
        }

        public ActionResult SchoolList(AfterShoolLectureSearch search, string modal="sel", int page=1)
        {            
            ViewBag.School = Code.GetOfficeOfEducation();
            ViewBag.ModalType = modal;
            if (search == null)
            {
                ViewModel<SchoolInfos, AfterShoolLectureSearch> model = new ViewModel<SchoolInfos, AfterShoolLectureSearch>()
                {
                    Search = new AfterShoolLectureSearch()
                    {
                        RegionCode = ""
                        ,SchoolName = ""
                    }
                    ,
                    PageSize = 7
                    ,PageIndex = page
                };
                AfterSchool_LectureRepository.Instance.GetSchoolnfos(model);
                return View(model);
            }
            else
            {
                ViewModel<SchoolInfos, AfterShoolLectureSearch> model = new ViewModel<SchoolInfos, AfterShoolLectureSearch>()
                {
                    Search = new AfterShoolLectureSearch()
                    {
                        RegionCode = search.RegionCode
                        ,SchoolName = search.SchoolName
                    }
                    ,PageSize = 7
                    ,PageIndex = page
                };
                AfterSchool_LectureRepository.Instance.GetSchoolnfos(model);               
                return View(model);
            }
        }

        public ActionResult SettingTime()
        {
            return View();
        }

        public ActionResult FileUpload()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult UploadFile()
        {
            string[] arrFileType = Request.Form["lecture_file_type"].ToString().Split(',');
            List<AfterSchool_Lecture_Files> entities = new List<AfterSchool_Lecture_Files>();
            try
            {
                int i = 0;
                foreach (string fle in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    int lectureSeq = Convert.ToInt32(Request.Form["___seq___"].ToString());
                    string filetype = arrFileType[i]; 
                    string fileId = Guid.NewGuid().ToString();
                    string fileName = file.FileName;
                    i++;

                    string pathForSaving = Server.MapPath("~/Uploads");
                    file.SaveAs(Path.Combine(pathForSaving, fileId));

                    AfterSchool_Lecture_Files entity = new AfterSchool_Lecture_Files();
                    entity.UploaderID = "admin";
                    entity.LectureSeq = lectureSeq;
                    entity.FileID = fileId;
                    entity.FileName = fileName;
                    entity.FileSize = file.ContentLength.ToString();
                    entity.FileType = filetype;

                    entities.Add(entity);

                }

                int rVal = AfterSchool_Lecture_FilesRepository.Instance.AfterSchool_Lecture_FilesUpsert(entities);

                if (rVal == 1)
                {
                    return Json(new
                    {
                        result = "T"
                    });
                }
                else
                {
                    return Json(new
                    {
                        result = "F"
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    result = "F"
                });
            }

        }

        public string SetLectureType(int lectureType)
        {
            if (lectureType == 0)
            {
                return "개방형 ";
            }
            else
            {
                return "학교연합형 ";
            }
        }
        
        [HttpPost]
        public JsonResult DeleteAfterSchoolLecture(int lectureSeq)
        {
            try
            {            
                string admin = "admin" ;

                int rVal = AfterSchool_LectureRepository.Instance.DeleteAfterSchoolLecture(lectureSeq, admin);
                if (rVal == 1)
                {
                    return Json(new
                    {
                        result = "T"
                    });
                }
                else
                {
                    return Json(new
                    {
                        result = "F"
                    });
                }
            }
            catch
            {
                return Json(new
                {
                    result = "F"
                });
            }

        }

        private string GetUploadFolder()
        {
            string folder = "/Uploads/";
            //folder = String.Format("data/");            
            return Server.MapPath(folder);
        }

        [HttpGet]
        public ActionResult Download(int seq)
        {
            AfterSchool_Lecture_Files f = AfterSchool_LectureRepository.Instance.GetFileSaveName(seq);

            string _fullName = Path.Combine(string.Format(GetUploadFolder() + @"{0}", f.FileID));
            FileInfo ofile = new FileInfo(_fullName);

            // 실제 물리적인 경로에 파일이 존재하는지 체크
            if (ofile.Exists)
            {
                try
                {
                    string sContentType = string.Empty;

                    if (Request.UserAgent.IndexOf("MSIE") >= 0)
                    {
                        //IE 5.0인 경우.
                        if (Request.UserAgent.IndexOf("MSIE 5.0") >= 0)
                        {
                            sContentType = "application/x-msdownload";
                        }
                        //IE 5.0이 아닌 경우.
                        else
                        {
                            sContentType = "Application/Octet-Stream";
                        }
                    }
                    else
                    {
                        //Netscape등 기타 브라우저인 경우.
                        sContentType = "application/unknown";
                    }
                    //Response.ContentEncoding = System.Text.Encoding.Default; 
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    //확인해봐야겠굼
                    Response.HeaderEncoding = Encoding.GetEncoding("utf-8");
                    //HttpContext.Current.Response.HeaderEncoding = Encoding.UTF8;
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(f.FileName));
                    //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + "a.zip");
                    Response.AddHeader("Content-Length", ofile.Length.ToString());
                    Response.ContentType = sContentType;

                    Response.TransmitFile(_fullName);

                    Response.Flush();
                    Response.End();
                }
                catch (ThreadAbortException ex)
                {
                    //fLog.Error(LogManager.WriteLog(ex));
                }
                catch (Exception ex)
                {
                    //fLog.Error(LogManager.WriteLog(ex, "server_nm=" + server_nm + "save_nm=" + save_nm));
                }
            }
            //파일이 없을경우
            else
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "", string.Format("alert('{0}')", HttpContext.GetGlobalResourceObject("Resource", "aspx_cs_content2", culture)), true);
            }
            ViewBag.FullName = _fullName;
            return View();
        }

        [HttpGet]
        public ActionResult StatDown(int type, int detail, string key)
        {            

            DataTable dt = new DataTable();

            string _type = string.Empty;
            string _detail = string.Empty;
            if (type == 1)
            {
                _type = "신청통계";
            }
            else
            {
                _type = "신청학생";
            }

            if(detail == 1)
            {
                _detail = "강의별";
            }
            else if(detail == 2)
            {
                _detail = "학교별";
            }
            else
            {
                _detail = "수업실시학교";
            }

            string fileName = string.Format("수강신청통계자료_{0}_{1}.xls",_type,_detail);

            if (type == 1)
            {
                List<AfterSchool_Lecture> lstAfterLecture = new List<AfterSchool_Lecture>();
                string[] headerTitleList = null;
                int columnIdx = 0;

                if (detail == 1)
                {
                    headerTitleList = new string[] { "연번", "학년도", "학기", "교과", "강의명", "수업실시학교", "정원", "신청" };
                    lstAfterLecture = AfterSchool_LectureRepository.Instance.GetState(type, detail, key);
                    foreach (string columName in headerTitleList)
                    {
                        if (columnIdx > headerTitleList.Length - 1)
                        {
                            break;
                        }
                        columnIdx++;
                        dt.Columns.Add(columName);
                    }
                    foreach (AfterSchool_Lecture sr in lstAfterLecture)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = sr.RowNum;
                        dr[1] = sr.Year; 
                        dr[2] = string.Format("{0}학기", sr.Semester.ToString().Replace("0", "전")); 
                        dr[3] = sr.CssName; 
                        dr[4] = sr.LectureName; 
                        dr[5] = sr.CreateSchName; 
                        dr[6] = sr.MaxSignUp; 
                        dr[7] = sr.CurrentSignUp;
                        dt.Rows.Add(dr);
                    }
                    return new ExcelResult<AfterSchool_Lecture>(fileName, dt, headerTitleList, "UTF-8");
                }
                else if (detail == 2)
                {
                    headerTitleList = new string[] { "연번", "학년도", "학기", "학교", "교과", "강의명", "신청" };
                    lstAfterLecture = AfterSchool_LectureRepository.Instance.GetState(type, detail, key);
                    foreach (string columName in headerTitleList)
                    {
                        if (columnIdx > headerTitleList.Length - 1)
                        {
                            break;
                        }
                        columnIdx++;
                        dt.Columns.Add(columName);
                    }
                    foreach (AfterSchool_Lecture sr in lstAfterLecture)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = sr.RowNum;
                        dr[1] = sr.Year;
                        dr[2] = string.Format("{0}학기", sr.Semester.ToString().Replace("0", "전"));
                        dr[3] = sr.CreateSchName;
                        dr[4] = sr.CssName; 
                        dr[5] = sr.LectureName;
                        dr[6] = sr.CurrentSignUp;
                        dt.Rows.Add(dr);
                    }
                    return new ExcelResult<AfterSchool_Lecture>(fileName, dt, headerTitleList, "UTF-8");
                }
                else
                {
                    headerTitleList = new string[] { "연번", "학년도", "학기", "수업실시학교", "교과", "강의명", "정원", "신청" };
                    lstAfterLecture = AfterSchool_LectureRepository.Instance.GetState(type, detail, key);
                    foreach (string columName in headerTitleList)
                    {
                        if (columnIdx > headerTitleList.Length - 1)
                        {
                            break;
                        }
                        columnIdx++;
                        dt.Columns.Add(columName);
                    }
                    foreach (AfterSchool_Lecture sr in lstAfterLecture)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = sr.RowNum;
                        dr[1] = sr.Year;
                        dr[2] = string.Format("{0}학기", sr.Semester.ToString().Replace("0", "전"));
                        dr[3] = sr.CreateSchName;
                        dr[4] = sr.CssName;
                        dr[5] = sr.LectureName;
                        dr[6] = sr.MaxSignUp;
                        dr[7] = sr.CurrentSignUp; 
                        dt.Rows.Add(dr);
                    }
                    return new ExcelResult<AfterSchool_Lecture>(fileName, dt, headerTitleList, "UTF-8");
                }
            }

            else
            {
                List<AfterSchool_Lecture_Request> lstAfterLecture = new List<AfterSchool_Lecture_Request>();
                string[] headerTitleList = null;
                int columnIdx = 0;
                if (detail == 1)
                {
                    headerTitleList = new string[] { "연번", "학년도", "학기", "교과", "강의명", "학교", "학년", "반", "번호", "이름", "연락처" };
                    lstAfterLecture = AfterSchool_LectureRepository.Instance.GetState_User(type, detail, key);
                    foreach (string columName in headerTitleList)
                    {
                        if (columnIdx > headerTitleList.Length - 1)
                        {
                            break;
                        }
                        columnIdx++;
                        dt.Columns.Add(columName);
                    }
                    foreach (AfterSchool_Lecture_Request sr in lstAfterLecture)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = sr.RowNum;
                        dr[1] = sr.Year; 
                        dr[2] = string.Format("{0}학기", sr.Semester.ToString().Replace("0", "전")); 
                        dr[3] = sr.CssName; 
                        dr[4] = sr.LectureName; 
                        dr[5] = sr.SchoolName; 
                        dr[6] = sr.Grade; 
                        dr[7] = sr.Class;
                        dr[8] = sr.StudentId;
                        dr[9] = sr.Uname;
                        dr[10] = sr.Phone;
                        dt.Rows.Add(dr);
                    }
                    return new ExcelResult<AfterSchool_Lecture>(fileName, dt, headerTitleList, "UTF-8");
                }
                else if (detail == 2)
                {
                    headerTitleList = new string[] { "연번", "학년도", "학기", "학교", "교과", "강의명", "학년", "반", "번호", "이름", "연락처" };
                    lstAfterLecture = AfterSchool_LectureRepository.Instance.GetState_User(type, detail, key);
                    foreach (string columName in headerTitleList)
                    {
                        if (columnIdx > headerTitleList.Length - 1)
                        {
                            break;
                        }
                        columnIdx++;
                        dt.Columns.Add(columName);
                    }
                    foreach (AfterSchool_Lecture_Request sr in lstAfterLecture)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = sr.RowNum;
                        dr[1] = sr.Year;
                        dr[2] = string.Format("{0}학기", sr.Semester.ToString().Replace("0", "전"));
                        dr[3] = sr.SchoolName;
                        dr[4] = sr.CssName;
                        dr[5] = sr.LectureName;
                        dr[6] = sr.Grade; 
                        dr[7] = sr.Class;
                        dr[8] = sr.StudentId;
                        dr[9] = sr.Uname;
                        dr[10] = sr.Phone;
                        dt.Rows.Add(dr);
                    }
                    return new ExcelResult<AfterSchool_Lecture>(fileName, dt, headerTitleList, "UTF-8");
                }
                else
                {
                    headerTitleList = new string[] { "연번", "학년도", "학기", "수업실시학교", "교과", "강의명", "학년", "반", "번호", "이름", "연락처" };
                    lstAfterLecture = AfterSchool_LectureRepository.Instance.GetState_User(type, detail, key);
                    foreach (string columName in headerTitleList)
                    {
                        if (columnIdx > headerTitleList.Length - 1)
                        {
                            break;
                        }
                        columnIdx++;
                        dt.Columns.Add(columName);
                    }
                    foreach (AfterSchool_Lecture_Request sr in lstAfterLecture)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = sr.RowNum;
                        dr[1] = sr.Year;
                        dr[2] = string.Format("{0}학기", sr.Semester.ToString().Replace("0", "전"));
                        dr[3] = sr.SchoolName;
                        dr[4] = sr.CssName;
                        dr[5] = sr.LectureName;
                        dr[6] = sr.Grade;
                        dr[7] = sr.Class;
                        dr[8] = sr.StudentId;
                        dr[9] = sr.Uname;
                        dr[10] = sr.Phone;
                        dt.Rows.Add(dr);
                    }
                    return new ExcelResult<AfterSchool_Lecture>(fileName, dt, headerTitleList, "UTF-8");
                }
            }
        }

        [HttpGet]
        public ActionResult StudentListDown(int seq)
        {
            DataTable dt = new DataTable();
            string fileName = string.Empty;
            string[] headerTitleList = null;
            //headerTitleList = new string[] { "SchoolName", "Grade", "Class", "StudentId", "Uname","Phone","Motive","State" };
            headerTitleList = new string[] { "학교명", "학년", "반", "번호", "이름", "전화번호", "동기", "상태" };


            AfterSchool_Lecture entity = new AfterSchool_Lecture();
            entity.LectureSeq = seq;
            List<AfterSchool_Lecture_Request> list = new List<AfterSchool_Lecture_Request>();
            ViewModel<AfterSchool_Lecture_Request, AfterSchool_Lecture> model = new ViewModel<AfterSchool_Lecture_Request, AfterSchool_Lecture>()
            {
                List = list
               ,
                Search = entity
            };

            AfterSchool_LectureRepository.Instance.GetAfterShcoolLectureRequest(model);


            dt = SetStudentListToDataTable(model.List, headerTitleList);
            string y = DateTime.Now.Year.ToString();
            string m = DateTime.Now.Month.ToString();
            string d = DateTime.Now.Day.ToString();
            

            fileName = string.Format("{0}_{1}_{2}_{3}.xls",model.Search.LectureName.Replace(" ",""),y,m,d);
            return new ExcelResult<AfterSchool_Lecture_Request>(fileName, dt, headerTitleList, "UTF-8");

        }

        private static DataTable SetStatsToDataTable(List<AfterSchool_Lecture> list, string[] headerTitleList)
        {
            DataTable dt = new DataTable();
            int columnIdx = 0;
            foreach (string columName in headerTitleList)
            {
                if (columnIdx > headerTitleList.Length - 1)
                {
                    break;
                }
                columnIdx++;
                dt.Columns.Add(columName);
            }
            //headerTitleList = new string[] { "Year", "Semester", "CssCode", "LectureName", "CreateSchCode" };
            //headerTitleList = new string[] { "년도", "학기", "교과명", "강의명", "개설학교" };
            foreach (AfterSchool_Lecture sr in list)
            {
                DataRow dr = dt.NewRow();
                dr["년도"] = sr.Year;
                dr["학기"] = string.Format("{0}학기", sr.Semester.ToString().Replace("0", "전"));
                dr["교과명"] = sr.CssName;
                dr["강의명"] = sr.LectureName;
                dr["개설학교"] = sr.CreateSchName;
                dt.Rows.Add(dr);
            }

            return dt;
        }

        //private static DataTable SetStatsUserToDataTable(List<AfterSchool_Lecture_Request> list, string[] headerTitleList)
        //{
        //    DataTable dt = new DataTable();
        //    int columnIdx = 0;
        //    foreach (string columName in headerTitleList)
        //    {
        //        if (columnIdx > headerTitleList.Length - 1)
        //        {
        //            break;
        //        }
        //        columnIdx++;
        //        dt.Columns.Add(columName);
        //    }
        //    headerTitleList = new string[] { "Year", "Semester", "CssCode", "LectureName", "CreateSchCode" };
        //    foreach (AfterSchool_Lecture_Request sr in list)
        //    {
        //        DataRow dr = dt.NewRow();
        //        dr["Year"] = sr.Year;
        //        dr["Semester"] = sr.Semester;
        //        dr["CssCode"] = sr.CssCode;
        //        dr["LectureName"] = sr.LectureName;
        //        dr["CreateSchCode"] = sr.CreateSchCode;
        //        dt.Rows.Add(dr);
        //    }

        //    return dt;
        //}

        private static DataTable SetStudentListToDataTable(List<AfterSchool_Lecture_Request> list, string[] headerTitleList)
        {
            DataTable dt = new DataTable();
            int columnIdx = 0;
            foreach (string columName in headerTitleList)
            {
                if (columnIdx > headerTitleList.Length - 1)
                {
                    break;
                }
                columnIdx++;
                dt.Columns.Add(columName);
            }
            //headerTitleList = new string[] { "SchoolName", "Grade", "Class", "StudentId", "Uname", "Phone", "Motive", "State" };
            //headerTitleList = new string[] { "학교명", "학년", "반", "번호", "이름", "전화번호", "동기" };
            foreach (AfterSchool_Lecture_Request sr in list)
            {
                DataRow dr = dt.NewRow();
                dr["학교명"] = sr.SchoolName;
                dr["학년"] = sr.Grade;
                dr["반"] = sr.Class;
                dr["번호"] = sr.StudentId;
                dr["이름"] = sr.Uname;
                dr["전화번호"] = sr.Phone;
                dr["동기"] = sr.Motive;
                dr["상태"] = sr.State.ToString().Replace("0","대기").Replace("1", "접수");
                dt.Rows.Add(dr);
            }

            return dt;
        }



        [HttpPost]
        public JsonResult SetLectureRequestState(int seq, int state)
        {
            try
            {
                AfterSchool_Lecture_Request m = new AfterSchool_Lecture_Request
                {
                    RequestSeq = seq
                    ,State = state
                };

                AfterSchool_LectureRepository.Instance.SetAfterLectureRequestStat(m);
                return Json(new { jsonMsg = "S" });
            }
            catch (Exception ex)
            {
                return Json(new { jsonMsg = "F" });
            }
        }
    }
}
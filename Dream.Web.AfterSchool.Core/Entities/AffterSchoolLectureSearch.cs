﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dream.Web.AfterSchool.Core.Entities
{
    public class AfterShoolLectureSearch
    {
        public string RegionCode { get; set; }
        public string SchoolName { get; set; }
        public string Year { get; set; }
        public string Semester { get; set; }
        public string SearchKey { get; set; }
        public string SearchType { get; set; }
        public string LectureType { get; set; }
        public string LectureSeq { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dream.Web.AfterSchool.MobileWeb.Models
{
    public class BaseModel
    {
        public string Title { get; set; }
        public int midx { get; set; }
        public string userName { get; set; }
        public string schoolName { get; set; }

        public BaseModel()
        {

        }

        public BaseModel(BaseModel model)
        {
            Title = model.Title;
            midx = model.midx;
            userName = model.userName;
            schoolName = model.schoolName;
        }
    }
}
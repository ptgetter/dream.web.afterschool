﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dream.Web.AfterSchool.Models
{
    public class ApplicationMessages
    {
        public static ApplicationStyleMessages Success(string msg)
        {
            ApplicationStyleMessages style = new ApplicationStyleMessages();
            style.message = msg;
            style.css = ApplicationCss.Success;
            style.type = ResponseEnum.Success;
            return style;
        }

        public static ApplicationStyleMessages Info(string msg)
        {
            ApplicationStyleMessages style = new ApplicationStyleMessages();
            style.message = msg;
            style.css = ApplicationCss.Info;
            style.type = ResponseEnum.Info;
            return style;
        }
        public static ApplicationStyleMessages Error(string msg)
        {
            ApplicationStyleMessages style = new ApplicationStyleMessages();
            style.message = msg;
            style.css = ApplicationCss.Error;
            style.type = ResponseEnum.Error;
            return style;
        }

        public static ApplicationStyleMessages Warring(string msg)
        {
            ApplicationStyleMessages style = new ApplicationStyleMessages();
            style.message = msg;
            style.css = ApplicationCss.Warning;
            style.type = ResponseEnum.Warning;
            return style;
        }

        public static ApplicationStyleMessages ApplicationError = new ApplicationStyleMessages
        {
            message = "알 수 없는 문제가 발생 하였습니다, 퓨쳐플랜으로 연락주시기 바랍니다.",
            css = ApplicationCss.Error,
            type = ResponseEnum.Error
        };

        public static ApplicationStyleMessages DateTimeError = new ApplicationStyleMessages
        {
            message = "날짜를 입력해 주세요.",
            css = ApplicationCss.Error,
            type = ResponseEnum.Error
        };

        public static ApplicationStyleMessages InsertSuccess = new ApplicationStyleMessages
        {
            message = "등록 되었습니다.", //updated success
            css = ApplicationCss.Success,
            type = ResponseEnum.Success
        };

        public static ApplicationStyleMessages UpdateSuccess = new ApplicationStyleMessages
        {
            message = "저장 완료", //updated success
            css = ApplicationCss.Success,
            type = ResponseEnum.Success
        };

        public static ApplicationStyleMessages DeleteSuccess = new ApplicationStyleMessages
        {
            message = "삭제완료", //updated success
            css = ApplicationCss.Success,
            type = ResponseEnum.Success
        };
    }

    public enum ResponseEnum
    {
        Success = 0,
        Error = 1,
        Info = 2,
        Warning = 3
    }
    public class ApplicationStyleMessages
    {
        public string css { get; set; }
        public string message { get; set; }
        public ResponseEnum type { get; set; }
    }
    public class ApplicationCss
    {
        public const string Primary = "alert alert-primary";
        public const string Success = "alert alert-success";
        public const string Info = "alert alert-info";
        public const string Warning = "alert alert-warning";
        public const string Error = "alert alert-danger";
        public const string Mode = "alert alert-mode";
    }
}
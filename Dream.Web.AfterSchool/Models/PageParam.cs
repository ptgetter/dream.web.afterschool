﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace Dream.Web.AfterSchool.Models
{
    public class PageParam
    {
        /// <summary>
        /// 키 - Detail 페이지 조회
        /// </summary>
        public long PK { get; set; }
        /// <summary>
        /// Search Option 검색옵션
        /// </summary>
        public string SO { get; set; }
        /// <summary>
        /// Search Word 검색어
        /// </summary>
        public string SW { get; set; }
        /// <summary>
        /// 페이지마다 가져올 ROW COUNT
        /// </summary>
        public byte RC { get; set; }
        /// <summary>
        /// 현재 페이지
        /// </summary>
        public int PG { get; set; }
        /// <summary>
        /// 페이지 크기 - 페이저에서 보여지는 페이지 수
        /// </summary>
        public int SIZE { get; set; }
        /// <summary>
        /// 옵션1
        /// </summary>
        public string OP1 { get; set; }
        /// <summary>
        /// 옵션2
        /// </summary>
        public string OP2 { get; set; }
        /// <summary>
        /// 옵션3
        /// </summary>
        public string OP3 { get; set; }
        /// <summary>
        /// 옵션4
        /// </summary>
        public string OP4 { get; set; }
        /// <summary>
        /// 옵션5
        /// </summary>
        public string OP5 { get; set; }
        /// <summary>
        /// 옵션6
        /// </summary>
        public string OP6 { get; set; }
        /// <summary>
        /// 옵션7
        /// </summary>
        public string OP7 { get; set; }
        /// <summary>
        /// 옵션8
        /// </summary>
        public string OP8 { get; set; }


        public PageParam()
        {
            this.PG = 1;
            this.RC = 10;
            this.SIZE = 10;
        }

        public PageParam SetPK(long pk)
        {
            this.PK = pk;
            return this;
        }

        //public override string ToString()
        //{
        //    StringBuilder sb = new StringBuilder();

        //    Type type = Type.GetType("FPFrame.Common.Web.PageParam");
        //    FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        //    foreach (var item in fieldInfos)
        //    {
        //        string name = item.Name.Substring(1, item.Name.IndexOf('>') - 1);
        //        sb.AppendFormat("{0}={1}&", name, item.GetValue(this));
        //    }

        //    return sb.ToString();

        //    //return $"PK={this.PK}&SO={this.SO}&SW={this.SW}&RC={this.RC}&PG={this.PG}&SIZE={this.SIZE}&OP1={this.OP1}&OP2={this.OP2}&OP3={this.OP3}&OP4={this.OP4}";
        //}

        //public string ToHidden()
        //{
        //    StringBuilder sb = new StringBuilder();

        //    Type type = Type.GetType("FPFrame.Common.Web.PageParam");
        //    FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        //    foreach (var item in fieldInfos)
        //    {
        //        string name = item.Name.Substring(1, item.Name.IndexOf('>') - 1);
        //        sb.AppendFormat("<input type='hidden' id='{0}' name='{0}' value='{1}' />", name, item.GetValue(this));
        //    }

        //    return sb.ToString();
        //}
    }
}
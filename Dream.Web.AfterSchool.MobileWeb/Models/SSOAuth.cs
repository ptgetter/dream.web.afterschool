﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Dream.Web.AfterSchool.MobileWeb.Models
{
    public class SSOAuth
    {
        #region Singleton Instance

        private static volatile SSOAuth _instance;

        private readonly byte[] _secret;
        private readonly string _issuer;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static SSOAuth Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(SSOAuth))
                    {
                        if (_instance == null)
                        {
                            _instance = new SSOAuth();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion

        public SSOAuth()
        {
            _secret = Convert.FromBase64String(ConfigurationManager.AppSettings["secret"]);
            _issuer = ConfigurationManager.AppSettings["issuer"].ToString();
        }

        public ClaimsIdentity Unprotect(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                var jsth = new JwtSecurityTokenHandler();

                SecurityToken sToken;
                TokenValidationParameters param = new TokenValidationParameters();
                param.ValidateIssuerSigningKey = true;
                param.IssuerSigningKey = new SymmetricSecurityKey(_secret);
                param.ValidateIssuer = true;
                param.ValidIssuer = _issuer;
                param.ValidAudiences = new[] { "Any" };
                param.ValidateLifetime = true;

                var principal = jsth.ValidateToken(token, param, out sToken);

                if (DateTime.Now <= sToken.ValidTo.ToLocalTime())
                {
                    return principal.Identity as ClaimsIdentity;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public string Protect(ClaimsIdentity identity, AuthenticationProperties property)
        {
            AuthenticationTicket data = new AuthenticationTicket(identity, property);

            if (data == null)
            {
                return string.Empty;
            }

            var signingKey = new SigningCredentials(new SymmetricSecurityKey(_secret), SecurityAlgorithms.HmacSha256Signature);
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            string token = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(_issuer, "Any", data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey));
            return token;
        }
    }
}
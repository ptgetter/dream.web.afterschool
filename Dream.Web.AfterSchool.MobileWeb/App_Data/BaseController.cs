﻿using Dream.Web.AfterSchool.MobileWeb.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.MobileWeb
{
    [Authorize(Roles = "AfterSchool")]
    public class BaseController : Controller
    {
        public BaseModel baseModel;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            string actionName = filterContext.ActionDescriptor.ActionName.ToLower();
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();

            if (actionName != "userinformation" && controllerName != "common")
            {
                if (claims.Where(c => c.Type == "setuserinfo").Select(c => c.Value).SingleOrDefault() == "Y")
                {
                    filterContext.Result = new RedirectResult("~/Common/UserInformation");
                    return;
                }
            }

            int midx = 0;
            int.TryParse(claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault(), out midx);

            baseModel = new BaseModel
            {
                Title = "",
                midx = midx,
                userName = claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Value).SingleOrDefault(),
                schoolName = claims.Where(c => c.Type == "schoolName").Select(c => c.Value).SingleOrDefault()
            };

            base.OnActionExecuting(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.Result = new RedirectResult("~/Account/Logout");
            base.OnException(filterContext);
        }
    }
}
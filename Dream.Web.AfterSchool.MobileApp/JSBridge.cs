﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Interop;

namespace Dream.Web.AfterSchool.MobileApp
{
    public class JSBridge : Activity
    {
        Context _context;

        public JSBridge(Context context)
        {
            this._context = context;
        }

        /// <summary>
        /// 현재 디바이스의 Push 토큰과 디바이스 정보를 웹에 전달하도록 요청한다.
        /// </summary>
        [Export("loginCRM")]
        [JavascriptInterface]
        public void LoginCRM()
        {
            //((MainActivity)_context).LoginCRM();
        }

        /// <summary>
        /// 카메라 실행
        /// </summary>
        [Export("openCamera")]
        [JavascriptInterface]
        public void OpenCamera()
        {
            //((MainActivity)_context).OpenCamera();
        }

        /// <summary>
        /// 갤러리 열기
        /// </summary>
        [Export("openGallery")]
        [JavascriptInterface]
        public void OpenGallery()
        {
            //((MainActivity)_context).OpenGallery();
        }

        /// <summary>
        /// 모달 갯수 설정
        /// </summary>
        [Export("setModalStatus")]
        [JavascriptInterface]
        public void SetModalStatus(int pStatus)
        {
            //((MainActivity)_context).SetModalStatus(pStatus);
        }

        /// <summary>
        /// 알림 설정 창
        /// </summary>
        [Export("notiSetting")]
        [JavascriptInterface]
        public void NotiSetting()
        {
           // ((MainActivity)_context).NotiSetting();
        }

        /// <summary>
        /// 로그아웃
        /// </summary>
        [Export("logoutCRM")]
        [JavascriptInterface]
        public void LogoutCRM()
        {
            //((MainActivity)_context).LogoutCRM();
        }

        /// <summary>
        /// 현재 App의 버젼
        /// </summary>
        [Export("getAppVersion")]
        [JavascriptInterface]
        public void GetAppVersion()
        {
            //((MainActivity)_context).GetAppVersion();
        }

        /// <summary>
        /// 인터넷 연결 상태
        /// </summary>
        [Export("getConnectionStatus")]
        [JavascriptInterface]
        public void GetConnectionStatus()
        {
            //((MainActivity)_context).GetConnectionStatus();
        }

        /// <summary>
        /// 페이지 새로고침
        /// </summary>
        [Export("webViewRefresh")]
        [JavascriptInterface]
        public void WebViewRefresh()
        {
            //((MainActivity)_context).WebViewRefresh();
        }

        /// <summary>
        /// 앱종료
        /// </summary>
        [Export("appTerminate")]
        [JavascriptInterface]
        public void AppTerminate()
        {
            //((MainActivity)_context).AppTerminate();
        }

        [JavascriptInterface]
        [Export("fileDownload")]
        public void FileDownload(string name, string url)
        {
            ((MainActivity)_context).DownloadFromURL(name, url);
        }

        [Export("goAppUpdate")]
        [JavascriptInterface]
        public void GoAppUpdate(string packageName)
        {
            //((MainActivity)_context).GoAppUpdate(packageName);
        }
    }
}
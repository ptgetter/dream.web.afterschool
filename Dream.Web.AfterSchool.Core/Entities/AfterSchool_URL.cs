using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("AfterSchool_URL")]
    public partial class AfterSchool_URL : BaseEntity
    {
        public int Idx { get; set; }

        public string URL { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsUse { get; set; }
    }
}

﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Net;

namespace Dream.Web.AfterSchool.MobileWeb
{
    public partial class Startup
    {
        // 인증 구성에 대한 자세한 내용은 https://go.microsoft.com/fwlink/?LinkId=301864를 참조하세요.
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                LogoutPath = new PathString("/Account/LogOut"),
                CookieName = "AfterSchool",
                ExpireTimeSpan = TimeSpan.FromMinutes(30),
                Provider = new CookieAuthenticationProvider
                {
                    OnApplyRedirect = context => {
                        if (context.OwinContext.Authentication.User.Identity.IsAuthenticated)
                        {
                            if (context.OwinContext.Response.StatusCode == Convert.ToInt32(HttpStatusCode.Unauthorized))
                            {
                                context.Response.StatusCode = Convert.ToInt32(HttpStatusCode.Forbidden);
                            }
                        }
                        else
                        {
                            bool isAjaxCall = string.Equals("XMLHttpRequest", context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);
                            if (isAjaxCall)
                            {
                                context.Response.Write("{ result:'unauthenticated', message:'권한이 없습니다.'}");
                            }
                            else
                            {

                                context.Response.Redirect(context.RedirectUri);
                            }
                        }

                    },
                    OnException = context => {

                    },
                    OnResponseSignIn = context => {

                    },
                    OnResponseSignOut = context => {

                    },
                    OnResponseSignedIn = context => {

                    },

                    //OnValidateIdentity = SecurityStampValidator.OnValidateIdentity(
                    //    validateInterval: TimeSpan.FromMinutes(2),
                    //    regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
                    //    getUserIdCallback: (id) => (Int32.Parse(id.GetUserId())))
                    //OnValidateIdentity = async context =>
                    //{

                    //    //var uow = DependencyResolver.Current.GetService<IUnitOfWork>();
                    //    //var user = "";
                    //    //uow.Reload(user);
                    //    //var oldSecurityStamp = (context.Identity as ClaimsIdentity).Claims.Where(x => x.Type.Equals(ClaimTypes.Expiration)).FirstOrDefault().Value;

                    //    //if (!user.SecurityStamp.Equals(oldSecurityStamp))
                    //    //{
                    //    //context.RejectIdentity();
                    //    //context.OwinContext.Authentication.SignOut(context.Options.AuthenticationType);
                    //    //}
                    //}
                }
            });

            // 타사 로그인 공급자로 로그인할 수 있으려면 다음 줄의 주석 처리를 제거합니다.
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LectureManage.Entities;
using LectureManage.Repository;
using LectureManage.Models;
using PagedList;
using System.Security.Claims;

namespace LectureManage.Controllers
{
    public class LectureManageController : BaseController
    {
        /// <summary>
        /// 강좌 리스트
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public ActionResult LectureList(SearchCondition search)
        {
            ViewBag.LectureType = search.lectureType;
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            AfterSchool_LectureRepository repository = new AfterSchool_LectureRepository();

            if (search == null) {
                search = new SearchCondition();
            }

            if (search.page == 0)
            {
                search.page = 1;

            }

            if (search.period == null)
            {
                search.period = new List<YearField>();
                List<AfterSchool_Lecture> year = repository.AfterSchool_LectureYear();

                //for (int i = 0; i < year.Count; i++)
                foreach(var lecture in year)
                {
                    search.period.Add(new YearField
                    {
                        year_semester = string.Format("{0}|{1}", lecture.Year, 1)
                    });
                    search.period.Add(new YearField
                    {
                        year_semester = string.Format("{0}|{1}", lecture.Year, 2)
                    });
                }
            }

            if (search.searchField == null) {
                search.searchField = new List<SearchField>();

                if (search.lectureType == 0)
                {
                    search.searchField.Add(new SearchField
                    {
                        fieldName = "강좌명",
                        fieldValue = "LectureName"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "학교명",
                        fieldValue = "CreateSchCode"
                    });
                }
                else
                {
                    search.searchField.Add(new SearchField
                    {
                        fieldName = "지역별",
                        fieldValue = "Region"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "교과명",
                        fieldValue = "CssCode"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "강좌명",
                        fieldValue = "LectureName"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "학교명",
                        fieldValue = "CreateSchCode"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "신청 학교명",
                        fieldValue = "TargetSchCodes"
                    });
                }
            }

            if (search.stateField == null)
            {
                search.stateField = new List<StateField>();
                search.stateField.Add(new StateField
                {
                    stateCode = 0,
                    stateName = "종료"
                });
                search.stateField.Add(new StateField
                {
                    stateCode = 1,
                    stateName = "운영"
                });
                search.stateField.Add(new StateField
                {
                    stateCode = 3,
                    stateName = "접수"
                });
                search.stateField.Add(new StateField
                {
                    stateCode = 4,
                    stateName = "마감"
                });
                search.stateField.Add(new StateField
                {
                    stateCode = 5,
                    stateName = "대기"
                });
            }

            ViewModel<AfterSchool_Lecture> entities = new ViewModel<AfterSchool_Lecture>();
            entities.PageSize = 15;
            entities.PageIndex = search.page;
            entities.SearchField = search.selectField == null ? "" : search.selectField;
            entities.SearchText = search.searchText;
            entities.Search = new AfterSchool_Lecture();
            entities.Search.LectureType = search.lectureType;
            entities.Search.midx = claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault();
            //entities.Search.Year = search.yearIdx == 0 ? 0 : int.Parse(search.period[search.yearIdx].year_semester.Split('|')[0]);
            //entities.Search.Semester = search.yearIdx == 0 ? 0 : int.Parse(search.period[search.yearIdx].year_semester.Split('|')[1]);
            entities.Search.Year = search.selectYear == null ? 0 : int.Parse(search.selectYear.Split('|')[0]);
            entities.Search.Semester = search.selectYear == null ? 0 : int.Parse(search.selectYear.Split('|')[1]);

            repository.AfterSchool_LectureSelectPaging(entities);

            SearchLectureManageModel model = new SearchLectureManageModel()
            {
                Title = "강좌관리",
                entity = entities,
                pageList = new StaticPagedList<AfterSchool_Lecture>(entities.List, search.page, 15, entities.TotalCount),
                search = search
            };

            return View(model);
        }

        public ActionResult LectureRequest(int lectureSeq = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            int midx = 0;
            int.TryParse(claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault(), out midx);

            AfterSchool_LectureRepository lRepository = new AfterSchool_LectureRepository();
            memberRepository mRepository = new memberRepository();
            SchoolInfosRepository sRepository = new SchoolInfosRepository();

            var user = mRepository.memberSelectItem(new member() { midx = midx });
            var lecture = lRepository.AfterSchool_LectureSelectItem(new AfterSchool_Lecture() { LectureSeq = lectureSeq });
            var schools = sRepository.SchoolInfosSelect(new SchoolInfos());

            List<SchoolInfo> schoolInfo = new List<SchoolInfo>();
            foreach (var school in schools)
            {
                schoolInfo.Add(new SchoolInfo
                {
                    schoolCode = school.sch_code,
                    schoolName = school.sch_name
                });
            }

            LectureRequestModel model = new LectureRequestModel()
            {
                Title = "강좌관리",
                lecture = lecture,
                user = user,
                school = schoolInfo,
                telcom = new List<string>( new string[] { "010", "011", "016", "017", "019" } )
            };

            return View(model);
        }

        [HttpPost]
        public JsonResult LectureRequestInsert(AfterSchool_Lecture_Request request, member user)
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                int midx = 0;
                int.TryParse(claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault(), out midx);

                AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();
                memberRepository mRepository = new memberRepository();

                request.midx = midx;
                user.midx = midx;
                mRepository.memberUpdate(user);
                int rtn = rRepository.AfterSchool_Lecture_RequestInsert(request);


                if (rtn == -1)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "신청 인원을 초과 하였습니다."
                    });
                }
                else
                {
                    return Json(new
                    {
                        ok = true,
                        message = "신청하였습니다."
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = string.Format("오류가 발생 했습니다.\r\n{0}", ex.ToString())
                });
            }
        }

        [HttpPost]
        public JsonResult LectureRequestCancel(long requestSeq)
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                int midx = 0;
                int.TryParse(claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).SingleOrDefault(), out midx);

                AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();

                int rtn = rRepository.AfterSchool_Lecture_RequestDelete(new AfterSchool_Lecture_Request { midx = midx, RequestSeq = requestSeq });


                if (rtn == -1)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "오류가 발생 했습니다"
                    });
                }
                else
                {
                    return Json(new
                    {
                        ok = true,
                        message = "취소 했습니다."
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = string.Format("오류가 발생 했습니다.\r\n{0}", ex.ToString())
                });
            }
        }
    }
}
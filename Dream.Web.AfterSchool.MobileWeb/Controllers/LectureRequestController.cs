﻿using Dream.Web.AfterSchool.MobileWeb.Models;
using Dream.Web.AfterSchool.Core.Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dream.Web.AfterSchool.Core.Repository;

namespace Dream.Web.AfterSchool.MobileWeb.Controllers
{
    public class LectureRequestController : BaseController
    {
        #region View

        /// <summary>
        /// 강좌 리스트
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public ActionResult LectureList(SearchCondition search)
        {
            ViewBag.LectureType = search.lectureType;
            if (search.lectureType == 0)
            {
                baseModel.Title = "개방형 강의";
            }
            else if (search.lectureType == 1)
            {
                baseModel.Title = "학교연합형 강의";
            }
            else
            {
                baseModel.Title = "연합교육과정";
            }

            AfterSchool_LectureRepository repository = new AfterSchool_LectureRepository();

            if (search == null)
            {
                search = new SearchCondition();
            }

            if (search.page == 0)
            {
                search.page = 1;

            }

            // 년도 및 학기
            if (search.period == null)
            {
                search.period = new List<YearField>();
                //List<AfterSchool_Lecture> year = repository.AfterSchool_LectureYear();

                //for (int i = 0; i < year.Count; i++)
                //foreach (var lecture in year)
                //{
                //    search.period.Add(new YearField
                //    {
                //        year_semester = string.Format("{0}|{1}", lecture.Year, 1)
                //    });
                //    search.period.Add(new YearField
                //    {
                //        year_semester = string.Format("{0}|{1}", lecture.Year, 2)
                //    });
                //}

                search.period.Add(new YearField
                {
                    year_semester = string.Format("{0}|{1}", DateTime.Now.Year, DateTime.Now.Month < 7 ? 1 : 2)
                });

                search.period.Add(new YearField
                {
                    year_semester = string.Format("{0}|{1}", DateTime.Now.Month < 7 ? DateTime.Now.Year : DateTime.Now.Year + 1, DateTime.Now.Month < 7 ? 2 : 1)
                });
            }

            // 검색 필드
            if (search.searchField == null)
            {
                search.searchField = new List<SearchField>();

                if (search.lectureType == 0)
                {
                    search.searchField.Add(new SearchField
                    {
                        fieldName = "강의명",
                        fieldValue = "LectureName"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "개설학교",
                        fieldValue = "CreateSchCode"
                    });
                }
                else
                {
                    search.searchField.Add(new SearchField
                    {
                        fieldName = "지역별",
                        fieldValue = "Region"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "교과명",
                        fieldValue = "CssCode"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "강의명",
                        fieldValue = "LectureName"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "학교명",
                        fieldValue = "CreateSchCode"
                    });

                    search.searchField.Add(new SearchField
                    {
                        fieldName = "신청 학교명",
                        fieldValue = "TargetSchCodes"
                    });
                }
            }

            if (search.stateField == null)
            {
                search.stateField = new List<string>();
                search.stateField.Add("종료");
                search.stateField.Add("운영");
                search.stateField.Add("");
                search.stateField.Add("접수");
                search.stateField.Add("마감");
                search.stateField.Add("대기");
            }

            ViewModel<AfterSchool_Lecture> entities = new ViewModel<AfterSchool_Lecture>();
            entities.PageSize = 15;
            entities.PageIndex = search.page;
            entities.SearchField = search.selectField == null ? "" : search.selectField;
            entities.SearchText = search.searchText;
            entities.Search = new AfterSchool_Lecture();
            entities.Search.LectureType = search.lectureType;
            entities.Search.midx = baseModel.midx.ToString();
            entities.Search.Year = search.selectYear == null ? DateTime.Now.Year : int.Parse(search.selectYear.Split('|')[0]);
            entities.Search.Semester = search.selectYear == null ? (DateTime.Now.Month < 7 ? 1 : 2) : int.Parse(search.selectYear.Split('|')[1]);

            repository.AfterSchool_LectureSelectPaging(entities);

            

            SearchLectureManageModel model = new SearchLectureManageModel(baseModel);
            //model.Title = baseModel.Title;
            model.entity = entities;
            model.pageList = new StaticPagedList<AfterSchool_Lecture>(entities.List, search.page, 15, entities.TotalCount);
            model.search = search;

            return View(model);
        }

        [HttpPost]
        public ActionResult LectureRequest(int lectureSeq = 0)
        {
            baseModel.Title = "강의관리";

            AfterSchool_LectureRepository lRepository = new AfterSchool_LectureRepository();
            memberRepository mRepository = new memberRepository();
            SchoolInfosRepository sRepository = new SchoolInfosRepository();

            var user = mRepository.memberSelectItem(new member() { midx = baseModel.midx });
            var lecture = lRepository.AfterSchool_LectureSelectItem(new AfterSchool_Lecture() { LectureSeq = lectureSeq });

            try
            {
                int grade = 0;

                // 신청 가능한지 검사
                if (lecture.MaxSignUp <= lecture.CurrentSignUp)
                {
                    ViewBag.ErrorMsg = "신청 인원이 초과되었습니다.";
                }
                else if (int.TryParse(user.grade, out grade) && lecture.TargetGrade.Length <= grade && lecture.TargetGrade[grade] == '1')
                {
                    ViewBag.ErrorMsg = "신청 대상 학년이 아닙니다.";
                }
                else if (lecture.TargetSchCodes.IndexOf(user.sch_code) < 0)
                {
                    ViewBag.ErrorMsg = "신청 대상 학교가 아닙니다.";
                }

                if (!string.IsNullOrEmpty(ViewBag.ErrorMsg))
                {
                    if (Request.IsAjaxRequest())
                    {
                        return Json(new
                        {
                            ok = false,
                            message = ViewBag.ErrorMsg
                        });
                    }
                    else
                    {
                        return View(new LectureRequestModel(baseModel));
                    }
                }
            }
            catch { }

            var schools = sRepository.SchoolInfosSelect(new SchoolInfos());

            List<SchoolInfo> schoolInfo = new List<SchoolInfo>();
            foreach (var school in schools)
            {
                schoolInfo.Add(new SchoolInfo
                {
                    schoolCode = school.sch_code,
                    schoolName = school.sch_name
                });
            }

            LectureRequestModel model = new LectureRequestModel(baseModel)
            {
                lecture = lecture,
                user = user,
                school = schoolInfo,
                telcom = new List<string>(new string[] { "010", "011", "016", "017", "019" })
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }
            else
            {
                return View(model);
            }

            
        }

        public ActionResult LectureRequestList(SearchCondition search)
        {
            baseModel.Title = "나의 수강신청";

            if (search == null)
            {
                search = new SearchCondition();
            }

            if (search.page == 0)
            {
                search.page = 1;
            }

            if (search == null)
            {
                search = new SearchCondition();
            }

            if (search.page == 0)
            {
                search.page = 1;

            }

            // 년도 및 학기
            if (search.period == null)
            {
                search.period = new List<YearField>();
                AfterSchool_LectureRepository repository = new AfterSchool_LectureRepository();
                List<AfterSchool_Lecture> year = repository.AfterSchool_LectureYear();

                //for (int i = 0; i < year.Count; i++)
                foreach (var lecture in year)
                {
                    search.period.Add(new YearField
                    {
                        year_semester = string.Format("{0}|{1}", lecture.Year, 1)
                    });
                    search.period.Add(new YearField
                    {
                        year_semester = string.Format("{0}|{1}", lecture.Year, 2)
                    });
                }
            }

            // 검색 필드
            if (search.searchField == null)
            {
                search.searchField = new List<SearchField>();

                search.searchField.Add(new SearchField
                {
                    fieldName = "강의명",
                    fieldValue = "LectureName"
                });

                search.searchField.Add(new SearchField
                {
                    fieldName = "개설학교",
                    fieldValue = "CreateSchCode"
                });
            }

            AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();
            ViewModel<AfterSchool_Lecture_Request> entities = new ViewModel<AfterSchool_Lecture_Request>();
            entities.PageSize = 15;
            entities.PageIndex = search.page;
            entities.SearchField = search.selectField == null ? "" : search.selectField;
            entities.SearchText = search.searchText;
            entities.Search = new AfterSchool_Lecture_Request();
            entities.Search.midx = baseModel.midx;
            entities.Search.Year = search.selectYear == null ? DateTime.Now.Year : int.Parse(search.selectYear.Split('|')[0]);
            entities.Search.Semester = search.selectYear == null ? (DateTime.Now.Month < 7 ? 1 : 2) : int.Parse(search.selectYear.Split('|')[1]);

            rRepository.AfterSchool_Lecture_RequestSelectPaging(entities);

            SearchLectureRequstModel model = new SearchLectureRequstModel(baseModel)
            {
                entity = entities,
                pageList = new StaticPagedList<AfterSchool_Lecture_Request>(entities.List, search.page, 15, entities.TotalCount),
                search = search
            };

            return View(model);
        }

        #endregion

        #region AJAX

        [HttpPost]
        public JsonResult LectureRequestInsert(AfterSchool_Lecture_Request request)
        {
            try
            {
                baseModel.Title = "강의관리";

                AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();
                request.midx = baseModel.midx;
                request.Motive = request.Motive ?? "";
                int rtn = rRepository.AfterSchool_Lecture_RequestInsert(request);


                if (rtn == 0)
                {
                    return Json(new
                    {
                        ok = true,
                        message = "신청하였습니다."
                    });

                }
                else if (rtn == 1)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "신청 인원을 초과 하였습니다."
                    });
                }
                else if (rtn == 2)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "이미 신청한 강의입니다."
                    });
                }
                else if (rtn == 3)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "신청 조건이 맞지 않습니다."
                    });
                }
                else
                {
                    return Json(new
                    {
                        ok = false,
                        message = "오류가 발생하였습니다."
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = string.Format("오류가 발생 했습니다.\r\n{0}", ex.ToString())
                });
            }
        }

        [HttpPost]
        public JsonResult LectureRequestCancel(long requestSeq)
        {
            try
            {
                baseModel.Title = "강의관리";

                AfterSchool_Lecture_RequestRepository rRepository = new AfterSchool_Lecture_RequestRepository();

                int rtn = rRepository.AfterSchool_Lecture_RequestDelete(new AfterSchool_Lecture_Request { midx = baseModel.midx, RequestSeq = requestSeq });

                if (rtn == -1)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "오류가 발생 했습니다"
                    });
                }
                else
                {
                    return Json(new
                    {
                        ok = true,
                        message = "취소 했습니다."
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = string.Format("오류가 발생 했습니다.\r\n{0}", ex.ToString())
                });
            }
        }

        #endregion
    }
}
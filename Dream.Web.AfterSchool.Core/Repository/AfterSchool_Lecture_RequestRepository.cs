using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// AfterSchool_Lecture_RequestRepository
    /// </summary>
    public class AfterSchool_Lecture_RequestRepository : BaseRepository
    {
        /// <summary>
        /// AfterSchool_Lecture_Request 목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Request</param>
        /// <returns></returns>
        public List<AfterSchool_Lecture_Request> AfterSchool_Lecture_RequestSelect(AfterSchool_Lecture_Request entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);
				parameters.Add("@RequestSeq", entity.RequestSeq);

                var ret = dbConnection.Query<AfterSchool_Lecture_Request>("AfterSchool_Lecture_RequestSelect", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Request ?�이�?목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Request</param>
        /// <returns></returns>
        public void AfterSchool_Lecture_RequestSelectPaging(ViewModel<AfterSchool_Lecture_Request> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@midx", entity.Search.midx);
                parameters.Add("@Year", entity.Search.Year);
                parameters.Add("@Semester", entity.Search.Semester);
                parameters.Add("@SearchField", entity.SearchField);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.QueryMultiple("AfterSchool_Lecture_RequestSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);

                var rlist = ret.Read<AfterSchool_Lecture_Request>().ToList();
                var flist = ret.Read<AfterSchool_Lecture_Files>().ToList();

                foreach (var lecture in rlist)
                {
                    if (lecture.AfterSchoolLectureFiles == null)
                    {
                        lecture.AfterSchoolLectureFiles = new List<AfterSchool_Lecture_Files>();
                    }
                    foreach (var file in flist)
                    {
                        if (lecture.LectureSeq == file.LectureSeq)
                        {
                            lecture.AfterSchoolLectureFiles.Add(file);
                        }
                    }
                }

                entity.List = rlist;
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Request ??�� 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Request</param>
        /// <returns></returns>
        public AfterSchool_Lecture_Request AfterSchool_Lecture_RequestSelectItem(AfterSchool_Lecture_Request entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);
				parameters.Add("@RequestSeq", entity.RequestSeq);

                var ret = dbConnection.Query<AfterSchool_Lecture_Request>("AfterSchool_Lecture_RequestSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Request ?�록
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Request</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_RequestInsert(AfterSchool_Lecture_Request entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
				parameters.Add("@midx", entity.midx);
				parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@Motive", entity.Motive);
				parameters.Add("@State", entity.State);
                parameters.Add("@rtnValue", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                int ret = dbConnection.Execute("AfterSchool_Lecture_RequestInsert", param: parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("rtnValue");
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Request ?�정
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Request</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_RequestUpdate(AfterSchool_Lecture_Request entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestSeq", entity.RequestSeq);
				parameters.Add("@midx", entity.midx);
				parameters.Add("@SchCode", entity.SchCode);
				parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@Motive", entity.Motive);
				parameters.Add("@State", entity.State);

                return dbConnection.Execute("AfterSchool_Lecture_RequestUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Request ?�록 �??�정
        /// </summary>
        /// <returns></returns>
        public int AfterSchool_Lecture_RequestUpsert(AfterSchool_Lecture_Request entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestSeq", entity.RequestSeq);
				parameters.Add("@midx", entity.midx);
				parameters.Add("@SchCode", entity.SchCode);
				parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@Motive", entity.Motive);
				parameters.Add("@State", entity.State);

                return dbConnection.Execute("AfterSchool_Lecture_RequestUpsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture_Request ??��
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture_Request</param>
        /// <returns></returns>
        public int AfterSchool_Lecture_RequestDelete(AfterSchool_Lecture_Request entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);
				parameters.Add("@RequestSeq", entity.RequestSeq);

                return dbConnection.Execute("AfterSchool_Lecture_RequestDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
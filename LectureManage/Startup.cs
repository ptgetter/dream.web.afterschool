﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LectureManage.Startup))]
namespace LectureManage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("AfterSchool_Lecture_Time")]
    public partial class AfterSchool_Lecture_Time : BaseEntity
    {
        /// <summary>
		/// 강의 Seq
		/// </summary>
		[Key]
        [Required]
        public long LectureSeq { get; set; }


        /// <summary>
        /// 강의 시간 Seq
        /// </summary>
        [Key]
        [Required]
        public long LectureTimeSeq { get; set; }


        /// <summary>
        /// 강의 선택 요일
        /// </summary>
        [Required]
        public int DayOfTheWeek { get; set; }


        /// <summary>
        /// 강의 시작 시간
        /// </summary>
        [Required]
        public DateTime StartTime { get; set; }


        /// <summary>
        /// 강의 종료 시간
        /// </summary>
        [Required]
        public DateTime EndTime { get; set; }


        /// <summary>
        /// 삭제 여부(0: 사용, 1: 삭제)
        /// </summary>
        [Required]
        public int IsDelete { get; set; }


        /// <summary>
        /// 등록자 ID
        /// </summary>
        [Required]
        [StringLength(100)]
        public string RegID { get; set; }


        /// <summary>
        /// 등록날짜
        /// </summary>
        [Required]
        public DateTime RegDate { get; set; }


        /// <summary>
        /// 수정자 ID
        /// </summary>
        [StringLength(100)]
        public string ModifyID { get; set; }


        /// <summary>
        /// 수정날짜
        /// </summary>
        public DateTime? ModifyDate { get; set; }


        /// <summary>
		/// 강의시간
		/// </summary>
		public string LectureTime { get; set; }
        public string LectureTimeCode { get; set; }


    }
}

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("SchoolInfos")]
    public partial class SchoolInfos : BaseEntity
    {
        /// <summary>
		/// 과거 학교코드
		/// </summary>
		[StringLength(50)]
        public string o_sch_code { get; set; }


        /// <summary>
        /// 학교코드
        /// </summary>
        [Key]
        [Required]
        [StringLength(20)]
        public string sch_code { get; set; }


        /// <summary>
        /// 학교 타입(E:초등, M:중학교, H:고등학교)
        /// </summary>
        [Required]
        [StringLength(2)]
        public string sch_type { get; set; }


        /// <summary>
        /// 학교 이름
        /// </summary>
        [Required]
        [StringLength(200)]
        public string sch_name { get; set; }


        /// <summary>
        /// 학교 지역(특별시/광역시/도)
        /// </summary>
        [Required]
        [StringLength(10)]
        public string sch_region1 { get; set; }


        /// <summary>
        /// 학교 지역(시/구/군)
        /// 교육(지원)청 이름과 연계
        /// </summary>
        [Required]
        [StringLength(10)]
        public string sch_region2 { get; set; }


        /// <summary>
        /// 학교주소
        /// </summary>
        public string sch_addr { get; set; }


        /// <summary>
        /// 학교 홈페이지
        /// </summary>
        [StringLength(200)]
        public string sch_url { get; set; }


        /// <summary>
        /// 학교 전화번호
        /// </summary>
        [StringLength(50)]
        public string sch_tel { get; set; }


        /// <summary>
        /// 학교 팩스번호
        /// </summary>
        [StringLength(50)]
        public string sch_fax { get; set; }


        /// <summary>
        /// 공립/사립 구분
        /// </summary>
        [StringLength(10)]
        public string sch_est_ghn { get; set; }


        #region | 추가 |
        public string RegionCode { get; set; }

        public string SchCode { get; set; }

        public string SchName { get; set; }

        public string SchType { get; set; }

        public string OfficeOfEducationName { get; set; }

        

        #endregion


    }
}

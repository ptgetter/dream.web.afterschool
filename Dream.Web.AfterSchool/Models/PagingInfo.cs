﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dream.Web.AfterSchool.Models
{
    public class PagingInfo
    {
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public long TotalItems { get; set; }                      
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        //public int TotalPages
        //{
        //    get
        //    {
        //        var totalPage = (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);

        //        return totalPage > 0 ? totalPage : 1;
        //    }
        //}

        public PagingInfo()
        {
            //ItemsPerPage = 40;
        }

        public PagingInfo(int currentPage, int pageSize, int totalItems)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalItems = totalItems;
        }
    }
}
using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// AfterSchool_LectureRepository
    /// </summary>
    public class AfterSchool_LectureRepository : BaseRepository
    {
        #region Singleton Instance

        private static volatile AfterSchool_LectureRepository _instance;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static AfterSchool_LectureRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(AfterSchool_LectureRepository))
                    {
                        if (_instance == null)
                        {
                            _instance = new AfterSchool_LectureRepository();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion
        

        #region | �л��� |
        /// <summary>
        /// AfterSchool_Lecture 목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture</param>
        /// <returns></returns>
        public List<AfterSchool_Lecture> AfterSchool_LectureSelect(AfterSchool_Lecture entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);

                var ret = dbConnection.Query<AfterSchool_Lecture>("AfterSchool_LectureSelect", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture ?�이�?목록 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture</param>
        /// <returns></returns>
        public void AfterSchool_LectureSelectPaging(ViewModel<AfterSchool_Lecture> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@midx", entity.Search.midx);
                parameters.Add("@Year", entity.Search.Year);
                parameters.Add("@Semester", entity.Search.Semester);
                parameters.Add("@LectureType", entity.Search.LectureType);
                parameters.Add("@SearchField", entity.SearchField);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.QueryMultiple("AfterSchool_LectureSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);
                var llist = ret.Read<AfterSchool_Lecture>().ToList();
                var flist = ret.Read<AfterSchool_Lecture_Files>().ToList();

                foreach (var lecture in llist)
                {
                    if (lecture.AfterSchoolLectureFiles == null)
                    {
                        lecture.AfterSchoolLectureFiles = new List<AfterSchool_Lecture_Files>();
                    }
                    foreach (var file in flist)
                    {
                        if (lecture.LectureSeq == file.LectureSeq)
                        {
                            lecture.AfterSchoolLectureFiles.Add(file);
                        }
                    }
                }

                entity.List = llist;
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// AfterSchool_Lecture ??�� 조회
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture</param>
        /// <returns></returns>
        public AfterSchool_Lecture AfterSchool_LectureSelectItem(AfterSchool_Lecture entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);

                var ret = dbConnection.Query<AfterSchool_Lecture>("AfterSchool_LectureSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// AfterSchool_Lecture ?�록
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture</param>
        /// <returns></returns>
        public int AfterSchool_LectureInsert(AfterSchool_Lecture entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@Year", entity.Year);
				parameters.Add("@Semester", entity.Semester);
				parameters.Add("@LectureType", entity.LectureType);
				parameters.Add("@CssCode", entity.CssCode);
				parameters.Add("@LectureName", entity.LectureName);
				parameters.Add("@MaxTime", entity.MaxTime);
				parameters.Add("@CreateSchCode", entity.CreateSchCode);
				parameters.Add("@MaxSignUp", entity.MaxSignUp);
				parameters.Add("@TargetSchCodes", entity.TargetSchCodes);
				parameters.Add("@OperationStartDate", entity.OperationStartDate);
				parameters.Add("@OperationEndDate", entity.OperationEndDate);
				parameters.Add("@TargetGrade", entity.TargetGrade);
				parameters.Add("@TargetGender", entity.TargetGender);
				parameters.Add("@Reception", entity.Reception);
				parameters.Add("@ReceptionClosed", entity.ReceptionClosed);
				parameters.Add("@State", entity.State);
				parameters.Add("@RegID", entity.RegID);
				parameters.Add("@RegDate", entity.RegDate);
				parameters.Add("@ModifyID", entity.ModifyID);
				parameters.Add("@ModifyDate", entity.ModifyDate);

                return dbConnection.Execute("AfterSchool_LectureInsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture ?�정
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture</param>
        /// <returns></returns>
        public int AfterSchool_LectureUpdate(AfterSchool_Lecture entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@Year", entity.Year);
				parameters.Add("@Semester", entity.Semester);
				parameters.Add("@LectureType", entity.LectureType);
				parameters.Add("@CssCode", entity.CssCode);
				parameters.Add("@LectureName", entity.LectureName);
				parameters.Add("@MaxTime", entity.MaxTime);
				parameters.Add("@CreateSchCode", entity.CreateSchCode);
				parameters.Add("@MaxSignUp", entity.MaxSignUp);
				parameters.Add("@TargetSchCodes", entity.TargetSchCodes);
				parameters.Add("@OperationStartDate", entity.OperationStartDate);
				parameters.Add("@OperationEndDate", entity.OperationEndDate);
				parameters.Add("@TargetGrade", entity.TargetGrade);
				parameters.Add("@TargetGender", entity.TargetGender);
				parameters.Add("@Reception", entity.Reception);
				parameters.Add("@ReceptionClosed", entity.ReceptionClosed);
				parameters.Add("@State", entity.State);
				parameters.Add("@RegID", entity.RegID);
				parameters.Add("@RegDate", entity.RegDate);
				parameters.Add("@ModifyID", entity.ModifyID);
				parameters.Add("@ModifyDate", entity.ModifyDate);

                return dbConnection.Execute("AfterSchool_LectureUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture ?�록 �??�정
        /// </summary>
        /// <returns></returns>
        public int AfterSchool_LectureUpsert(AfterSchool_Lecture entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);
				parameters.Add("@Year", entity.Year);
				parameters.Add("@Semester", entity.Semester);
				parameters.Add("@LectureType", entity.LectureType);
				parameters.Add("@CssCode", entity.CssCode);
				parameters.Add("@LectureName", entity.LectureName);
				parameters.Add("@MaxTime", entity.MaxTime);
				parameters.Add("@CreateSchCode", entity.CreateSchCode);
				parameters.Add("@MaxSignUp", entity.MaxSignUp);
				parameters.Add("@TargetSchCodes", entity.TargetSchCodes);
				parameters.Add("@OperationStartDate", entity.OperationStartDate);
				parameters.Add("@OperationEndDate", entity.OperationEndDate);
				parameters.Add("@TargetGrade", entity.TargetGrade);
				parameters.Add("@TargetGender", entity.TargetGender);
				parameters.Add("@Reception", entity.Reception);
				parameters.Add("@ReceptionClosed", entity.ReceptionClosed);
				parameters.Add("@State", entity.State);
				parameters.Add("@RegID", entity.RegID);
				parameters.Add("@RegDate", entity.RegDate);
				parameters.Add("@ModifyID", entity.ModifyID);
				parameters.Add("@ModifyDate", entity.ModifyDate);

                return dbConnection.Execute("AfterSchool_LectureUpsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// AfterSchool_Lecture ??��
        /// </summary>
        /// <param name="entity">AfterSchool_Lecture</param>
        /// <returns></returns>
        public int AfterSchool_LectureDelete(AfterSchool_Lecture entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", entity.LectureSeq);

                return dbConnection.Execute("AfterSchool_LectureDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public List<AfterSchool_Lecture> AfterSchool_LectureYear()
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                var ret = dbConnection.Query<AfterSchool_Lecture>("AfterSchool_LectureSelectYear", commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        public List<AfterSchool_Lecture> AfterSchool_Lecture_Delete()
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                var ret = dbConnection.Query<AfterSchool_Lecture>("AfterSchool_LectureSelectYear", commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        #endregion


        #region | ������ |

        /// <summary>
        /// AfterSchool_Lecture �߰�
        /// </summary>
        /// <returns></returns>
        public int SetAfterSchoolLecture(AfterSchool_Lecture entity)
        {
            try
            {
                using (IDbConnection dbConnection = base.OpenConnection())
                {
                    int rVal = 0;
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@Year", entity.Year);
                    parameters.Add("@Semester", entity.Semester);
                    parameters.Add("@LectureType", entity.LectureType);
                    parameters.Add("@CssCode", entity.CssCode);
                    parameters.Add("@LectureName", entity.LectureName);
                    parameters.Add("@MaxTime", entity.MaxTime);
                    parameters.Add("@CreateSchCode", entity.CreateSchCode);
                    parameters.Add("@MaxSignUp", entity.MaxSignUp);
                    parameters.Add("@TargetSchCodes", entity.TargetSchCodes);
                    parameters.Add("@OperationStartDate", entity.OperationStartDate);
                    parameters.Add("@OperationEndDate", entity.OperationEndDate);
                    parameters.Add("@TargetGrade", entity.TargetGrade);
                    parameters.Add("@TargetGender", entity.TargetGender);
                    parameters.Add("@Reception", entity.Reception);
                    parameters.Add("@ReceptionClosed", entity.ReceptionClosed);
                    parameters.Add("@State", entity.State);
                    parameters.Add("@Place", entity.Place);
                    parameters.Add("@NssName", entity.NssName);
                    parameters.Add("@TeacherName", entity.TeacherName);
                    parameters.Add("@InfoMessage", entity.InfoMessage);
                    parameters.Add("@RegID", entity.RegID);
                    parameters.Add("@LectureSeq", entity.LectureSeq);
                    parameters.Add("@LectureTime", entity.LectureTime);
                    parameters.Add("@Result", rVal, DbType.Int32, ParameterDirection.Output);

                    dbConnection.Execute("sp_AfterSchool_Lecture_Insert", param: parameters, commandType: CommandType.StoredProcedure);

                    rVal = parameters.Get<int>("@Result");
                    return rVal;
                }
            }

            catch (Exception ex)
            {
                return -1;
            }
        }

        public List<AfterSchool_Lecture> GetAfterSchoolLecture(int lectureType, int lectureSeq = 0, AfterShoolLectureSearch search = null)
        {
            List<AfterSchool_Lecture> lstAfterSchoolLecture = new List<AfterSchool_Lecture>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureType", lectureType);
                parameters.Add("@LectureSeq", lectureSeq);
                var ret = dbConnection.QueryMultiple("sp_AfterSchool_Lecture_Select", param: parameters, commandType: CommandType.StoredProcedure);
                if (lectureSeq == 0)
                {

                    lstAfterSchoolLecture = ret.Read<AfterSchool_Lecture>().ToList();
                }
                else
                {
                    AfterSchool_Lecture afterSchoollecture = new AfterSchool_Lecture();
                    afterSchoollecture = ret.Read<AfterSchool_Lecture>().ToList()[0];
                    afterSchoollecture.AfterSchoolLectureTimes = ret.Read<AfterSchool_Lecture_Time>().ToList();
                    if (afterSchoollecture.LectureType == 1)
                    {
                        afterSchoollecture.AfterSchoolLectureSchool = ret.Read<AfterSchool_Lecture_School>().ToList();
                    }

                    afterSchoollecture.AfterSchoolLectureFiles = ret.Read<AfterSchool_Lecture_Files>().ToList();
                    lstAfterSchoolLecture.Add(afterSchoollecture);
                }

                return lstAfterSchoolLecture;
            }
        }

        public void GetAfterSchoolLecture(ViewModel<AfterSchool_Lecture, AfterShoolLectureSearch> model)
        {
            int totalCount = 0;
            List<AfterSchool_Lecture> lstAfterSchoolLecture = new List<AfterSchool_Lecture>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureType", model.Search.LectureType);
                parameters.Add("@LectureSeq", model.Search.LectureSeq);

                string offcieEcucationName = string.Empty;
                string cssName = string.Empty;
                string targetSchNames = string.Empty;
                string lectureName = string.Empty;
                string createSchName = string.Empty;


                switch (model.Search.SearchType)
                {
                    case "0":
                        {
                            offcieEcucationName = model.Search.SearchKey;
                            break;
                        }
                    case "1":
                        {
                            cssName = model.Search.SearchKey;
                            break;
                        }
                    case "2":
                        {
                            lectureName = model.Search.SearchKey;
                            break;
                        }
                    case "3":
                        {
                            createSchName = model.Search.SearchKey;
                            break;
                        }
                    case "4":
                        {
                            targetSchNames = model.Search.SearchKey;
                            break;
                        }
                }

                parameters.Add("@Year", model.Search.Year);
                parameters.Add("@Semester", model.Search.Semester);
                parameters.Add("@OfficeofEducationName", offcieEcucationName);
                parameters.Add("@CssName", cssName);
                parameters.Add("@TargetSchNames", targetSchNames);
                parameters.Add("@LectureName", lectureName);
                parameters.Add("@@CreateSchName", createSchName);
                parameters.Add("@currentPage", model.PageIndex);
                parameters.Add("@pageSize", model.PageSize);
                parameters.Add("@totalCount", totalCount, DbType.Int32, ParameterDirection.Output);
                var ret = dbConnection.Query<AfterSchool_Lecture>("sp_AfterSchool_Lecture_Select_for_List", param: parameters, commandType: CommandType.StoredProcedure);

                model.List = ret.ToList();
                model.TotalCount = parameters.Get<int>("@totalCount");
            }
        }

        public void GetSchoolnfos(ViewModel<SchoolInfos, AfterShoolLectureSearch> model)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RegionCode", model.Search.RegionCode);
                parameters.Add("@SchName", model.Search.SchoolName);
                parameters.Add("@currentPage", model.PageIndex);
                parameters.Add("@pageSize", model.PageSize);
                parameters.Add("@totalCount", model.TotalCount, DbType.Int32, ParameterDirection.Output);
                var ret = dbConnection.Query<SchoolInfos>("sp_After_School_Select", param: parameters, commandType: CommandType.StoredProcedure);

                model.List = ret.ToList();
                model.TotalCount = parameters.Get<int>("@totalCount");
            }
        }

        public List<Region> GetRegion(string officeOfEcucation)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@OfficeOfEduCation", officeOfEcucation);
                var ret = dbConnection.Query<Region>("sp_After_Region_OfficeOfEduCation_Select", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        public List<member> GetMemer(int idx = 0)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", idx);
                var ret = dbConnection.Query<member>("sp_AfterSchool_Member_Select", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>        
        /// </summary>
        /// <param name="lectureSeq"></param>
        /// <param name="modifyId"></param>
        /// <returns></returns>
        public int DeleteAfterSchoolLecture(int lectureSeq, string modifyId)
        {
            int rVal = 0;
            try
            {
                using (IDbConnection dbConnection = base.OpenConnection())
                {
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@LectureSeq", lectureSeq);
                    parameters.Add("@ModifyID", modifyId);
                    parameters.Add("@Result", rVal, DbType.Int32, ParameterDirection.Output);

                    rVal = dbConnection.Execute("sp_AfterSchool_Lecture_Delete", param: parameters, commandType: CommandType.StoredProcedure);
                }
                return rVal;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        public List<AfterSchool_Lecture> GetAfterSchoolLectureItem(AfterShoolLectureSearch search = null)
        {
            List<AfterSchool_Lecture> lstAfterSchoolLecture = new List<AfterSchool_Lecture>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureType", search.LectureType);

                string offcieEcucationName = string.Empty;
                string cssName = string.Empty;
                string targetSchNames = string.Empty;
                string lectureName = string.Empty;
                string createSchName = string.Empty;


                switch (search.SearchType)
                {
                    case "0":
                        {
                            offcieEcucationName = search.SearchKey;
                            break;
                        }
                    case "1":
                        {
                            cssName = search.SearchKey;
                            break;
                        }
                    case "2":
                        {
                            lectureName = search.SearchKey;
                            break;
                        }
                    case "3":
                        {
                            createSchName = search.SearchKey;
                            break;
                        }
                    case "4":
                        {
                            targetSchNames = search.SearchKey;
                            break;
                        }
                }

                parameters.Add("@Year", search.Year);
                parameters.Add("@Semester", search.Semester);
                parameters.Add("@OfficeofEducationName", offcieEcucationName);
                parameters.Add("@CssName", cssName);
                parameters.Add("@TargetSchNames", targetSchNames);
                parameters.Add("@LectureName", lectureName);
                parameters.Add("@CreateSchName", createSchName);

                var ret = dbConnection.QueryMultiple("sp_AfterSchool_Lecture_Item_Select", param: parameters, commandType: CommandType.StoredProcedure);

                lstAfterSchoolLecture = ret.Read<AfterSchool_Lecture>().ToList();
                return lstAfterSchoolLecture;
            }
        }

        public List<SchoolInfos> GetAfterSchoolItem(AfterShoolLectureSearch search = null)
        {
            List<SchoolInfos> lstAfterSchoolLecture = new List<SchoolInfos>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureType", search.LectureType);

                string offcieEcucationName = string.Empty;
                string cssName = string.Empty;
                string targetSchNames = string.Empty;
                string lectureName = string.Empty;
                string createSchName = string.Empty;


                switch (search.SearchType)
                {
                    case "0":
                        {
                            offcieEcucationName = search.SearchKey;
                            break;
                        }
                    case "1":
                        {
                            cssName = search.SearchKey;
                            break;
                        }
                    case "2":
                        {
                            lectureName = search.SearchKey;
                            break;
                        }
                    case "3":
                        {
                            createSchName = search.SearchKey;
                            break;
                        }
                    case "4":
                        {
                            targetSchNames = search.SearchKey;
                            break;
                        }
                }

                parameters.Add("@Year", search.Year);
                parameters.Add("@Semester", search.Semester);
                parameters.Add("@OfficeofEducationName", offcieEcucationName);
                parameters.Add("@CssName", cssName);
                parameters.Add("@TargetSchNames", targetSchNames);
                parameters.Add("@LectureName", lectureName);
                parameters.Add("@CreateSchName", createSchName);

                var ret = dbConnection.QueryMultiple("sp_AfterSchool_SchoolInfo_Item_Select", param: parameters, commandType: CommandType.StoredProcedure);
                lstAfterSchoolLecture = ret.Read<SchoolInfos>().ToList();
                return lstAfterSchoolLecture;
            }
        }

        public int InitSetMember(member member)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@midx", member.midx);
                parameters.Add("@uname", member.uname);
                parameters.Add("@email", member.email);
                parameters.Add("@telcom", member.telcom);
                parameters.Add("@phone1", member.phone1);
                parameters.Add("@phone2", member.phone2);
                parameters.Add("@preception", member.preception);
                parameters.Add("@pwd", member.pwd);
                parameters.Add("@gtype", member.gtype);
                parameters.Add("@mtype", member.mtype);
                parameters.Add("@school", member.school);
                parameters.Add("@grade", member.grade);
                parameters.Add("@mclass", member.mclass);
                parameters.Add("@no", member.no);
                parameters.Add("@agree", member.agree);

                parameters.Add("@used", member.used);
                parameters.Add("@midx2", member.midx2);
                parameters.Add("@sch_code", member.sch_code);
                parameters.Add("@avator", member.avator);
                parameters.Add("@sch_link_code", member.sch_link_code);
                parameters.Add("@isadmin", member.isadmin);
                parameters.Add("@EnterSchoolConference", member.EnterSchoolConference);
                parameters.Add("@EnterSchoolSupportGroup", member.EnterSchoolSupportGroup);

                var result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Member_Insert", parameters, commandType: CommandType.StoredProcedure);

                return result;
            }
        }


        public member GetAfterSchoolLectureTeacher(int midx)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                try
                {
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@midx", midx);
                    var ret = dbConnection.Query<member>("sp_AfterSchool_Lecture_Techer_Login", param: parameters, commandType: CommandType.StoredProcedure);
                    return ret.ToList()[0];
                }
                catch
                {
                    return null;
                }
            }
        }


        public void GetMemberList(ViewModel<member, member> model)
        {
            int totalCount = 0;
            List<member> lstMember = new List<member>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();

                parameters.Add("@email", model.Search.email);
                parameters.Add("@uname", model.Search.uname);
                parameters.Add("@mtype", model.Search.mtype);
                parameters.Add("@currentPage", model.PageIndex);
                parameters.Add("@pageSize", model.PageSize);
                parameters.Add("@totalCount", totalCount, DbType.Int32, ParameterDirection.Output);
                var ret = dbConnection.Query<member>("sp_AfterSchool_Member_Search", param: parameters, commandType: CommandType.StoredProcedure);

                model.List = ret.ToList();
                model.TotalCount = parameters.Get<int>("@totalCount");
            }
        }

        public int SetAfterLecture(member m)
        {
            int rVal = 0;
            try
            {
                using (IDbConnection dbConnection = base.OpenConnection())
                {
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@midx", m.midx);
                    parameters.Add("@Role", m.Role);
                    parameters.Add("@IsUsed", m.IsUsed);

                    rVal = dbConnection.Execute("sp_AfterSchool_Lecture_Teacher_Update", param: parameters, commandType: CommandType.StoredProcedure);
                }
                return rVal;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public AfterSchool_Lecture_Files GetFileSaveName(int seq)
        {
            AfterSchool_Lecture_Files f = new AfterSchool_Lecture_Files();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                try
                {
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@LectureSeq", seq);

                    var ret = dbConnection.Query<AfterSchool_Lecture_Files>("sp_AfterSchool_Lecture_File_Select", param: parameters, commandType: CommandType.StoredProcedure);

                    f = ret.ToList()[0];
                    return f;
                }

                catch
                {
                    return null;
                }

            }
        }


        public List<AfterSchool_Lecture> GetState(int type, int detail, string key)
        {
            List<AfterSchool_Lecture> lstAfterSchoolLecture = new List<AfterSchool_Lecture>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@StatType", type);
                parameters.Add("@StatDetailType", detail);
                if ((type == 1 && detail == 1) || (type == 2 && detail == 1))
                {
                    if (string.IsNullOrEmpty(key))
                    {
                        parameters.Add("@LectureSeq", null);
                    }
                    else
                    {
                        parameters.Add("@LectureSeq", Convert.ToInt32(key));
                    }

                    parameters.Add("@SchCode", null);
                }

                else
                {
                    parameters.Add("@LectureSeq", null);
                    parameters.Add("@SchCode", key);
                }


                var ret = dbConnection.QueryMultiple("sp_AfterSchool_Lecture_State_Select", param: parameters, commandType: CommandType.StoredProcedure);

                lstAfterSchoolLecture = ret.Read<AfterSchool_Lecture>().ToList();
                return lstAfterSchoolLecture;
            }
        }

        public List<AfterSchool_Lecture_Request> GetState_User(int type, int detail, string key)
        {
            List<AfterSchool_Lecture_Request> lstAfterSchoolLecture = new List<AfterSchool_Lecture_Request>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@StatType", type);
                parameters.Add("@StatDetailType", detail);
                if ((type == 1 && detail == 1) || (type == 2 && detail == 1))
                {
                    if (string.IsNullOrEmpty(key))
                    {
                        parameters.Add("@LectureSeq", null);
                    }
                    else
                    {
                        parameters.Add("@LectureSeq", Convert.ToInt32(key));
                    }
                    parameters.Add("@SchCode", null);
                }

                else
                {
                    parameters.Add("@LectureSeq", null);
                    parameters.Add("@SchCode", key);
                }


                var ret = dbConnection.QueryMultiple("sp_AfterSchool_Lecture_State_Select", param: parameters, commandType: CommandType.StoredProcedure);

                lstAfterSchoolLecture = ret.Read<AfterSchool_Lecture_Request>().ToList();

                return lstAfterSchoolLecture;
            }
        }
        public void GetAfterShcoolLectureRequest(ViewModel<AfterSchool_Lecture_Request, AfterSchool_Lecture> v_model)
        {
            List<AfterSchool_Lecture_Request> lstAfterLectureRequest = new List<AfterSchool_Lecture_Request>();
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@LectureSeq", v_model.Search.LectureSeq);

                var ret = dbConnection.QueryMultiple("sp_AfterSchool_Lecture_Request_Select", param: parameters, commandType: CommandType.StoredProcedure);
                v_model.Search = ret.Read<AfterSchool_Lecture>().ToList()[0];
                v_model.List = ret.Read<AfterSchool_Lecture_Request>().ToList();


            }
        }

        public int SetAfterLectureRequestStat(AfterSchool_Lecture_Request m)
        {
            int rVal = 0;
            try
            {
                using (IDbConnection dbConnection = base.OpenConnection())
                {
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@RequestSeq", m.RequestSeq);
                    parameters.Add("@State", m.State);

                    rVal = dbConnection.Execute("sp_AfterSchool_Lecture_Request_Update", param: parameters, commandType: CommandType.StoredProcedure);
                }
                return rVal;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        #endregion

    }
}
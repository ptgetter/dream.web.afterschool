using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [Table("member")]
    public partial class member : BaseEntity
    {
        /// <summary>
		/// idx
		/// </summary>
		[Key]
        [Required]
        public int midx { get; set; }


        /// <summary>
        /// 이름
        /// </summary>
        [Required]
        public string uname { get; set; }


        /// <summary>
        /// 이메일
        /// </summary>
        [Required]
        [StringLength(2)]
        public string email { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public string ereception { get; set; }


        /// <summary>
        /// 핸드폰 앞자리
        /// </summary>
        public string telcom { get; set; }


        /// <summary>
        /// 핸드폰 둘째자리
        /// </summary>
        public string phone1 { get; set; }


        /// <summary>
        /// 핸드폰 셋째자리
        /// </summary>
        public string phone2 { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public string preception { get; set; }


        /// <summary>
        /// 패스워드
        /// </summary>
        public string pwd { get; set; }


        /// <summary>
        /// 학교타입(E:초등학교, M:중학교, H:고등학교)
        /// </summary>
        public string gtype { get; set; }


        /// <summary>
        /// 사용자타입(S:학생, T:선생님, T1:??)
        /// </summary>
        public string mtype { get; set; }


        /// <summary>
        /// 학교이름
        /// </summary>
        public string school { get; set; }


        /// <summary>
        /// 학년
        /// </summary>
        public string grade { get; set; }


        /// <summary>
        /// 반
        /// </summary>
        public int? mclass { get; set; }


        /// <summary>
        /// 번호
        /// </summary>
        public string no { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public string agree { get; set; }


        /// <summary>
        /// 로그인 이력??
        /// </summary>
        public DateTime? logdate { get; set; }


        /// <summary>
        /// 등록 날짜??
        /// </summary>
        public DateTime? regdate { get; set; }


        /// <summary>
        /// 만료 날짜??
        /// </summary>
        public DateTime? outdate { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public DateTime? withdrawaldate { get; set; }


        /// <summary>
        /// 상담지역코드(사용안함)
        /// </summary>
        public int? CounselArea { get; set; }


        /// <summary>
        /// 사용여부?
        /// </summary>
        public string used { get; set; }


        /// <summary>
        /// 디버그 컬럼으로 임시사용(사용안함)
        /// </summary>
        public int? midx2 { get; set; }


        /// <summary>
        /// 학교코드(미사용)
        /// </summary>
        public string sch_code { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public string avator { get; set; }


        /// <summary>
        /// 협력 학교 코드???
        /// </summary>
        public string sch_link_code { get; set; }


        /// <summary>
        /// 관리자 유무
        /// </summary>
        public bool? isadmin { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public bool? EnterSchoolConference { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public bool? EnterSchoolSupportGroup { get; set; }


        /// <summary>
        /// 상담권역코드(사용안함)
        /// </summary>
        public int? ConsultPosition { get; set; }


        /// <summary>
        /// 인문계/자연계/예체능계/전문계 구분코드
        /// </summary>
        [StringLength(50)]
        public string UnitCode { get; set; }

        public string UnitCodeName { get; set; }



        /// <summary>
        /// 일반고/실업고 등 학교 유형 구분코드
        /// </summary>
        [StringLength(50)]
        public string SchoolType { get; set; }

        public string SchoolTypeName { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        public DateTime? CreatedDate { get; set; }


        /// <summary>
        /// 수정날짜??
        /// </summary>
        public DateTime? UpdatedDate { get; set; }


        /// <summary>
        /// ??
        /// </summary>
        public string Note { get; set; }


        /// <summary>
        /// 성별(1:남자, 2:여자)
        /// </summary>
        public int? Gender { get; set; }


        #region 선생님관리 추가ㅣ

        public string TeacherName {get;set;}
        public string LoginId { get; set; }
        public string HP { get; set; }
        public string Password { get; set; }
        public string SchoolName { get; set; }

        public int IsUsed { get; set; }
        public int Role { get; set; }

        #endregion

    }
}

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("AfterSchool_Lecture_Files")]
    public partial class AfterSchool_Lecture_Files : BaseEntity
    {
        /// <summary>
		/// 업로더 ID
		/// </summary>
		[Key]
        [Required]
        public string UploaderID { get; set; }


        /// <summary>
        /// 업로더 Type(T: 선생님, S: 학생)
        /// </summary>
        [Required]
        public string UploaderType { get; set; }


        /// <summary>
        /// 강의 Seq
        /// </summary>
        [Required]
        public long LectureSeq { get; set; }

        public string FileType { get; set; }

        /// <summary>
        /// 파일 ID
        /// </summary>
        [Required]
        public string FileID { get; set; }


        /// <summary>
        /// 파일이름
        /// </summary>
        [Required]
        public string FileName { get; set; }


        /// <summary>
        /// 파일크기
        /// </summary>
        [Required]
        public string FileSize { get; set; }


        /// <summary>
        /// 파일경로
        /// </summary>
        [Required]
        public string FilePath { get; set; }


        /// <summary>
        /// 학교코드
        /// </summary>
        [StringLength(20)]
        public string SchCode { get; set; }


        /// <summary>
        /// 삭제여부(0: 사용, 1: 삭제)
        /// </summary>
        public bool? IsDelete { get; set; }


        /// <summary>
        /// 생성날짜
        /// </summary>
        public DateTime? CreateDate { get; set; }


        /// <summary>
        /// 삭제날짜
        /// </summary>
        public DateTime? DeleteDate { get; set; }


        //public string FileType { get; set; }

    }
}

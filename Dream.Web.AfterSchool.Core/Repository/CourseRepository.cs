using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// CourseRepository
    /// </summary>
    public class CourseRepository : BaseRepository
    {
        /// <summary>
        /// Course 목록 조회
        /// </summary>
        /// <param name="entity">Course</param>
        /// <returns></returns>
        public List<Course> CourseSelect(Course entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                

                var ret = dbConnection.Query<Course>("CourseSelect", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// Course 페이징 목록 조회
        /// </summary>
        /// <param name="entity">Course</param>
        /// <returns></returns>
        public void CourseSelectPaging(ViewModel<Course> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@SortField", entity.SortField);
                parameters.Add("@SortOrder", entity.SortOrder);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.Query<Course>("CourseSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);
                entity.List = ret.ToList();
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// Course 항목 조회
        /// </summary>
        /// <param name="entity">Course</param>
        /// <returns></returns>
        public Course CourseSelectItem(Course entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                

                var ret = dbConnection.Query<Course>("CourseSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// Course 등록
        /// </summary>
        /// <param name="entity">Course</param>
        /// <returns></returns>
        public int CourseInsert(Course entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@CssCode", entity.CssCode);
				parameters.Add("@RegionName", entity.RegionName);
				parameters.Add("@CssType", entity.CssType);
				parameters.Add("@CourseName", entity.CourseName);

                return dbConnection.Execute("CourseInsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Course 수정
        /// </summary>
        /// <param name="entity">Course</param>
        /// <returns></returns>
        public int CourseUpdate(Course entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@CssCode", entity.CssCode);
				parameters.Add("@RegionName", entity.RegionName);
				parameters.Add("@CssType", entity.CssType);
				parameters.Add("@CourseName", entity.CourseName);

                return dbConnection.Execute("CourseUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Course 등록 및 수정
        /// </summary>
        /// <returns></returns>
        public int CourseUpsert(Course entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@CssCode", entity.CssCode);
				parameters.Add("@RegionName", entity.RegionName);
				parameters.Add("@CssType", entity.CssType);
				parameters.Add("@CourseName", entity.CourseName);

                return dbConnection.Execute("CourseUpsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Course 삭제
        /// </summary>
        /// <param name="entity">Course</param>
        /// <returns></returns>
        public int CourseDelete(Course entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                

                return dbConnection.Execute("CourseDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
using Dream.Web.AfterSchool.Core.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dream.Web.AfterSchool.Core.Repository
{
    /// <summary>
    /// memberRepository
    /// </summary>
    public class memberRepository : BaseRepository
    {
        /// <summary>
        /// 로그???�인
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public member LoginMember(member entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@EMAIL", entity.email.Replace(" ",""));
                parameters.Add("@PWD", entity.pwd);

                var ret = dbConnection.Query<member>("LoginMember", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// member 목록 조회
        /// </summary>
        /// <param name="entity">member</param>
        /// <returns></returns>
        public List<member> memberSelect(member entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);

                var ret = dbConnection.Query<member>("memberSelect", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.ToList();
            }
        }

        /// <summary>
        /// member ?�이�?목록 조회
        /// </summary>
        /// <param name="entity">member</param>
        /// <returns></returns>
        public void memberSelectPaging(ViewModel<member> entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PageIndex", entity.PageIndex);
                parameters.Add("@PageSize", entity.PageSize);
                parameters.Add("@SortField", entity.SortField);
                parameters.Add("@SortOrder", entity.SortOrder);
                parameters.Add("@SearchText", entity.SearchText);
                parameters.Add("@TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var ret = dbConnection.Query<member>("memberSelectPaging", param: parameters, commandType: CommandType.StoredProcedure);
                entity.List = ret.ToList();
                entity.TotalCount = parameters.Get<int>("@TotalCount");
            }
        }

        /// <summary>
        /// member ??�� 조회
        /// </summary>
        /// <param name="entity">member</param>
        /// <returns></returns>
        public member memberSelectItem(member entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);

                var ret = dbConnection.Query<member>("memberSelectItem", param: parameters, commandType: CommandType.StoredProcedure);
                return ret.FirstOrDefault();
            }
        }

        /// <summary>
        /// member ?�록
        /// </summary>
        /// <param name="entity">member</param>
        /// <returns></returns>
        public int memberInsert(member entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);
				parameters.Add("@uname", entity.uname);
				parameters.Add("@email", entity.email);
				parameters.Add("@ereception", entity.ereception);
				parameters.Add("@telcom", entity.telcom);
				parameters.Add("@phone1", entity.phone1);
				parameters.Add("@phone2", entity.phone2);
				parameters.Add("@preception", entity.preception);
				parameters.Add("@pwd", entity.pwd);
				parameters.Add("@gtype", entity.gtype);
				parameters.Add("@mtype", entity.mtype);
				parameters.Add("@school", entity.school);
				parameters.Add("@grade", entity.grade);
				parameters.Add("@mclass", entity.mclass);
				parameters.Add("@no", entity.no);
				parameters.Add("@agree", entity.agree);
				parameters.Add("@logdate", entity.logdate);
				parameters.Add("@regdate", entity.regdate);
				parameters.Add("@outdate", entity.outdate);
				parameters.Add("@withdrawaldate", entity.withdrawaldate);
				parameters.Add("@CounselArea", entity.CounselArea);
				parameters.Add("@used", entity.used);
				parameters.Add("@midx2", entity.midx2);
				parameters.Add("@sch_code", entity.sch_code);
				parameters.Add("@avator", entity.avator);
				parameters.Add("@sch_link_code", entity.sch_link_code);
				parameters.Add("@isadmin", entity.isadmin);
				parameters.Add("@EnterSchoolConference", entity.EnterSchoolConference);
				parameters.Add("@EnterSchoolSupportGroup", entity.EnterSchoolSupportGroup);
				parameters.Add("@ConsultPosition", entity.ConsultPosition);
				parameters.Add("@UnitCode", entity.UnitCode);
				parameters.Add("@SchoolType", entity.SchoolType);
				parameters.Add("@CreateDate", entity.CreatedDate);
				parameters.Add("@UpdateDate", entity.UpdatedDate);
				parameters.Add("@Note", entity.Note);
				parameters.Add("@Gender", entity.Gender);

                return dbConnection.Execute("memberInsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// member ?�정
        /// </summary>
        /// <param name="entity">member</param>
        /// <returns></returns>
        public int memberUpdate(member entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);
				parameters.Add("@uname", entity.uname);
				parameters.Add("@email", entity.email);
				parameters.Add("@ereception", entity.ereception);
				parameters.Add("@telcom", entity.telcom);
				parameters.Add("@phone1", entity.phone1);
				parameters.Add("@phone2", entity.phone2);
				parameters.Add("@preception", entity.preception);
				parameters.Add("@pwd", entity.pwd);
				parameters.Add("@gtype", entity.gtype);
				parameters.Add("@mtype", entity.mtype);
				parameters.Add("@school", entity.school);
				parameters.Add("@grade", entity.grade);
				parameters.Add("@mclass", entity.mclass);
				parameters.Add("@no", entity.no);
				parameters.Add("@agree", entity.agree);
				parameters.Add("@logdate", entity.logdate);
				parameters.Add("@regdate", entity.regdate);
				parameters.Add("@outdate", entity.outdate);
				parameters.Add("@withdrawaldate", entity.withdrawaldate);
				parameters.Add("@CounselArea", entity.CounselArea);
				parameters.Add("@used", entity.used);
				parameters.Add("@midx2", entity.midx2);
				parameters.Add("@sch_code", entity.sch_code);
				parameters.Add("@avator", entity.avator);
				parameters.Add("@sch_link_code", entity.sch_link_code);
				parameters.Add("@isadmin", entity.isadmin);
				parameters.Add("@EnterSchoolConference", entity.EnterSchoolConference);
				parameters.Add("@EnterSchoolSupportGroup", entity.EnterSchoolSupportGroup);
				parameters.Add("@ConsultPosition", entity.ConsultPosition);
				parameters.Add("@UnitCode", entity.UnitCode);
				parameters.Add("@SchoolType", entity.SchoolType);
				parameters.Add("@CreateDate", entity.CreatedDate);
				parameters.Add("@UpdateDate", entity.UpdatedDate);
				parameters.Add("@Note", entity.Note);
				parameters.Add("@Gender", entity.Gender);

                return dbConnection.Execute("memberUpdate", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// member ?�록 �??�정
        /// </summary>
        /// <returns></returns>
        public int memberUpsert(member entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);
				parameters.Add("@uname", entity.uname);
				parameters.Add("@email", entity.email);
				parameters.Add("@ereception", entity.ereception);
				parameters.Add("@telcom", entity.telcom);
				parameters.Add("@phone1", entity.phone1);
				parameters.Add("@phone2", entity.phone2);
				parameters.Add("@preception", entity.preception);
				parameters.Add("@pwd", entity.pwd);
				parameters.Add("@gtype", entity.gtype);
				parameters.Add("@mtype", entity.mtype);
				parameters.Add("@school", entity.school);
				parameters.Add("@grade", entity.grade);
				parameters.Add("@mclass", entity.mclass);
				parameters.Add("@no", entity.no);
				parameters.Add("@agree", entity.agree);
				parameters.Add("@logdate", entity.logdate);
				parameters.Add("@regdate", entity.regdate);
				parameters.Add("@outdate", entity.outdate);
				parameters.Add("@withdrawaldate", entity.withdrawaldate);
				parameters.Add("@CounselArea", entity.CounselArea);
				parameters.Add("@used", entity.used);
				parameters.Add("@midx2", entity.midx2);
				parameters.Add("@sch_code", entity.sch_code);
				parameters.Add("@avator", entity.avator);
				parameters.Add("@sch_link_code", entity.sch_link_code);
				parameters.Add("@isadmin", entity.isadmin);
				parameters.Add("@EnterSchoolConference", entity.EnterSchoolConference);
				parameters.Add("@EnterSchoolSupportGroup", entity.EnterSchoolSupportGroup);
				parameters.Add("@ConsultPosition", entity.ConsultPosition);
				parameters.Add("@UnitCode", entity.UnitCode);
				parameters.Add("@SchoolType", entity.SchoolType);
				parameters.Add("@CreateDate", entity.CreatedDate);
				parameters.Add("@UpdateDate", entity.UpdatedDate);
				parameters.Add("@Note", entity.Note);
				parameters.Add("@Gender", entity.Gender);

                return dbConnection.Execute("memberUpsert", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// member ??��
        /// </summary>
        /// <param name="entity">member</param>
        /// <returns></returns>
        public int memberDelete(member entity)
        {
            using (IDbConnection dbConnection = base.OpenConnection())
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@midx", entity.midx);

                return dbConnection.Execute("memberDelete", param: parameters, commandType: CommandType.StoredProcedure);
            }
        }        
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Dream.Web.AfterSchool.Startup))]
namespace Dream.Web.AfterSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

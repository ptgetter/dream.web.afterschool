﻿using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.Controllers
{
    [Authorize]
    public class TeacherManageMentController : Controller
    {
        // GET: TeacherManageMent
        public ActionResult Index()
        {
            if (AppIdentity.UserType != "1")
            {
                if (AppIdentity.UserType == "STUDENT")
                {
                    return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
                }
                else
                {
                    return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureManageMent/List?type=1"";</script>");
                }
            }

            return View();
        }

        public ActionResult List(member search = null, int page = 1 )
        {
            if (AppIdentity.UserType != "1")
            {
                if (AppIdentity.UserType == "STUDENT")
                {
                    return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
                }
                else
                {
                    return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureManageMent/List?type=1"";</script>");
                }
            }

            ViewBag.SubTitle = "기본관리";
            List<member> model = new List<member>();
            if (search == null)
            {
                ViewModel<member, member> v_modal = new ViewModel<member, member>()
                {
                    List = model
                   ,Search = new member()
                    {
                        uname = ""
                      , email = ""
                      , mtype = "!S"
                    }
                   ,PageSize = 10
                   ,PageIndex = page
                };
                AfterSchool_LectureRepository.Instance.GetMemberList(v_modal);
                return View(v_modal);
            }
            else
            {
                search.mtype = "!S";
                ViewModel<member, member> v_modal = new ViewModel<member, member>()
                {
                    List = model
                   ,Search = search
                   ,PageSize = 10
                   ,PageIndex = page
                };
                AfterSchool_LectureRepository.Instance.GetMemberList(v_modal);
                return View(v_modal);
            }

            
        }

        public ActionResult Modify(int seq)
        {
            if (AppIdentity.UserType != "1")
            {
                if (AppIdentity.UserType == "STUDENT")
                {
                    return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureRequest/LectureList"";</script>");
                }
                else
                {
                    return Content(@"<script type=""text/javascript"">alert(""권한이 없습니다.""); location.href = ""/LectureManageMent/List?type=1"";</script>");
                }
            }

            ViewBag.SubTitle = "기본관리";
            member model = new member();
            model = AfterSchool_LectureRepository.Instance.GetMemer(seq)[0];
            return View(model);
        }

        [HttpPost]
        public JsonResult SetAfterLectureTecher(int midx, int isused, int role)
        {
            if (AppIdentity.UserType != "1")
            {
                return Json(new { jsonMsg = "F" });
            }

            try
            {
                member m = new member
                {
                    midx = midx
                   ,
                    IsUsed = isused
                   ,
                    Role = role
                };

                AfterSchool_LectureRepository.Instance.SetAfterLecture(m);
                return Json(new { jsonMsg = "S" });
            }
            catch(Exception ex)
            {
                return Json(new { jsonMsg = "F" });
            }
        }
    }
}
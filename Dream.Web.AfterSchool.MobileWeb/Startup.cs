﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Dream.Web.AfterSchool.MobileWeb.Startup))]
namespace Dream.Web.AfterSchool.MobileWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

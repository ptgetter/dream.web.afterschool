using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("PortalPopup")]
    public partial class PortalPopup : BaseEntity
    {
        public int Idx { get; set; }

        [StringLength(1)]
        public string PopupType { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsUse { get; set; }
    }
}

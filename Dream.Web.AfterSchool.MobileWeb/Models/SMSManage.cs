﻿using Dream.Web.AfterSchool.Core.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Dream.Web.AfterSchool.MobileWeb.Models
{
    public class SMSManage
    {
        #region Singleton Instance

        private static volatile SMSManage _instance;

        private readonly string SMSId;
        private readonly string SMSKey;
        private readonly string SMSSender;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static SMSManage Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(SMSManage))
                    {
                        if (_instance == null)
                        {
                            _instance = new SMSManage();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion

        public SMSManage()
        {
            SMSId = ConfigurationManager.AppSettings.Get("SMSId");
            SMSKey = ConfigurationManager.AppSettings.Get("SMSKey");
            SMSSender = ConfigurationManager.AppSettings.Get("SMSSender");
        }

        public string SendSMSCode(string code, string sender)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://apis.aligo.in/send/");
                HttpWebResponse response = null;
                string postData = string.Format("user_id={0}&key={1}&sender={2}&receiver={3}&msg=[전남진로진학포털] 본인확인 인증번호 [{4}] (5분간 유효합니다.)", SMSId, SMSKey, SMSSender, sender, code);
                byte[] data = Encoding.UTF8.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();

                    response = (HttpWebResponse)request.GetResponse();
                }

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return string.Empty;
                }
                else
                {
                    return response.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                //SetExceptionInfo(ex);
                return ex.ToString();
            }
        }

        public string RndNumber(int digit = 999999)
        {
            Random random = new Random();

            return random.Next(digit).ToString(string.Format("D{0}", digit.ToString().Length));
        }
    }
}

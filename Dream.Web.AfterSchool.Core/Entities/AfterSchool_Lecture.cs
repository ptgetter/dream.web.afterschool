using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dream.Web.AfterSchool.Core.Entities
{
	/// <summary>
    /// 
    /// </summary>
    [Table ("AfterSchool_Lecture")]
    public partial class AfterSchool_Lecture : BaseEntity
    {
        /// <summary>
		/// idx
		/// </summary>
		[Key]
		[Required]
		public long LectureSeq { get; set; }


        /// <summary>
        /// 개설년도
        /// </summary>
        [Required]
		public int Year { get; set; }


        /// <summary>
        /// 학기(0:전체, 1:1학기, 2:2학기)
        /// </summary>
        [Required]
		public int Semester { get; set; }


        /// <summary>
        /// 강의 타입(0:개방형, 1:밴드형)
        /// </summary>
        [Required]
		public int LectureType { get; set; }


        /// <summary>
        /// 관련 교과 Code
        /// </summary>
        [Required]
		public int CssCode { get; set; }


        /// <summary>
        /// 강의명
        /// </summary>
        [Required]
		[StringLength(100)]
		public string LectureName { get; set; }


        /// <summary>
        /// 수업시수
        /// </summary>
        public int? MaxTime { get; set; }


        /// <summary>
        /// 개설학교 코드
        /// </summary>
        [Required]
		[StringLength(20)]
		public string CreateSchCode { get; set; }


        /// <summary>
        /// 신청정원
        /// </summary>
        [Required]
		public int MaxSignUp { get; set; }


        /// <summary>
        /// 신청가능학교 학교코드 모음
        /// </summary>
        public string TargetSchCodes { get; set; }


        /// <summary>
        /// 수업시작일
        /// </summary>
        public DateTime? OperationStartDate { get; set; }


        /// <summary>
        /// 수업종료일
        /// </summary>
        public DateTime? OperationEndDate { get; set; }


        /// <summary>
        /// 대상학년
        /// </summary>
        public string TargetGrade { get; set; }


        /// <summary>
        /// 대상성별(0:전체, 1:남자, 2:여자)
        /// </summary>
        public int? TargetGender { get; set; }


        /// <summary>
        /// 접수시작일
        /// </summary>
        public DateTime? Reception { get; set; }


        /// <summary>
        /// 접수종료일
        /// </summary>
        public DateTime? ReceptionClosed { get; set; }


        /// <summary>
        /// 상태(0:종료, 1:운영, 3:접수, 4:마감, 5:대기)
        /// </summary>
        public int? State { get; set; }

        /// <summary>
        /// 강의실
        /// </summary>
        public string Place { get; set; }

        /// <summary>
        /// 교사명
        /// </summary>
        public string TeacherName { get; set; }

        /// <summary>
        /// 교과명
        /// </summary>
        public string NssName { get; set; }

        /// <summary>
        /// 알림란
        /// </summary>
        public string InfoMessage { get; set; }


        /// <summary>
        /// 등록자 ID
        /// </summary>
        [StringLength(100)]
        public string RegID { get; set; }


        /// <summary>
        /// 등록날짜
        /// </summary>
        public DateTime? RegDate { get; set; }


        /// <summary>
        /// 수정자 ID
        /// </summary>
        [StringLength(100)]
        public string ModifyID { get; set; }


        /// <summary>
        /// 수정날짜
        /// </summary>
        public DateTime? ModifyDate { get; set; }


        #region MLML추가 

        public string RowIdx { get; set; }
        public string CreateSchName { get; set; }
        public string LectureTime { get; set; }
        public string StateName { get; set; }
        public string TargetGenderStr { get; set; }
        public string RequestSum { get; set; }
        public string FileName { get; set; }
        public string FileId { get; set; }
        public string TargetSchNames { get; set; }

        //public string Place { get; set; }
        public string TehacherName { get; set; }
        //public string NssName { get; set; }
        //public string InfoMessage { get; set; }

        public List<AfterSchool_Lecture_Time> AfterSchoolLectureTimes { get; set; }
        public List<AfterSchool_Lecture_School> AfterSchoolLectureSchool { get; set; }
        public List<AfterSchool_Lecture_Files> AfterSchoolLectureFiles { get; set; }

        #endregion

    }
}

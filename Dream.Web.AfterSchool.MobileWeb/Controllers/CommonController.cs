﻿using Dream.Web.AfterSchool.MobileWeb.Models;
using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Dream.Web.AfterSchool.MobileWeb.Controllers
{
    public class CommonController : BaseController
    {
        public ActionResult UserInformation()
        {
            baseModel.Title = "사용자 정보";

            memberRepository mRepository = new memberRepository();
            SchoolInfosRepository sRepository = new SchoolInfosRepository();

            var user = mRepository.memberSelectItem(new member() { midx = baseModel.midx });
            var schools = sRepository.SchoolInfosSelect(new SchoolInfos());
            var model = new UserInformationViewModel(baseModel)
            {
                member = user,
                schoolInfos = schools
            };
            

            return View(model);
        }

        [HttpPost]
        public JsonResult SetUserInformation(member member)
        {
            try
            {
                memberRepository mRepository = new memberRepository();
                mRepository.memberUpdate(member);

                var updatemember = mRepository.memberSelectItem(member);

                var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Email, ClaimTypes.Role);
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, updatemember.midx.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Name, updatemember.uname));
                identity.AddClaim(new Claim("schoolName", updatemember.school));
                identity.AddClaim(new Claim(ClaimTypes.Email, updatemember.email));
                identity.AddClaim(new Claim(ClaimTypes.Role, "AfterSchool"));

                if (string.IsNullOrEmpty(updatemember.sch_code) || string.IsNullOrEmpty(updatemember.grade) || updatemember.mclass == null || string.IsNullOrEmpty(updatemember.no) || string.IsNullOrEmpty(updatemember.uname) || string.IsNullOrEmpty(updatemember.sch_code))
                {
                    identity.AddClaim(new Claim("setuserinfo", "Y"));
                }

                HttpContext.GetOwinContext().Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties { IsPersistent = false });

                return Json(new
                {
                    ok = true,
                    message = "수정하였습니다."
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = true,
                    message = "오류가 발생하였습니다."
                });
            }
        }
    }
}
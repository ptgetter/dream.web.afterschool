﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Dream.Web.AfterSchool.Core.Entities;
using Dream.Web.AfterSchool.Core.Repository;
using System.Threading;
using System.Security.Principal;
using System.Collections.Generic;
using System.Web.Security;
using MainApp.Common.Connection;
using System.Data;
using Dream.Web.AfterSchool.Core.Util;
using Dream.Web.AfterSchool.Models;
using DreamPlus.Web.Consult.Models;
using System.Text.RegularExpressions;

namespace Dream.Web.AfterSchool.Controllers
{
    [Authorize]
    public class AccountController :BaseController
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel() { Title = "로그인" });
        }
        [AllowAnonymous]
        public ActionResult LoginTest(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel() { Title = "로그인" });
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl = "/LectureManageMent/List?type=1")
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(model);
            //}
            try
            {
                string pwd = WEBEncryptHelper.Encrypt(model.Password);

            }
            catch
            {
                return View(model);
            }
            memberRepository repository = new memberRepository();

            //개발계
            member loginMember = repository.LoginMember(new member() { email = model.Email, pwd = WEBEncryptHelper.Encrypt(model.Password) });

            //운영계(전남서버쿼리)
            //member loginMember = GetMemberInfo("1");

            
            if (loginMember == null)
            {
                ViewBag.Result = "F";
                return View();
            }
            else
            {
                ViewBag.Result = "T";

                if (loginMember.mtype != "S")
                {
                    member teacher = AfterSchool_LectureRepository.Instance.GetAfterSchoolLectureTeacher(loginMember.midx);

                    loginMember.Role = teacher == null ? 0 : teacher.Role;
                    loginMember.IsUsed = teacher == null ? 0 : teacher.IsUsed;

                    if (teacher.IsUsed == 1)
                    {
                        //if (string.IsNullOrEmpty(url))
                        //{
                            returnUrl = Url.Action("List", "LectureManageMent") + "?type=1";
                        //}
                    }
                    else
                    {
                        returnUrl = "/Account/Info";
                    }
                }
                else
                {
                    loginMember.IsUsed = 1;

                    //if (string.IsNullOrEmpty(url))
                    //{
                        returnUrl = Url.Action("LectureList", "LectureRequest");
                    //}
                }

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                await SetAuthorization(loginMember);


                //var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Email, ClaimTypes.Role);
                //if (loginMember.mtype != "S")
                //{
                //    member teacherMember = AfterSchool_LectureRepository.Instance.GetAfterSchoolLectureTeacher(Convert.ToInt32(loginMember.midx));

                //    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginMember.midx.ToString()));
                //    identity.AddClaim(new Claim(ClaimTypes.Email, loginMember.email));
                //    identity.AddClaim(new Claim(ClaimTypes.Role, teacherMember.Role.ToString()));
                //    identity.AddClaim(new Claim("mtype", loginMember.mtype));
                //    identity.AddClaim(new Claim("SchoolName", string.IsNullOrEmpty(loginMember.school) ? "드림고등학교" : loginMember.school));
                    
                //    if (teacherMember.IsUsed == 1)
                //    {
                //        returnUrl = "/LectureManageMent/List?type=0";
                //    }
                //    else
                //    {
                //        returnUrl = "/Account/Info";
                //    }
                //}
                //else
                //{
                //    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginMember.midx.ToString()));
                //    identity.AddClaim(new Claim(ClaimTypes.Email, loginMember.email));
                //    identity.AddClaim(new Claim(ClaimTypes.Role, "STUDENT"));
                //    identity.AddClaim(new Claim("mtype", loginMember.mtype));
                //    identity.AddClaim(new Claim("SchoolName", string.IsNullOrEmpty(loginMember.school)?"드림고등학교":loginMember.school));
                //    returnUrl = "/LectureRequest/LectureList";

                //    if (string.IsNullOrEmpty(loginMember.sch_code) || string.IsNullOrEmpty(loginMember.grade) || loginMember.mclass == null || string.IsNullOrEmpty(loginMember.no) || string.IsNullOrEmpty(loginMember.uname) || string.IsNullOrEmpty(loginMember.sch_code))
                //    {
                //        identity.AddClaim(new Claim("setuserinfo", "Y"));
                //    }
                //}

                

                //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                //AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);

                return Redirect(returnUrl);
            }

        }

       
        //[AllowAnonymous]
        //public ActionResult sso(string m="5570", string url = null)
        //{
        //    try
        //    {
        //        m = m.Replace("hfdkshjfkdsljfa", "").Replace("fjdkjfksdf", "");
        //        string returnUrl = string.Empty;

        //        if (AppIdentity.Midx != m)
        //        {
        //            if (m == "null" || m == "")
        //            {
        //                //return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.""); location.href = ""https://jdream.jne.go.kr/UAccount/Login"";</script>");
        //                return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.""); location.href = ""http://tempjdream.futureplan.co.kr/UAccount/Login"";</script>");
        //            }
        //            else
        //            {
        //                //개발계
        //                //member loginMember = AfterSchool_LectureRepository.Instance.GetMemer(Convert.ToInt32(m))[0];
        //                //future_tch_01@jne.go.kr
        //                //future_tch_02@jne.go.kr

        //                //운영계(전남서버쿼리)
        //                member loginMember = GetMemberInfo(m);
        //                if (loginMember == null)
        //                {
        //                    ViewBag.Result = "로그인안됨";
        //                    //return View();
        //                    //return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.""); location.href = ""https://jdream.jne.go.kr/UAccount/Login"";</script>");
        //                    return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.""); location.href = ""http://tempjdream.futureplan.co.kr/UAccount/Login"";</script>");
        //                }
        //                else
        //                {
        //                    memberRepository repository = new memberRepository();
        //                    loginMember = repository.memberSelectItem(new member { midx = loginMember.midx });
        //                    var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Email, ClaimTypes.Role);
        //                    if (loginMember.mtype != "S")
        //                    {
        //                        member teacherMember = AfterSchool_LectureRepository.Instance.GetAfterSchoolLectureTeacher(Convert.ToInt32(m));

        //                        identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginMember.midx.ToString()));
        //                        identity.AddClaim(new Claim(ClaimTypes.Email, loginMember.email));
        //                        identity.AddClaim(new Claim(ClaimTypes.Role, teacherMember.Role.ToString()));
        //                        identity.AddClaim(new Claim("SchoolName", string.IsNullOrEmpty(loginMember.school) ? "드림고등학교" : loginMember.school));

        //                        if (teacherMember.IsUsed == 1)
        //                        {
        //                            if (string.IsNullOrEmpty(url))
        //                            {
        //                                returnUrl = Url.Action("List", "LectureManageMent") + "?type=1";
        //                            }
        //                            else
        //                            {
        //                                returnUrl = url;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            returnUrl = "/Account/Info";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginMember.midx.ToString()));
        //                        identity.AddClaim(new Claim(ClaimTypes.Email, loginMember.email));
        //                        identity.AddClaim(new Claim(ClaimTypes.Role, "STUDENT"));
        //                        identity.AddClaim(new Claim("SchoolName", string.IsNullOrEmpty(loginMember.school) ? "드림고등학교" : loginMember.school));
                                
        //                        returnUrl = Url.Action("LectureList", "LectureRequest");

        //                        if (string.IsNullOrEmpty(loginMember.sch_code) || string.IsNullOrEmpty(loginMember.grade) || loginMember.mclass == null || string.IsNullOrEmpty(loginMember.no) || string.IsNullOrEmpty(loginMember.uname) || string.IsNullOrEmpty(loginMember.sch_code))
        //                        {
        //                            identity.AddClaim(new Claim("setuserinfo", "Y"));
        //                        }
        //                    }



        //                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);

        //                    return Redirect(returnUrl);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (string.IsNullOrEmpty(url))
        //            {
        //                returnUrl = Url.Action("List", "LectureManageMent") + "?type=1";
        //            }
        //            else
        //            {
        //                returnUrl = url;
        //            }

        //            return Redirect(returnUrl);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.""); location.href = ""https://jdream.jne.go.kr/UAccount/Login"";</script>");
        //        return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.""); location.href = ""http://tempjdream.futureplan.co.kr/UAccount/Login"";</script>");
        //    }
        //}

        [AllowAnonymous]
        public async Task<ActionResult> SSO(string data)
        {
            try
            {
                var ssoidentity = SSOAuth.Instance.Unprotect(data);

                if (ssoidentity != null && ssoidentity.FindFirst(ClaimTypes.Role).Value == "JDREAMSSO")
                {
                    string m = ssoidentity.FindFirst("midx").Value;
                    string url = ssoidentity.FindFirst("url").Value;

                    if (AppIdentity.Midx != m)
                    {
                        member loginMember = GetMemberInfo(m);
                        if (loginMember == null)
                        {
                            ViewBag.Result = "로그인안됨";
                            //return View();
                            //return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.""); location.href = ""https://jdream.jne.go.kr/UAccount/Login"";</script>");
                            return Content(@"<script type=""text/javascript"">alert(""사용자 정보가 없습니다.\r\n 본 서비스는 로그인 후 이용이 가능합니다.""); location.href = '" + SSOAuth.Instance.LoginURL + "';</script>");
                        }
                        else
                        {
                            memberRepository repository = new memberRepository();
                            loginMember = repository.memberSelectItem(new member { midx = loginMember.midx });

                            if (loginMember.mtype != "S")
                            {
                                member teacher = AfterSchool_LectureRepository.Instance.GetAfterSchoolLectureTeacher(loginMember.midx);

                                loginMember.Role = teacher == null ? 0 : teacher.Role;
                                loginMember.IsUsed = teacher == null ? 0 : teacher.IsUsed;

                                if (teacher.IsUsed == 1)
                                {
                                    if (string.IsNullOrEmpty(url))
                                    {
                                        url = Url.Action("List", "LectureManageMent") + "?type=1";
                                    }
                                }
                                else
                                {
                                    url = "/Account/Info";
                                }
                            }
                            else
                            {
                                loginMember.IsUsed = 1;

                                if (string.IsNullOrEmpty(url))
                                {
                                    url = Url.Action("LectureList", "LectureRequest");
                                }
                            }

                            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                            await SetAuthorization(loginMember);

                            //var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Email, ClaimTypes.Role);
                            //if (loginMember.mtype != "S")
                            //{
                            //    member teacherMember = AfterSchool_LectureRepository.Instance.GetAfterSchoolLectureTeacher(Convert.ToInt32(m));

                            //    identity.AddClaim(new Claim("MIdx", loginMember.midx.ToString()));
                            //    identity.AddClaim(new Claim("Email", loginMember.email));
                            //    identity.AddClaim(new Claim(ClaimTypes.Role, teacherMember.Role.ToString()));
                            //    identity.AddClaim(new Claim("School", string.IsNullOrEmpty(loginMember.school) ? "드림고등학교" : loginMember.school));

                            //    if (teacherMember.IsUsed == 1)
                            //    {
                            //        if (string.IsNullOrEmpty(url))
                            //        {
                            //            url = Url.Action("List", "LectureManageMent") + "?type=1";
                            //        }
                            //    }
                            //    else
                            //    {
                            //        url = "/Account/Info";
                            //    }
                            //}
                            //else
                            //{
                            //    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, loginMember.midx.ToString()));
                            //    identity.AddClaim(new Claim(ClaimTypes.Email, loginMember.email));
                            //    identity.AddClaim(new Claim(ClaimTypes.Role, "STUDENT"));
                            //    identity.AddClaim(new Claim("SchoolName", string.IsNullOrEmpty(loginMember.school) ? "드림고등학교" : loginMember.school));

                            //    if (string.IsNullOrEmpty(url))
                            //    {
                            //        url = Url.Action("LectureList", "LectureRequest");
                            //    }

                            //    if (string.IsNullOrEmpty(loginMember.sch_code) || string.IsNullOrEmpty(loginMember.grade) || loginMember.mclass == null || string.IsNullOrEmpty(loginMember.no) || string.IsNullOrEmpty(loginMember.uname) || string.IsNullOrEmpty(loginMember.sch_code))
                            //    {
                            //        identity.AddClaim(new Claim("setuserinfo", "Y"));
                            //    }
                            //}
                            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                            //AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);
                        }
                    }

                    if (url == "ssologin")
                    {
                        return Content("");
                    }
                    else if (string.IsNullOrEmpty(url))
                    {
                        if (AppIdentity.UserType != "S")
                        {
                            url = Url.Action("List", "LectureManageMent") + "?type=1";
                        }
                        else
                        {
                            url = Url.Action("LectureList", "LectureRequest");
                        }
                    }
                    
                    return Redirect(HttpUtility.UrlDecode(url));
                }
                else
                {
                    return Content(@"<script type=""text/javascript"">alert(""잘못된 인증 정보입니다.\r\n본 서비스는 로그인 후 이용이 가능합니다.""); location.href = '" + SSOAuth.Instance.LoginURL + "';</script>");
                }
            }
            catch (Exception ex)
            {
                return Content(@"<script type=""text/javascript"">alert(""본 서비스는 로그인 후 이용이 가능합니다.\r\n""" + ex.ToString() + "); location.href = '" + SSOAuth.Instance.LoginURL + "';</script>");
            }
        }

        [AllowAnonymous]
        public ActionResult Logout(LoginViewModel model, string returnUrl = "")
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            if (returnUrl == "ssologout")
            {
                return View();
            }
            else
            {
                return Redirect("../");
            }
        }

        public member GetMemberInfo(string idx)
        {
            try
            {
                member member = new member();
                WebService.Instance.WebServiceType = WebServiceType.JeollanamDo;
                DataTable dt = WebServiceUtil.ExecQuery(string.Format("Select Top 1 * From [university_link].[dbo].[member] where midx ='{0}'", idx)).Tables[0];
                DataRow dr = dt.Rows[0];

                member.midx = Convert.ToInt32(dr["midx"]);
                member.uname = dr["uname"].ToString();
                member.email = dr["email"].ToString();
                member.ereception = dr["ereception"].ToString();
                member.telcom = dr["telcom"].ToString();
                member.phone1 = dr["phone1"].ToString();
                member.phone2 = dr["phone2"].ToString();
                member.preception = dr["preception"].ToString();
                member.pwd = dr["pwd"].ToString();
                member.gtype = dr["gtype"].ToString();
                member.mtype = dr["mtype"].ToString();
                member.school = dr["school"].ToString();
                member.grade = dr["grade"].ToString();
                member.mclass = Convert.ToInt32(dr["mclass"].ToString());
                member.no = dr["no"].ToString();
                member.agree = dr["agree"].ToString();

                member.used = dr["used"].ToString();
                member.sch_code = dr["sch_code"].ToString();
                member.avator = dr["avator"].ToString();
                member.sch_link_code = dr["sch_link_code"].ToString();

                Regex ex = new Regex("학교$");
                if (!string.IsNullOrEmpty(member.school) && !ex.IsMatch(member.school))
                {
                    member.school = string.Concat(member.school, "학교");
                }
                else if (string.IsNullOrEmpty(member.school))
                {
                    member.school = "기타";
                }

                AfterSchool_LectureRepository.Instance.InitSetMember(member);

                return member;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [AllowAnonymous]
        public ActionResult Info()
        {

            //string aaa = WEBEncryptHelper.Decrypt("gi+uU04/19UT+uz8d0oXqg==");
            string aaa = string.Empty;
            ViewBag.Id = aaa; 
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        #region 도우미
        // 외부 로그인을 추가할 때 XSRF 보호에 사용
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
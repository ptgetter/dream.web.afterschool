﻿$(function () {

});

if (typeof AppCommon == 'undefined') { AppCommon = {}; }
if (typeof AppEvent == 'undefined') { AppEvent = {}; }
if (typeof AppLibraries == 'undefined') { AppLibraries = {}; }
if (typeof AppOptionalEducation == 'undefined') { AppOptionalEducation = {}; }

AppCommon =
{
    Ajax: function (url, jsonData, callback, error) {
        $.ajax({
            url: url,
            data: JSON.stringify(jsonData),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                $.isLoading({
                    text: "불러오는 중..",
                    position: "overlay",
                    class: "fa fa-spinner fa-spin"
                });
            },
            complete: function () {
                $.isLoading("hide");
            }
        })
        .done(function (json) {
                callback(json);
        })
        .error(function (msg) {
            if (error != null) {
                error(msg);
            }
            else {
                error("오류가 발생하였습니다.");
            }
        });
    },
    AjaxFile: function (url, jsonData, callback) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function (aa, bb) {
            var a;

            if (xhttp.readyState === 4 && xhttp.status === 200) {

                if (xhttp.response.size == 0) {
                    alert("작업 중 오류가 발생 하였습니다.");
                }
                else if (xhttp.response.type.indexOf("text/html") > -1 ) {
                    var fileReader = new FileReader();
                    fileReader.onload = function (e) {
                        alert(e.srcElement.result);
                    }
                    fileReader.readAsText(xhttp.response);
                }
                else {
                    var filename = "";
                    var disposition = xhttp.getResponseHeader('Content-Disposition');
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) {
                            filename = matches[1].replace(/['"]/g, '');

                            if (filename.indexOf("UTF-8") == 0) {
                                filename = decodeURIComponent(filename.substring(5, filename.length));
                            }
                        }
                    }

                    var type = xhttp.getResponseHeader('Content-Type');

                    var blob = typeof File === 'function'
                        ? new File([this.response], filename, { type: type })
                        : new Blob([this.response], { type: type });

                    if (window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(blob, filename);
                    }
                    else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob);

                        if (filename) {
                            var a = document.createElement("a");
                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = filename;
                                document.body.appendChild(a);
                                a.click();
                            }
                        } else {
                            window.location = downloadUrl;
                        }

                        setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 1000); // cleanup
                    }
                }
            }
            else {
                //alert("xhttp status : " + xhttp.readyState + " / " + xhttp.status);
            }
        };

        var data = JSON.stringify(jsonData);

        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        //xhttp.setRequestHeader("Content-Length", JSON.stringify(data).length);
        xhttp.responseType = 'blob';
        xhttp.send(data);
    }, 
    // Ajax Json Return
    AjaxJson: function (jsonData, url, callback) {
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(jsonData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.ErrorMessage != null) {
                    alert("Error : " + data.ErrorMessage);
                } else {
                    callback(data);
                }
                return;
            },
            error: function (msg) {
                alert("AjaxJson Error : " + msg.responseText);
            }
        });
    },
    // Ajax Html Return
    AjaxHtml: function (jsonData, url, callback) {
        $.ajax({
            type: "POST",
            url: url,
            data: jsonData,
            dataType: "html",
            success: function (data) {
                if (data.ErrorMessage != null) {
                    alert("Error : " + data.ErrorMessage);
                } else {
                    callback(data);
                }
                return;
            },
            error: function (msg) {
                alert("AjaxHtml Error : " + msg.responseText);
            }
        });
    },
    AjaxModal: function (jsonData, url, error) {
        $.ajax({
            type: "POST",
            url: url,
            data: jsonData,
            contentType: "application/json",
            beforeSend: function () {
                $.isLoading({
                    text: "불러오는 중..",
                    position: "overlay",
                    class: "fa fa-spinner fa-spin"
                });
            },
            complete: function () {
                $.isLoading("hide");
            },
            success: function (data) {
                if (data.message != null) {
                    alert(data.message);
                } else {
                    if ($("#fpModal").length > 0) {
                        $("#modalcontent").html("");
                        //if ($("#modalcontent").offset().top != 0) {
                        //    $("#modalcontent").offset({ left: $("#modalcontent").offset().left, top: 0 })
                        //}
                        $("#modalcontent").html(data);
                        
                        
                        //console.log($("#modalcontent").offset());
                        //$("#fpModal").modal('show');
                        $("#modalcontent").css("top", "0");

                        $("#fpModal").modal('show');

                        
                    }
                    else {
                        console.log("Not found modal");
                    }
                }
                return;
            },
            error: function (msg) {
                if (error != null) {
                    error(msg);
                }
                else {
                    alert("AjaxHtml Error : " + msg.responseText);
                }
            }
        });
    },
    AjaxMobileDown: function (url, name) {
        if (window.JSBridge.fileDownload != null) {
            window.JSBridge.fileDownload(url, name);
        }
        else {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function (aa, bb) {
                var a;

                if (xhttp.readyState === 4 && xhttp.status === 200) {

                    if (xhttp.response.size == 0) {
                        alert("작업 중 오류가 발생 하였습니다.");
                    }
                    else if (xhttp.response.type.indexOf("text/html") > -1) {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                            alert(e.srcElement.result);
                        }
                        fileReader.readAsText(xhttp.response);
                    }
                    else {
                        var filename = name;

                        var type = xhttp.getResponseHeader('Content-Type');

                        var blob = typeof File === 'function'
                            ? new File([this.response], filename, { type: type })
                            : new Blob([this.response], { type: type });

                        if (window.navigator.msSaveOrOpenBlob) {
                            window.navigator.msSaveOrOpenBlob(blob, filename);
                        }
                        else {
                            var URL = window.URL || window.webkitURL;
                            var downloadUrl = URL.createObjectURL(blob);

                            if (filename) {
                                var a = document.createElement("a");
                                if (typeof a.download === 'undefined') {
                                    window.location = downloadUrl;
                                } else {
                                    a.href = downloadUrl;
                                    a.download = name;
                                    document.body.appendChild(a);
                                    a.click();
                                }
                            } else {
                                window.location = downloadUrl;
                            }

                            setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 1000); // cleanup
                        }
                    }
                }
                else {
                    //alert("xhttp status : " + xhttp.readyState + " / " + xhttp.status);
                }
            };

            xhttp.open("GET", url, true);
            xhttp.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            //xhttp.setRequestHeader("Content-Length", JSON.stringify(data).length);
            xhttp.responseType = 'blob';
            xhttp.send();
        }
    },
    // 파라미터를 가져옵니다.
    GetParameterByName: function (name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null) {
            return "";
        } else {
            return results[1];
        }
    },
    // 파라미터를 가져옵니다.
    GetUrlParameterByName: function (url, name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        if (results == null) {
            return "";
        } else {
            return results[1];
        }
    },
    // 파라미터를 배열로가져온다. 
    GetParams: function () {
        var param = new Array();
        var url = location.search;
        var params;
        params = url.substring(url.indexOf('?') + 1, url.length);
        params = params.split("&");
        var size = params.length;
        var key, value;
        for (var i = 0; i < size; i++) {
            key = params[i].split("=")[0];
            value = params[i].split("=")[1];
            param[key] = value;
        }

        return param;
    },
    // 파라미터를 삭제한다.
    GetParameters: function (remove) {
        var param = new Array();
        var url = location.search;
        var params;
        params = url.substring(url.indexOf('?') + 1, url.length);
        params = params.split("&");
        var size = params.length;
        var key, value;
        for (var i = 0; i < size; i++) {
            key = params[i].split("=")[0];
            value = params[i].split("=")[1];

            var ischeck = true;
            if (remove != undefined) {
                for (var j = 0; j < remove.length; j++) {
                    if (remove[j] == key) {
                        ischeck = false;
                    }
                }
            }

            if (ischeck) {
                param[key] = value;
            }
        }

        var Url = new Array();
        for (var key in param) {
            Url.push(key + "=" + param[key]);
        }

        var urlstring = "";
        if (Url.length > 0) {
            urlstring = "?" + Url.join("&");
        }

        return urlstring;
    },
    // 파라미터 배열을 Url 스트링으로 변환
    GetParamsString: function (ar) {

        var Url = new Array();
        for (var key in ar) {
            Url.push(key + "=" + ar[key]);
        }

        return Url.join("&"); //인코딩 x
    },
    // 숫자만 입력가능하게 이벤트를 등록한다.
    OnlyNum: function () {
        $(".OnlyNum").keydown(function (e) {
            // 키입력에 사용되는 몇몇 키는 허용..delete, backspace, esc 등등
            if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 || e.keyCode == 13
                || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39) || e.keyCode == 110) {
                return;
            }
            else {
                if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                    //모바일 IE 브라우저의 경우 문자입력시 빈값이 남는다
                    if ($(this).val().length == 0) {
                        $(this).val('');
                    }
                    //모바일 크롬 브라우저의 경우 특수문자가 다 0으로 넘어오고  e.preventDefault(); 이벤트 실행이 안되는 버그수정
                    if (e.keyCode == 0) {
                        $(this).val('');
                    }
                    else {
                        e.preventDefault();
                    }
                }
            }
        });
    },
    // 컨트롤 체크
    ControlNullCheckFocus : function (controlId, message) {
        if ($(controlId).val().replace(/^\s+|\s+$/g, '') == '') {
            $(controlId).focus();
            alert(message);
            return false;
        }
    return true;
    },
    // Select 정렬
    SortSelect : function (select) {
        var options = jQuery.makeArray(select.find('option'));

        var sorted = options.sort(function (a, b) {
            //return (jQuery(a).attr("data-num") > jQuery(b).attr("data-num")) ? 1 : -1;
            return jQuery(a).attr("data-num") - jQuery(b).attr("data-num");
        });

        select.append(jQuery(sorted)).attr('selectedIndex', 0);
    }
}

AppEvent = 
{
    // Select 변경 이벤트
    OnSelectIndexChanged: function (pageSize, searchText) {
        OnRefresh(1, pageSize, searchText);
    },

    // 생성 이벤트
    OnInsert: function (data) {
        if (data.IsSuccess != undefined) {
            if (!data.IsSuccess) {
                alert(data.ErrorMessage);
                return false;
            }
        }
        else {
            alert("생성 되었습니다.");
        }
    },

    // 수정 이벤트
    OnUpdate: function (data) {
        if (data.IsSuccess != undefined) {
            if (!data.IsSuccess) {
                alert(data.ErrorMessage);
                return false;
            }
        }
        else {
            alert("수정 되었습니다.");
        }
    },

    // 생성 수정 이벤트
    OnUpsert: function (data) {
        if (data.IsSuccess != undefined) {
            if (!data.IsSuccess) {
                alert(data.ErrorMessage);
                return false;
            }
        }
        else {
            alert("적용 되었습니다.");
        }
    },

    // 삭제 이벤트
    OnDelete: function (data) {
        if (data.IsSuccess != undefined) {
            if (!data.IsSuccess) {
                alert(data.ErrorMessage);
                return false;
            }
        }
        else {
            alert("삭제 되었습니다.");
        }
    },

    // 갱신 이벤트
    OnRefresh : function (url, pageIndex, pageSize, searchText) {
        var DTO = { pageindex: pageIndex, pagesize: pageSize, searchText: searchText };
        AppCommon.AjaxHtml(DTO, url, function (data) {
            $("#content").html(data);
        });
    },

    // 전체 선택 및 해제
    OnCheckedAll: function (obj) {
        if (obj.checked) {
            $(".checkListAll").prop('checked', true);
            $(".checkListAll").attr('checked', true);
        }
        else {
            $(".checkListAll").prop('checked', false);
            $(".checkListAll").attr('checked', false);
        }
    },

    // 페이지가 업데이트되기 직전에 호출 되는 이벤트
    OnBegin: function () {
    },

    // 페이지가 업데이트된 후 호출 되는 이벤트
    OnComplete: function () {
    },

    // 응답 데이터가 인스턴스화된 경우 페이지가 업데이트되기 전에 호출 되는 이벤트
    OnSuccess: function () {
    },

    // 페이지를 업데이트하지 못할 경우 호출 되는 이벤트
    OnFailure: function (xhr, status, message) {
        alert(xhr.responseJSON);
        //alert('Status Code : ' + xhr.status + '\nStatus Message : ' + xhr.message + '\nError Message : ' + xhr.responseJSON);
    }
}

AppOptionalEducation =
{
	ControlCheckBoxAll : function () {
        $("#chkAll").change(function () {
            if ($(this).is(":checked")) {
                $("input[type=checkbox]").prop("checked", true);
            } else {
                $("input[type=checkbox]").prop("checked", false);
            }
        });
    },

	BindDatePicker : function () {
        $('.jDatepicker').datetimepicker({
            format: "Y-m-d H:i"
            , stepMinute: 15
            , width: 100
        });

        $.datetimepicker.setLocale('ko');
        $(".jDatepickerImg").click(function () {
            $(this).parent().find(".jDatepicker").focus();
        });

        $('.jDatepicker2').datetimepicker({
            format: "Y-m-d"
            , timepicker: false
        });
        $.datetimepicker.setLocale('ko');


        $(".jDatepickerImg2").click(function () {
            $(this).parent().find(".jDatepicker2").focus();
        });
    },

    //레이아웃 페이지 새로고침.
	ReloadLayout : function () {
        this.location.reload();
    },
    //날짜 형식 체크.
	CheckDateTime : function (value) {
		return moment(value).isValid();
	},
	// 시작날짜와 끝 날짜를 체크.
	CheckDateTimeLength : function (startDate, endDate) {
		return moment(startDate) < moment(endDate);
    },
	StringBuilder : function () {
        var strings = [];

        this.append = function (string) {
            string = verify(string);
            if (string.length > 0) strings[strings.length] = string;
        };

        this.appendLine = function (string) {
            string = verify(string);
            if (this.isEmpty()) {
                if (string.length > 0) strings[strings.length] = string;
                else return;
            }
            else strings[strings.length] = string.length > 0 ? "\r\n" + string : "\r\n";
        };

        this.clear = function () { strings = []; };

        this.isEmpty = function () { return strings.length == 0; };

        this.toString = function () { return strings.join(""); };

        var verify = function (string) {
            if (!defined(string)) return "";
            if (getType(string) != getType(new String())) return String(string);
            return string;
        };

        var defined = function (el) {
            // Changed per Ryan O'Hara's comment:
            return el != null && typeof (el) != "undefined";
        };

        var getType = function (instance) {
            if (!defined(instance.constructor)) throw Error("Unexpected object type");
            var type = String(instance.constructor).match(/function\s+(\w+)/);

            return defined(type) ? type[1] : "undefined";
        };
    }
    }

function StringBuilder() {
    var strings = [];

    this.append = function (string) {
        string = verify(string);
        if (string.length > 0) strings[strings.length] = string;
    };

    this.appendLine = function (string) {
        string = verify(string);
        if (this.isEmpty()) {
            if (string.length > 0) strings[strings.length] = string;
            else return;
        }
        else strings[strings.length] = string.length > 0 ? "\r\n" + string : "\r\n";
    };

    this.clear = function () { strings = []; };

    this.isEmpty = function () { return strings.length == 0; };

    this.toString = function () { return strings.join(""); };

    var verify = function (string) {
        if (!defined(string)) return "";
        if (getType(string) != getType(new String())) return String(string);
        return string;
    };

    var defined = function (el) {
        // Changed per Ryan O'Hara's comment:
        return el != null && typeof (el) != "undefined";
    };

    var getType = function (instance) {
        if (!defined(instance.constructor)) throw Error("Unexpected object type");
        var type = String(instance.constructor).match(/function\s+(\w+)/);

        return defined(type) ? type[1] : "undefined";
    };
};

$(document).ready(function () {
    $("#pager a").click(function () {
        var page = AppCommon.GetUrlParameterByName(this.href, "page");
        $("#page").val(page);
        $("#searchForm").submit();
        return false;
    });
});